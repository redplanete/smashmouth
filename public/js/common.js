var slideDelay = 11000;
var newSlide = "";
var doGetEvents = true;
var numCallsToDoGetEvents = 0;
	
function changeSlide(new_page, current_page, page_count)
{
	clearTimeout(window.sliderTimerId);
	
	var slides = ["what_is_smashmouth.html", "how_to_play.html", "free_trial.html", "insider.html"]

	shiftBy = $('#featured-slider').width();
	
	current_page = $("#featured-slider div").first().attr("id");
	
	if (!current_page) current_page = "slide1";
	
	var moveAmount = "-="+shiftBy;
	var oPos = "left: "+shiftBy+"px;";
	if ( new_page === "+1" ) {
		new_page = (Number(current_page.replace("slide","")) + 1);
		if (new_page > page_count) new_page = '1';
	} else if (new_page === "-1" ) {
		new_page = (Number(current_page.replace("slide","")) - 1);
		if (new_page < 1) new_page = page_count;
		moveAmount = "+="+shiftBy;
		oPos = "left: -"+shiftBy+"px;"
	}

	if(("slide"+new_page != current_page) && new_page >=1 && new_page<=page_count) // is different page and in valid range
	{
		if ("slide"+new_page == newSlide) return;
		$('#featured-slider').append("<div class='slide-tile span12' id='slide"+new_page+"' style='"+oPos+"'><iframe src='/slides/"+slides[new_page-1]+"' scrolling='no' seamless='seamless'></div>");
		newSlide = "slide"+new_page;
		advanceSlide(moveAmount);
		$("#featuredButtons a").removeClass("active");
		$("#featuredButton"+new_page).addClass("active");
		window.sliderTimerId = window.setTimeout(changeSlide, slideDelay, "+1", null, page_count);
	}
}

function advanceSlide(amount) {
	$(".slide-tile").animate({"left": amount}, 2100, "easeInOutQuint",
		function() {
			if ($(this).attr('id') == newSlide) $(".slide-tile").not("#"+newSlide).remove();
		}
	);
}

function autoAnimateSlider(page_count) {
	clearTimeout(window.sliderTimerId);
	window.sliderTimerId = window.setTimeout(changeSlide, slideDelay, "+1", null, page_count);
}

function stopSlider() {
	clearTimeout(window.sliderTimerId);	
}

function isValidEmail(str) {
  // These comments use the following terms from RFC2822:
  // local-part, domain, domain-literal and dot-atom.
  // Does the address contain a local-part followed an @ followed by a domain?
  // Note the use of lastIndexOf to find the last @ in the address
  // since a valid email address may have a quoted @ in the local-part.
  // Does the domain name have at least two parts, i.e. at least one dot,
  // after the @? If not, is it a domain-literal?
  // This will accept some invalid email addresses
  // BUT it doesn't reject valid ones. 
  var atSym = str.lastIndexOf("@");
  if (atSym < 1) { return false; } // no local-part
  if (atSym == str.length - 1) { return false; } // no domain
  if (atSym > 64) { return false; } // there may only be 64 octets in the local-part
  if (str.length - atSym > 255) { return false; } // there may only be 255 octets in the domain

  // Is the domain plausible?
  var lastDot = str.lastIndexOf(".");
  // Check if it is a dot-atom such as example.com
  if (lastDot > atSym + 1 && lastDot < str.length - 1) { return true; }
  //  Check if could be a domain-literal.
  if (str.charAt(atSym + 1) == '[' &&  str.charAt(str.length - 1) == ']') { return true; }
  return false;
}

/**
 * A method to allow for easy ajax calls from 'a' tags by setting the href to
 * javascript:doAjaxRequest(url_to_query, {'key1': 'value1', 'key2': 'value2', ...}, name_of_callback_function)
 * 
 * Also, this provides a single point for a unified exit and entry for all XHR queries.
 * 
 * @param url			the url to get
 * @param [parameters]	an optional name-value object of query string parameters
 * @param callback  	the callback function or array of callback functions. It is passed the response data from the call as 
 * 						its one parameter
 * @param [method]      GET or POST.  The default is GET
 */
function doAjaxRequest( url, parameters, callback, method ) {
	if (arguments.length == 2) {
		callback = parameters;
		parameters = null;
	}
	
	if (!(callback instanceof Array)) {
		callback = [callback];
	}
	
	var httpFunction = (method && method.toUpperCase() == "POST" ) ? $.post : $.get;

	httpFunction(url, parameters,
		function(response) {			
			if (response.user_message) alert(response.user_message);
			
			if (response.instructions) executeInstructions(response.instructions);
			
			for(var i = 0; i < callback.length; i++) {
				if (typeof callback[i] === "function") callback[i](response);
			}
		}
	).fail(function( jqXHR, textStatus, errorThrown ) {console.log(textStatus,errorThrown)});
}

/**
 * Engine that executes instructions that are sent from the server as messages.
 * 
 * @param instructions   The list of elements to add, statement, and functions to run
 */
function executeInstructions(instructions) {
	for (var i = 0; i < instructions.length; i++) {
		
		var targets = (instructions[i]) ? instructions[i].targets : null;
		var actions = (instructions[i]) ? instructions[i].actions : null;
			
		targetsLength = $.isArray(targets) ? targets.length : -1;
		actionsLength = $.isArray(actions) ? actions.length : -1;
		
		var largestLength = Math.max(targetsLength,actionsLength,1);
		
		for (var j = 0; j < largestLength; j++) {
			
			if ( j == 0 && (targetsLength == -1 || actionsLength == -1) ) {
				if (targetsLength == -1 && actionsLength == -1) {
					// a single method with params, or staments and elements
					execute(actions, targets);
				} else if (targetsLength == -1) {
					// elements
					execute(null, targets);
					
					// current function with no args, or statement(s) from list of function(s), statement(s) or both
					execute(actions[0]); // j == 0			
				} else if (actionsLength == -1) {
					// current elements from list of elements, or elements and params
					execute(null, targets[0]); // j == 0
					
					// function with no args or statements
					execute(actions);
				}				
			} else {
				var action = ( j < actionsLength ) ? actions[j] : null;
				var target = ( j < targetsLength ) ? targets[j] : null;
				
				execute(action, target);
			}
			
		}
	}
}

/**
 *  Helper function that processes an action and target combination
 *  
 *  
 * @param executable  	a function or a string of javascript statements to execute
 * @param params	  	if there is a function, than a parameter list for the function,
 * 						otherwise, elements to add to the document.
 * 
 * @see /library/My/Exception/HandledException::__construct
 */
function execute(executable, params) {
	if ( typeof window[executable] === "function" ) {
		// execute function with parameters
		if ($.isArray(params)) {
			window[executable].apply(null, params.slice(0));
		} else {
			window[executable](params);
		}
		
	} else {
		// execute javascript code by appending it to document
		if (executable != null) $("body").append("<scr"+"ipt>try {"+executable+"} catch (err) {console.log(err);}</scr"+"ipt>");
		
		// append param(s) to document since there is no function
		if ($.isArray(params)) {
			for(var i = 0; i < params.length; i++) {
				if (params[i]) $("body").append(params[i]);
			}			
		} else {
			if (params) $("body").append(params);
		}
	}
	
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
}

function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}
	
function getEvents() {
	console.log("getEvents called " + ++numCallsToDoGetEvents + " time(s)." );
	if (doGetEvents) doAjaxRequest( "/message/getevents/guid/"+guid(), null, getEvents, "get" );
}

function displayNewAlerts(selector, count, tooltip) {
	console.log("displayNewAlerts called with", selector, count, tooltip);
	$(selector).html(count).attr("title", tooltip).fadeIn(
		function() {
			$(this).addClass("show");
		}
	);
}

function scrollToElement(elementId, targetSibling){
    var $element = $("#"+elementId);
    
    if (!$element.length) return;
    
	if (typeof targetSibling !== 'undefined') {
		console.log(targetSibling.replace("/", "\\/"));
		console.log($element.parent().find(targetSibling.replace("/", "\\/")));
		$element.parent().find(targetSibling.replace("/", "\\/")).each(
			function() {
				console.log(this);
			}
		);
		$('body').animate({scrollTop: $element.parent().find(targetSibling.replace("/", "\\/")).offset().top}, 1000, "easeInOutCirc");
	} else {
		$('body').animate({scrollTop: $element.offset().top}, 1000, "easeInOutCirc");
	}
}

/**
 * Launch javascript engines -
 *    1.) Ajax Polling for Events
 *    2.) Slides on the home page.
 * 
 * @todo Long Polling will be replaced by WebSocket
 */
$(document).ready(
	function() {
		// Start Long Polling
		//getEvents();
		
		// Start front page slider
		if ( $(".slider").length ) {
			autoAnimateSlider(4);
			$("#featured-slider").on("mouseenter", ".slide-tile", function(){stopSlider();}).on("mouseleave", function(){autoAnimateSlider(4);});
		}
		
		// Remove event stamp cookies for unload
		$(window).on('beforeunload',
			function() {
				$.removeCookie('cookie_stamp', { path: '/' });
			}
		);
	}
); 