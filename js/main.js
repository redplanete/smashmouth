$(document).ready(function() {
	$(".page-error").dialog({
		title: "Error",
		modal: true,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});

	$(".page-success").dialog({
		title: "Success",
		modal: false,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});

	$("input[type='submit'], button, .button").button();

	$("table.tablesorter").tablesorter();

	$(".datepicker").datepicker();

	$(".tabs").tabs();
});

$(document).bind("messagebox", function(e, message) {
	$("#pop-dialog").html(message).dialog({
		title : 'SmashMouth Fantasy Sports',
		modal: true,
		buttons: {
			"Close" : function() {
				$(this).dialog("close");
			}
		}
	});
});

$(document).bind("messagebox_refresh", function(e, message) {
	$("#pop-dialog").html(message).dialog({
		title : 'SmashMouth Fantasy Sports',
		modal: true,
		buttons: {
			"Close" : function() {
				window.location = window.location;
				$(this).dialog("close");
			}
		}
	});
});