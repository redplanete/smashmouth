<?php
/**
 * A special exeption that once invoked, forward the user to a custom
 * error page and can trigger addition actions from the client.
 * @author Leon McCottry
 *
 */
class My_Exception_FowardHttpErrorCodeException extends My_Exception_HandledException {
	
	/**
	 * (non-PHPdoc)
	 * @see \library\My\Exception\My_Exception_HandledException::prepareErrorInfo()
	 */
	public function prepareErrorInfo() {
		$error_handler = new ArrayObject(array('type' => 404));
		$request = $this->request;
		/* @vars $request Zend_Controller_Request_Http */
		
		$request->setParam("error_handler",$error_handler);	
		$request->setControllerName("ErrorController");	
		$request->setActionName("error");
		
		return function() use ($request) {
			$front = \Zend_Controller_Front::getInstance();
			$front->dispatch($request);			
		};
	}
}

?>