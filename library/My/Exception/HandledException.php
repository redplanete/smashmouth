<?php
/**
 * The base exception class that the front end know how to handle 
 * and present gracefully to the end user.
 *
 * @author Leon McCottry
 *        
 */
class My_Exception_HandledException extends Zend_Exception {
	
	/**
	 * @var Zend_Controller_Request_Http $request
	 *				The request that was made to the server
	 */
	protected $request;
	
	/**
	 * @var string  $json
	 * 			A JSON string of this exception that is to be presented to the frontend on XHR
	 * 			request
	 */
	protected $json;
	

	/**
	 * @var array $errorInfo The array form of this Exception
	 */
	protected $errorInfo = array();
	
	/**
	 * @param  Zend_Controller_Request_Http    $request  The request sent to the server	
	 * 	
	 * @param  message[optional]       A user-friendly message to be presented on the front-end
	 *        	
	 * @param  code[optional]
	 *        	
	 * @param  previous[optional]
	 *        	
	 */
	public final function __construct($message = null, $code = null, $previous = null) {
		parent::__construct ($message, $code, $previous);
		
		$this->errorInfo = array(
			'success' => false,
			'user_message' => $message,
			'instructions' => array() 
		);
		/* 
		 * 'instructions' is intended to be an array of two dimensional array with the with the 
		 *  first paramater titled 'targets' which is a list of frontend identifiers
		 *  who will be the subject(s) of the second column named 'actions' which is/are 
		 *  functions to invoke on the targets.
		 *  
		 *  If a target entry is also and array, the those targets are all presented as
		 *  parameters to each function in the order they are listed.
		 *  
		 *  If there are no targets, then the corresponding actions are simply run
		 *  
		 *  If there are no actions, then the targets are printed
		 *  
		 *  Example: 
		 *  
		 *  	instructions[0] =  array( 'targets' => array( array('user1', 50), array('user2',30) ),
		 *  							  'actions' => array( 'awardPoints', 'logPoints')
		 *  						);
		 *  	instructions[1] = array( 'targets' => '$(#scoreboard)',
		 *  							 'actions' => 'update'
		 *  						);
		 *      instructions[2] = array( 'targets' => null, // could also simple be ommitted
		 *  							 'actions' => 'alert("There are five minutes remaining in the game!")'
		 *  						); 
		 *      instructions[3] = array( 'targets' => null, // could also simple be ommitted
		 *  							 'actions' => 'checkEvents'
		 *  						); 
		 *  	instructions[4] = array( 'targets' => '<input type="hidden" name="last_updated" value="2014-09-30 11:40:48"/>' );
		 *  
		 *  When presented to the client as json, the client will do the following:
		 *  
		 *  			execute:  awardPoints('user1', 50);
		 *  					  awardPoints('user2',30);
		 *  					  logPoints('user1', 50);
		 *  					  logPoints('user2',30);
		 *  					  update($(#scoreboard));
		 *  					  alert("There are five minutes remaining in the game!");
		 *  					  checkEvents();
		 *  					  document.write('<input type="hidden" name="last_updated" value="2014-09-30 11:40:48"/>');
		 *  
		 *  
		 *  note: the client always quotes with single quotes.
		 *  
		 */
		$this->request = Zend_Controller_Front::getInstance()->getRequest();
				
		if ($this->request->isXmlHttpRequest()) {
			$this->json = json_encode($this->errorInfo);
		}	
		
		$callback = $this->prepareErrorInfo();
		$callback();
		$this->presentException();
	}
	
	/**
	 * Allows sub-classes to do something with the exception before it is returned.
	 * 
	 * @return function  returns a function to be invoked after the message is prepared
	 * 					 By default, the function does nothing.
	 */
	protected function prepareErrorInfo() {
		return function(){} ;
	}
	
	/**
	 *  The final act of this exception where it attempts to present itself gracefully to the user. 
	 */
	public final function presentException() {
		if ($this->request->isXmlHttpRequest()) {
			$jsonHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('json');
			$jsonHelper->sendJson( $this->errorInfo);
		} else {
			echo $this->errorInfo['user_message'];
		}
		exit;
	}
}

?>