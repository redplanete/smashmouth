<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class My_View_Helper_FlashMessenger extends Zend_View_Helper_Abstract
{
	public function flashMessenger ($width = null)
    {
		$flash = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');

		#for message

		
		if($flash->getCurrentMessages())
		{
			foreach($flash->getCurrentMessages() as $key => $msg)
			{
				$message[key($msg)][] = $msg[key($msg)];
			}

			$flash->clearCurrentMessages();
		}
		else if($flash->getMessages())
		{
			foreach($flash->getMessages() as $key => $msg)
			{
				$message[key($msg)][] = $msg[key($msg)];
			}
		}

		$str_msg = '';
		$style = '';

		if($width) $style = "style='width:".$width."px'";

		if(isset($message) && is_array($message))
		{
			foreach($message as $key => $arr_msg)
			{
				$str_msg .= '<div '.$style.' class="gr-'.$key.'">';
				$str_msg .=	'<ul>';

				foreach($arr_msg as $msg)
				{
						$str_msg .= '<li>'.$msg.'</li>';
				}
				$str_msg .= '</ul>';
				$str_msg .= '</div>';
			}
	   }

	   return $str_msg;
	}
}
?>
