<?php
/**
* Simple Template Parser Class
*
* @author  :  MA Razzaque Rupom <rupom_315@yahoo.com>, <rupom.bd@gmail.com>
*             Moderator, phpResource Group(http://groups.yahoo.com/group/phpresource/)
*             URL: http://www.rupom.info
* @date    : 07/17/2006
* @version :  1.0
* Purpose  :  Parsing Simple Template File and Data that Contains Macros
*/

class My_TemplateParser
{
     var $data;

     /**
      * Constructor to initialize parser
      * @param array $data
      */
     public function __construct(array $data = array()) {
     	$this->initData($data);
     }
     
     /**
     * Initializes "macro=>value" array
     * @param Array "macro=>value" array
     * @return none
     */
   function initData($data)
   {
      $this->data = array();
      $this->data = $data;
   }

   /**
     * Parses template file
     * @param template filename
     * @return parsed template
     */
   function parseTemplateFile($templateFile)
   {
      $searchPattern          = "/\\{\\{([a-zA-Z0-9_]+)\\}\\}/i"; // macro delimiter "{" and "}"
      $replacementFunction    = array(&$this, 'parseMatchedText');  //Method callbacks are performed this way
      $fileData               = file_get_contents($templateFile);
      $parsedTemplate         = preg_replace_callback($searchPattern, $replacementFunction, $fileData);

      return $parsedTemplate;
   }

   /**
     * Parses template data {{variable}} format
     * @param template data
     * @return parsed data
     */
   function parseTemplateData($templateData)
   {
      $searchPattern          = "/\\{\\{([a-zA-Z0-9_]+)\\}\\}/i"; //macro delimiter "{" and "}"
      $replacementFunction    = array(&$this, 'parseMatchedText');  //Method callbacks are performed this way
      $parsedTemplate         = preg_replace_callback($searchPattern, $replacementFunction, $templateData);

      return $parsedTemplate;
   }

   /**
    * parse php file containing data in {{$variable}} format
    * @param <type> $templateData
    * @return <type>
    */
   function parsePhpTemplateData($templateData)
   {
	  $searchPattern          = "/\\{\\{\\$([a-zA-Z0-9_]+)\\}\\}/i"; //macro delimiter "{$variable_name}"
      $replacementFunction    = array(&$this, 'parseMatchedText');  //Method callbacks are performed this way
      $parsedTemplate         = preg_replace_callback($searchPattern, $replacementFunction, $templateData);

      return $parsedTemplate;
   }

   /**
   * Callback function that returns value of a matching macro
   * @param Array $matches
   * @return String value of matching macro
   */
   function parseMatchedText($matches)
   {
      if(isset($this->data[$matches[1]]))
      {
         return $this->data[$matches[1]];
      }
      return '{{'.$matches[1].'}}';    //wont be parsed if no value is set for the macro
   }

} //End Of Class

?>