<?php

/** 
 * Class to add the ip address and user_id to the Session DB table
 * @author Leon McCottry
 * 
 */
class My_Db_Table_SessionSaveHandler extends \Zend_Session_SaveHandler_DbTable {
	
	/**
	 * Write session data
	 *
	 * @param string $id
	 * @param string $data
	 * @return boolean
	 */
	public function write($id, $data)
	{
		$return = false;
	
		$account = Zend_Auth::getInstance()->getIdentity();
		
		
		$users_id = ($account) ? $account->getUser()->getId() : null;
		
		$data = array($this->_modifiedColumn => time(),
				$this->_dataColumn     => (string) $data,
				"ip_address" => $_SERVER['REMOTE_ADDR'],
				"users_id" => $users_id);
	
		$rows = call_user_func_array(array(&$this, 'find'), $this->_getPrimary($id));
	
		if (count($rows)) {
			$data[$this->_lifetimeColumn] = $this->_getLifetime($rows->current());
	
			if ($this->update($data, $this->_getPrimary($id, self::PRIMARY_TYPE_WHERECLAUSE))) {
				$return = true;
			}
		} else {
			$data[$this->_lifetimeColumn] = $this->_lifetime;
	
			if ($this->insert(array_merge($this->_getPrimary($id, self::PRIMARY_TYPE_ASSOC), $data))) {
				$return = true;
			}
		}
	
		return $return;
	}	
}

?>