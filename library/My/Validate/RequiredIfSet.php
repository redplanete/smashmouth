<?php

require_once ('Zend\Validate\Abstract.php');

class My_Validate_RequiredIfSet extends Zend_Validate_Abstract {

	protected $_checkField = array();
	
	function __construct($fieldIfSet = array())
	{
		if(is_array($fieldIfSet))
		{
			foreach($fieldIfSet as $field)
			{
				$this->_checkField[] = (string) $field;
			}
		}
		else
		{
			$this->_checkField[] = (string) $field;
		}
	}
	
	public function isValid($value, $context = null)
	{
		$value = (string) $value;
		$this->_setValue($value);
		
		$error = false;
		
		foreach($this->_checkField as $field)
		{
			if(isset($context[$field]) && !empty($context[$field]))
			{
				
			}
		}
	}
}

?>