<?php
/**
* Validates a phone number (very basic).
*/
class My_Validate_PhoneNumber extends Zend_Validate_Abstract
{
    /**
    * Validates phone number.
    *
    * @param mixed $value
    * @return boolean
    */
	const NOT_MATCH = 'notMatch';

	protected $_messageTemplates = array(
        self::NOT_MATCH => 'Not a valid phone number'
    );
	
    public function isValid($value)
    {
        // Strip all non-numeric characters
        $value = (string) $value;
        
        preg_match("/(^(([\+]\d{1,3})?[ \.-]?[\(]?\d{3}[\)]?)?[ \.-]?\d{3}[ \.-]?\d{4}$)/", $value, $m);
        
        if(!sizeof($m))
        {
        	$this->_error(self::NOT_MATCH);
        	return false;
        }
        
        return true;
    }
}
?>