<?php
/**
 * @uses       Custom_Validate_PasswordConfirmation
 * @license    Dhruba Khanal (dhrubasearch@yahoo.com)
 */

class My_Validate_PasswordConfirmation extends Zend_Validate_Abstract
{
	const NOT_MATCH = 'notMatch';

	protected $_messageTemplates = array(
		self::NOT_MATCH => "The passwords don't match.  Please try again."
	);

	public function isValid($value, $context = null)
	{
		$value = (string) $value;
		$this->_setValue($value);

		if (is_array($context))
		{
			if (isset($context['login_pass']) && ($value == $context['login_pass'])) return true;
		}
		elseif (is_string($context) && ($value == $context))
		{
			return true;
		}

		$this->_error(self::NOT_MATCH);

		return false;
	}
}

