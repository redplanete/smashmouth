<?php
/**
 * @uses       Custom_Mail
 * @license    Dhruba Khanal (dhrubasearch@yahoo.com)
 */

class My_Mail extends Zend_Mail
{
	private $_error = '';

	public function  __construct($transport = 'sendmail')
	{
		parent::__construct();

		#set smtp detail if any
		if($transport == 'smtp')
		{
			try {
				  $config = array('auth' => 'login', 'username' => '',
						     	'password' => '','ssl' => 'tls','port' => 587);

				  $mailTransport = new Zend_Mail_Transport_Smtp('smtp.gmail.com',$config);
				  self::setDefaultTransport($mailTransport);
				} catch (Zend_Exception $e) { $this->setError($e->getMessage()); }
		}
		else
		{
			self::setDefaultTransport(new Zend_Mail_Transport_Sendmail());
		}

		$mail_config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/config.ini','mail');
		self::setDefaultFrom($mail_config->params->fromemail, $mail_config->params->fromname);
	}

	public function send()
	{
		try
		{
			return parent::send();
		}
		catch (Zend_Mail_Exception $e)
		{
			$this->setError($e->getMessage());
			return false;
		}
	}

	private function setError($err)
	{
		$this->_error = $err;
	}

	public function getError()
	{
		return $this->_error;
	}

	/**
	 * Parse html file or html data
	 * @param mixed $html html file or html data)
	 * @param array $data array of data for each replacing variable in template)
	 * @param bool $is_file default html file, html data set false
	 * @return string
	 */
	public function parseHtml($html, $data, $is_file = true)
	{
		$parser = new My_TemplateParser();
		$parser->initData($data);
		
		if($is_file)
		{
			$content = $parser->parseTemplateFile($html);
		}
		else
		{
			$content = $parser->parseTemplateData($html);
		}

		return $content;
	}
	

}

?>
