<?php

/**
 * @uses       Custom_Controller_Action
 * @license    Dhruba Khanal (dhrubasearch@yahoo.com)
 */
class My_Controller_Action extends Zend_Controller_Action {

    /**
     * Flash Messenger object, will be used in controller
     * @var object
     */
    protected $_flash = '';

    /**
     * Contains site full url
     * @var string
     */
    protected $_site_url = '';

    /**
     * Check whether the request is ajax
     * @var bool
     */
    protected $isAjax = false;

    /**
     * Check whether current user is admin or not
     * @var bool
     */
    protected $isAdmin = false;

    /**
     * Set search key if search is posted
     * @var string
     */

    /**
     * Check whether current user is admin or not
     * @var bool
     */
    protected $isSuperAdmin = false;

    /**
     * Set search key if search is posted
     * @var string
     */
    protected $searchKey = '';

    /**
     * Auth object
     * @var object
     */
    protected $auth = '';

    /**
     * Config container
     * @var object
     */
    protected $Config = '';


    function preDispatch() {

        $this->_flash = $this->_helper->getHelper('FlashMessenger');
        $this->_site_url = $this->getRequest()->getScheme() . "://" . $this->getRequest()->getHttpHost();
        $auth = Zend_Auth::getInstance();
        $this->auth = $auth;

        //set search for listing
    }

    /**
     * Add message to FlashMessenger plugin
     * @param string $message
     * @param string $type  Message type success,error etc
     */
    public function addMessage($message, $type = 'success') {
        $this->_flash->addMessage(array($type => $message));
    }



    protected function RedirectUrl($url, array $options = array()) {
        $this->_helper->redirector->gotoUrl($url, $options);
    }

    protected function Redirector($action, $controller, $module, $params = array()) {
        $this->_helper->redirector($action, $controller, $module, $params);
    }
   
    
}

?>
