<?php

/**
 * @uses       Custom_Db_Table_Abstract
 * @license    Dhruba Khanal (dhrubasearch@yahoo.com)
 */
class My_General {

    const MESSAGE_SUCCESS = 1;
    const MESSAGE_ERROR = 2;

    private static $apiSalt = 'fgjk@#2134FIU';
    private static $userKeyLength = 20;

    static function randomString($length = 10) {
        $random = "";

        mt_srand((double) microtime() * 1000000);

        $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
        $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
        $data .= "0FGH45OP89";

        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (mt_rand(1, strlen($data))), 1);
        }

        return $random;
    }

    static function encrypt($text) {
        return SHA1($text);
    }

    /**
     * Contert array to json data
     * @param array $data
     * @return json
     */
    static function toJSON($data) {
        try {
            return Zend_Json_Encoder::encode($data);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Decode json data
     * @param json $data
     * @param const $type json output type (Zend_Json::TYPE_OBJECT, Zend_Json::TYPE_ARRAY)
     * @return mixed
     */
    static function decodeJSON($data, $type = Zend_Json::TYPE_OBJECT) {
        try {
            return Zend_Json_Decoder::decode($data, $type);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Encode string with salt added
     * @param string $text
     * @return string
     */
    static function encode($text) {
        $finalTest = $text . ':' . self::$apiSalt;

        return base64_encode($finalTest);
    }

    /**
     * Encode string with salt added
     * @param string $text
     * @return string
     */
    static function decode($text) {
        $value = base64_decode($text);
        $arrValue = explode(':', $value);
        return $arrValue[0];
    }

    /**
     * Format date
     * @param date $date
     * @param string $format
     * @return date
     */
    static function dateFormat($date, $format = 'F jS, Y @ h:i A') {
        if (empty($date))
            return false;

        $time = strtotime($date);
        return date($format, $time);
    }

    /**
     * Convert locatime to Gmt Time using offset
     * @param string $dateTime
     * @param bool $timeOnly
     * @return string
     */
    static function localToGmt($dateTime, $timeOnly = false, $dateOnly = false) {
        if (empty($dateTime) || strstr('0000', $dateTime))
            return false;

        $userTzOffset = Zend_Auth::getInstance()->getIdentity()->timeZone;
        $osts = explode(":", $userTzOffset);
        $sec = ($osts[0] * 60 * 60) + $osts[1] * 60;
        $userTimeStamp = strtotime(date('Y-m-d::H:i:s', time() + $sec));
        $dt = date('Y-m-d::H:i:s', strtotime($dateTime . "- $sec SECONDS", $userTimeStamp));

        if ($timeOnly) {
            $dts = explode('::', $dt);
            return $dts[1];
        } else if ($dateOnly) {
            $dts = explode('::', $dt);
            return $dts[0];
        } else {
            return str_replace('::', ' ', $dt);
        }
    }

    /**
     * Convert Gmt Time local time using offset
     * @param string $dateTime
     * @param bool $timeOnly
     * @return string
     */
    static function gmtToLocal($dateTime, $timeOnly = false, $dateOnly = false) { //die;
        if (empty($dateTime) || strstr('0000', $dateTime))
            return false;
        $config = Zend_Registry::get('config');
        $dateFormat = $config->params->dateFormat . '::' . $config->params->timeFormat;
        ////print_r(Zend_Auth::getInstance()->getIdentity());die;
        $userTzOffset = Zend_Auth::getInstance()->getIdentity()->timeZone;
        //print_r($userTzOffset);die;
        $osts = explode(":", $userTzOffset);
        $sec = ($osts[0] * 60 * 60) + $osts[1] * 60;
        $gmtTimeStamp = strtotime($dateTime, time()) + $sec;
        $dt = date($dateFormat, $gmtTimeStamp);

        if ($timeOnly) {
            $dts = explode('::', $dt);
            return $dts[1];
        } else if ($dateOnly) {
            $dts = explode('::', $dt);
            return $dts[0];
        } else {
            return str_replace('::', ' ', $dt);
        }
    }

    /**
     * Return 6 character user unique id based on user id
     * @param int $userId
     * @return string
     */
    static function userUniqueKey($userId) {
        $hash = substr(self::encrypt($userId), -6);
        return $hash;
    }
    
    
    /**
     *
     * @param type $folder
     * @param type $zipFile
     * @param type $subfolder
     * @return boolean 
     */
    static function folderToZip($folder, &$zipFile, $subfolder = null) {
        if ($zipFile == null) {
            // no resource given, exit
            return false;
        }
        // we check if $folder has a slash at its end, if not, we append one
        $folder .= end(str_split($folder)) == "/" ? "" : "/";
        $subfolder .= end(str_split($subfolder)) == "/" ? "" : "/";
        // we start by going through all files in $folder
        $handle = opendir($folder);
        while ($f = readdir($handle)) {
            if ($f != "." && $f != "..") {
                if (is_file($folder . $f)) {
                    // if we find a file, store it
                    // if we have a subfolder, store it there
                    if ($subfolder != null)
                        $zipFile->addFile($folder . $f, $subfolder . $f);
                    else
                        $zipFile->addFile($folder . $f);
                } elseif (is_dir($folder . $f)) {
                    // if we find a folder, create a folder in the zip
                    $zipFile->addEmptyDir($f);
                    // and call the function again
                    folderToZip($folder . $f, $zipFile, $f);
                }
            }
        }
    }
    
    /**
     * Adds and ordinal to a number value
     * 
     * @param int $num
     * @return string  the number plus the ordinal
     */
	public static function addOrdinalNumberSuffix($num) {
		if (!in_array(($num % 100),array(11,12,13))){
			switch ($num % 10) {
				// Handle 1st, 2nd, 3rd
				case 1:  return $num.'st';
				case 2:  return $num.'nd';
				case 3:  return $num.'rd';
			}
		}
		return $num.'th';
  	}

}

?>
