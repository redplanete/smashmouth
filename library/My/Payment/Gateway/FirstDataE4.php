<?php

class My_Payment_Gateway_FirstDataE4
{
	private $_trxnProperties = array(
		"User_Name"=>null,
		"Secure_AuthResult"=>null,
		"Ecommerce_Flag"=>null,
		"XID"=>null,
		"ExactID"=>null,	//Payment Gateway
		"CAVV"=>null,
		"Password"=>null,	//Gateway Password
		"CAVV_Algorithm"=>null,
		"Transaction_Type"=>null,	//Transaction Code I.E. Purchase="00" Pre-Authorization="01" etc.
		"Reference_No"=>null,
		"Customer_Ref"=>null,
		"Reference_3"=>null,
		"Client_IP"=>null,	//This value is only used for fraud investigation.
		"Client_Email"=>null,	//This value is only used for fraud investigation.
		"Language"=>null,	//English="en" French="fr"
		"Card_Number"=>null,	//For Testing, Use Test#s VISA="4111111111111111" MasterCard="5500000000000004" etc.
		"Expiry_Date"=>null,	//This value should be in the format MM/YY.
		"CardHoldersName"=>null,
		"Track1"=>null,
		"Track2"=>null,
		"Authorization_Num"=>null,
		"Transaction_Tag"=>null,
		"DollarAmount"=>null,
		"VerificationStr1"=>null,
		"VerificationStr2"=>null,
		"CVD_Presence_Ind"=>null,
		"Secure_AuthRequired"=>null,
		"Currency"=>null,
		"PartialRedemption"=>null,

		// Level 2 fields
		"ZipCode"=>"",
		"Tax1Amount"=>"",
		"Tax1Number"=>"",
		"Tax2Amount"=>"",
		"Tax2Number"=>"",

		"SurchargeAmount"=>"",	//Used for debit transactions only
		"PAN"=>""	//Used for debit transactions only
	);

	private $_client = null;
	private $_uri = null;
	private $_response = null;
	protected $_errors = array();

	public function __construct($uri = null)
	{
		$this->setUri($uri);
	}

	public function setUri($uri)
	{
		$this->_uri = $uri;
	}

	public function process()
	{
		$log = Zend_Registry::get('log');
		try {
			$client = new SoapClient($this->_uri);

			$this->_errors = array();
			$this->_response = null;

			$trxn = array("Transaction" => $this->_trxnProperties);
			$trxnResult = $client->__soapCall('SendAndCommit', $trxn);

			$this->_response = $trxnResult;

			$log->info($trxn);
			$log->info($trxnResult);

			if($trxnResult->Transaction_Error)
			{
				$this->_errors[$trxnResult->EXact_Resp_Code] = $trxnResult->EXact_Message;
			}

			return $this->_response;
		} catch(Exception $e)
		{
			$log->info($e->getMessage());
			$this->_errors[] = $e->getMessage();
		}
	}

	public function getRepsonse()
	{
		return $this->_response;
	}

	public function isApproved()
	{
		return ($this->_response && $this->_response->Transaction_Approved) ? true : false;
	}

	public function getErrors()
	{
		return $this->_errors;
	}

	public function __set($name, $value)
	{
		if(!array_key_exists($name, $this->_trxnProperties))
		{
			throw new Exception("Invalid property: {$name}", 1);
		}

		$this->_trxnProperties[$name] = $value;
	}

	public function __get($name)
	{
		if(!array_key_exists($name, $this->_trxnProperties))
		{
			throw new Exception("Invalid property: {$name}", 1);
		}

		return $this->_trxnProperties[$name];
	}


}