<?php
/**
 * Base class for all model objects
 * 
 * @author Leon McCottry
 *
 */
abstract class Application_Model_AbstractModel {
	
	/**
	 * @var array   The data which represents a database row
	 */
	protected $data = array();
	
	/**
	 * @var array   columns which should be protected from over-writing
	 */
	protected $write_protected = array();
	
	/**
	 * The name of the model
	 * 
	 * @var string
	 */
	protected $model_name;

	/**
	 * The mapper object that handles saving this object to the database
	 * 
	 * @var Application_Model_AbstractMapper
	 */
	protected $mapper;
	
	/**
	 * reserved property names.
	 * 
	 * @var array[string]
	 */
	private $reserverd = array("data","mapper","modelName","id");
	
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	abstract public function __construct();
	
	/**
	 * Retrieves a single column value of a row by column name
	 * 
	 * @param string $name          The column name of the value to be retrieved
	 * @return multitype:|null      returns the column value on success, or null on failure
	 */
	public function __get($name)
	{	
		if (!in_array($name, $this->reserverd) && method_exists($this, "get".ucfirst($name))) {
			$userDefinedMethod = "get".ucfirst($name);
			return $this->$userDefinedMethod();
		}
			
		if(array_key_exists($name, $this->data))
		{
			return $this->data[$name];
		}
	
		return null;
	}
	
	/**
	 * Sets a single value of a row by column name in order to later save it
	 * 
	 * @param string $name    The column name of the value to be stored
	 * @param string $value   The value to be stored
	 * @throws Exception	  Throws an exception when attempting to overwrite protected data
	 * @return boolean		  returns true if the value is set, otherwise false
	 */
	public function __set($name, $value)
	{	
		if (!in_array($name, $this->reserverd) && method_exists($this, "set".ucfirst($name))) {
			$userDefinedMethod = "set".ucfirst($name);
			return $this->$userDefinedMethod($value);
		}
		
		if(in_array($name, $this->write_protected) 
		   || ( in_array($name, $this->getMapper()->getDbTable()->info(Zend_Db_Table_Abstract::PRIMARY)) 
		   		&& isset($this->data[$name]) ) )
		{
			throw new Exception("Cannot modify {$name} value", 1);
		}
	
		if(array_key_exists($name, $this->data))
		{
			$this->data[$name] = $value;
			return true;
		}
		return false;
	}

	/**
	 * Magic method to allow isset() and empty() to work with this object
	 * @param string $key
	 */
	public function __isset($key) {
		return isset($this->data[$key]);
	}
	
	/**
	 * Gets the data of the model
	 * 
	 * @return array  An associative array of the values stored in this model
	 */
	public final function getData() {
		return $this->data;
	}
	
	/**
	 * Alias for getData()
	 * 
	 * @see getData()
	 * @return array
	 */
	public function toArray() {
		return $this->getData();
	}
	
	/**
	 * Sets all writable data to this model
	 * 
	 * @param array $data                         The data to be stored in this model
	 * @return Application_Model_AbstractModel	  returns a reference to this model
	 */
	public function setData($data) {	
		foreach ( $data as $name => $value ) {
			try {
				$this->__set($name, $value);
			} catch (Exception $e) {
				throw $e;
			}			
		}
		return $this;
	}
	
	/**
	 * Retrieves the mapper which handles communicating with the database table for storage and retrieval
	 * 
	 * @return Application_Model_AbstractMapper  the mapper for this model to persist it to the database
	 */
	public final function getMapper() {
		if ((is_null($this->mapper))) {		
			$mapperName = "Application_Model_".$this->getModelName()."Mapper";
			$this->mapper = new $mapperName();
		}
		
		return $this->mapper;
	}
	
	/**
	 *  A string that indentifies this model.  It is used to reflectively construct the corresponding Mapper and DbTable
	 */
	public final function getModelName() {
		if ( (empty($this->model_name)) ) {
			$class_name = get_class($this);
			$this->model_name = str_replace("Application_Model_", "", $class_name);
		}
		return $this->model_name;
	}
	
	
	/**
	 *  Returns the primary key of this model
	 *  
	 *  @return mixed  A string or an array of strings that represent the primary key of this model
	 */
	public final function getId() {

		$primaryKeys = $this->getMapper()->getDbTable()->info(Zend_Db_Table_Abstract::PRIMARY);
		if (count($primaryKeys) == 0) {	
			return null;
		} else if (count($primaryKeys) == 1) {
			$id_name = current($primaryKeys);
			return $this->$id_name;
		} else {
			$keysAndValues = array();
			foreach ($primaryKeys as $key) {
				$keysAndValues[$key] = $this->$key;
			}
			return $keysAndValues;
		}
		
	}
	
	/**
	 * Model convenience method which uses it's mapper to save itself to the database
	 * 
	 * @return mixed  returns the primary key(s) of the saved object if it was and insert
	 *                for an update, the number of rows updated is returned
	 */
	public function save() {
		return $this->getMapper()->save($this);
	}
}
