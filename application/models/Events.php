<?php
/**
 * This represents the key component of the notification system to focus on delivering
 * real-time information to the users
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_Events extends Application_Model_AbstractModel
{

	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = 	array(
			"id" => null,
			"users_id" => null,
			"event_types_id" => null,
			"is_new" => null,
			"cookie_stamp" => null
		);
	}
	
}

