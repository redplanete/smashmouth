<?php

class Application_Model_Season {
	protected $_season;
	
	public function __construct(Zend_Db_Table_Row $season) {
		$this->_season = $season;
	}
	
	public static function createSeason($data) {
		$log = Zend_Registry::get("log");
		$seasonDb = new Application_Model_DbTable_Season();
		foreach (array("sport_id", "season_id", "date_started", "date_ended") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $seasonDb->info(Zend_Db_Table_Abstract::COLS);
		$season = null;
		try {
			$season = $seasonDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$season->$column = $data[$column];
				}
			}
			$season->save();
			return new self($season);
		} catch (Zend_Db_Exception $ex) {
			$error = $ex->getMessage();
			$log->info($error);
			$season->delete();
			if (strisstr($error, "Duplicate entry")) {
				throw new Exception("Duplicate season found");
			} else {
				throw new Exception("An error occured during season creation", 1);
			}
		}
	}
	public static function deleteSeason($data) {
		$seasonDb = new Application_Model_DbTable_Season();
		$select = $seasonDb->select();
		foreach (array("sport_id", "season_id", "date_started", "date_ended") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
			$select->where("{$field} = ?", $data[$field]);
		}
		$record = $seasonDb->fetchRow($select);
		if ($record) {
			$record->delete();
		}
	}
	public static function getSeason($data) {
		$seasonDb = new Application_Model_DbTable_Season();
		$select = $seasonDb->select();
		if (is_numeric($data)) {
			$select->where("id = ?", $data);
		} else {
			foreach (array("sport_id", "season_id") as $field) {
				if (!isset($data[$field]) || empty($data[$field])) {
					throw new Exception("{$field} missing or empty", 1);
				}
				$select->where("{$field} = ?", $data[$field]);
			}
		}
		$record = $seasonDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}
	
	public function __get($name) {
		$seasonDb = new Application_Model_DbTable_Season();
		$columns = $seasonDb->info(Zend_Db_Table_Abstract::COLS);
		if (in_array($name, $columns)) {
			return $this->_season->$name;
		}
		return false;
	}
	public function __set($name, $value) {
		$seasonDb = new Application_Model_DbTable_Season();
		$columns = $seasonDb->info(Zend_Db_Table_Abstract::COLS);
		if (in_array($name, array("id"))) {
			throw new Exception("Cannot modify the field {$name} of a season", 1);
		}
		if (in_array($name, $columns)) {
			$this->_season->$name = $value;
			try {
				$this->_season->save();
			} catch (Zend_Db_Exception $ex) {
				throw new Exception($ex->getMessage(), 1);
			}
			return true;
		}
		return false;
	}	
}

?>