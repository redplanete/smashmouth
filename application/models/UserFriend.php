<?php

class Application_Model_UserFriend {
	protected $_userFriend;
	
	public function __construct(Zend_Db_Table_Row $userFriend) {
		$this->_userFriend = $userFriend;
	}

//        public function testfunc()
//        {
//            $friendDb = new Application_Model_DbTable_UserFriend();
//            for($i=1;$i<=72;$i++)
//            {
//                $identity['user_id']=$i;
//                $friendDb->insert($identity);
//            }
//
//        }
	
	public static function createUserFriend($data) {
		$log = Zend_Registry::get("log");
		$friendDb = new Application_Model_DbTable_UserFriend();
		foreach (array("user_id", "friend_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $friendDb->info(Zend_Db_Table_Abstract::COLS);
		$userFriend = null;
		try {
			$userFriend = $userDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$userFriend->$column = $data[$column];
				}
			}
			$userFriend->save();
			return new self($userFriend);
		} catch (Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			$userFriend->delete();
			if (strisstr($ex->getMessage(), "Duplicate entry")) {
				throw new Exception("Duplicate user friend found");
			} else {
				throw new Exception("An error occured during user friend creation", 1);
                        }
		}
	}
	public static function deleteUserFriend($data) {
		$friendDb = new Application_Model_DbTable_UserFriend();
		$select = $friendDb->select();
		foreach (array("user_id", "friend_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
			$select->where("{$field} = ?", $data[$field]);
		}
		$record = $friendDb->fetchRow($select);
		if ($record) {
			$record->delete();
		}
	}
	
	public function __get($name) {
		$friendDb = new Application_Model_DbTable_UserFriend();
		$cols = $friendDb->info(Zend_Db_Table_Abstract::COLS);
		if(in_array($name, $cols)) {
			return $this->_userFriend->$name;
		}
		return false;
	}

	public function __set($name, $value) {
		$friendDb = new Application_Model_DbTable_UserFriend();
		$cols = $friendDb->info(Zend_Db_Table_Abstract::COLS);
		if(in_array($name, array('id'))) {
			throw new Exception("Cannot modify an user game {$name} value", 1);
		}
		if(in_array($name, $cols)) {
			$this->_userFriend->$name = $value;
			try {
				$this->_userFriend->save();
			} catch(Zend_Db_Exception $e) {
				throw new Exception($e->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
        

        //-----------------NEW FUNCTIONS---------------


        public static function getFriendList($id)
	{

		$userDb = new Application_Model_DbTable_UserFriend();
		$select = $userDb->select(array('friend_id'));
		$select->where('user_id = ?', $id);
		$list = $userDb->fetchRow($select);

		if ($list) {
			return new self($list);
		}
		return false;
	}
}

?>