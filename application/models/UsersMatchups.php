<?php
/**
 * The UsersMatchups, together with the UsersSmashups model are the main driving models
 * behind the SmashMouthFantasy game.  This table keeps track of score and players in 
 * a game.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_UsersMatchups extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"marquee_matchup_settings_id" => null,
			"challenge_initiator" => null,
			"type" => null,
			"home_user_id" => null,
			"away_user_id" => null,
			"sports_id" => null,
			"seasons_id" => null,
			"status" => null,
			"home_team_score" => null,
			"away_team_score" => null,
			"date_added" => null
		);
	}
	
	/**
	 * Sets the status.  If the status is being changed to completed, the score is first computed.
	 * 
	 * @param string $status
	 */
	public function setStatus($status) {
		if (($this->data['status'] != 'completed') && ($status == "completed")) {
			$this->computeScore();
		}
		
		$this->data['status'] = $status;
	}
	
	/**
	 * Ignores any attempt to manually set the score and proxies to 
	 * computation method
	 * 
	 * @see Application_Model_UsersMatchups::computeScore()
	 * @param mixed $ignored  this value is ignored
	 */
	public function setHome_team_score($ignored) {
		$this->computeScore();
	}

	/**
	 * Ignores any attempt to manually set the score and proxies to
	 * computation method
	 *
	 * @see Application_Model_UsersMatchups::computeScore()
	 * @param mixed $ignored  this value is ignored
	 */
	public function setAway_team_score($ignored) {
		$this->computeScore();
	}
	
	
	/**
	 * Automatically calculates the score once the matchup is complete
	 * 
	 * @throws My_Exception_HandledException
	 */
	public function computeScore() {
		// if already set to completed, the score should already be completed
		if ($this->status == "completed") return;
		
		if (!$this->id) throw new My_Exception_HandledException("This matchup does not exist.");
		
		$smashupsData =  array(
			"matchup" => $this,
			$this->home_user_id => array("smashups"=>array(), "players"=>Application_Model_DbTable_UsersSmashups::getSmashupArray($this->marquee_matchup_settings_id)),
			$this->away_user_id => array("smashups"=>array(), "players"=>Application_Model_DbTable_UsersSmashups::getSmashupArray($this->marquee_matchup_settings_id))
		);

		$smashupsDb = new Application_Model_DbTable_UsersSmashups();
		$smashupsDb->getSmashupsWithPlayers($smashupsData);

		// Calculate home users score
		$this->data['home_team_score'] = 0;
		foreach($smashupsData[$this->data['home_user_id']]["players"] as $position => $playersAtPosition ) {
			if ($position == "team_ids") continue;
			foreach($playersAtPosition as $player) {
				/* @var $player Application_Model_Players */
				if (!$player) continue;
				$this->data['home_team_score'] += $player->getScoreInGame($smashupsData[$this->data['home_user_id']]["smashups"][$player->id]->games_id, $this->marquee_matchup_settings_id);
			}
		}
	
		// Calculate away users score
		$this->data['away_team_score'] = 0;
		foreach($smashupsData[$this->data['away_user_id']]["players"] as $position => $playersAtPosition ) {
			if ($position == "team_ids") continue;
			foreach($playersAtPosition as $player) {
				/* @var $player Application_Model_Players */
				if (!$player) continue;
				$this->data['away_team_score'] += $player->getScoreInGame($smashupsData[$this->data['away_user_id']]["smashups"][$player->id]->games_id, $this->marquee_matchup_settings_id);
			}
		}
	}

}

