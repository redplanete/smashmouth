<?php
/**
 * The MarqueeMatchupSettings keeps track of admin selected games from which smashups will be selected
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_MarqueeMatchupSettings extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"seasons_id" => null,
			"smashup_open_date" => null,
			"smashup_close_date" => null,
			"number_of_matchups" => null,
			"positions" => null,
			"score_weights" => null
		);
	}
}

?>