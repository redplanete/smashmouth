<?php
/**
 * Mapper for the EventsTypes Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_EventsTypesMapper extends Application_Model_AbstractMapper
{
	/**
	 * Gets an EventsType object by its unique name
	 * 
	 * @param string $name  						The unique name of the EventsType
	 * @return Application_Model_EventsTypes|null	The EventsType object if found, otherwise null
	 */
	public function getEventsTypeByName( $name ) {
		$eventsTypesDb = $this->getDbTable();
		
		$select = $eventsTypesDb->select()
					->where("name = ?", $name );
		
		$matchingEventType = $eventsTypesDb->fetchRow($select);
		
		return $matchingEventType ? $this->getModel($matchingEventType->toArray()) : null;
	}
}

