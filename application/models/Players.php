<?php
/**
 * This model holds data that keeps the rosters of all teams.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_Players extends Application_Model_AbstractModel
{
	private $score = null;
	
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"uuid" => null,
			"name" => null,
			"position" => null,
			"teams_id" => null,
			"asset_id" => null,
			"active" => null,
			"date_updated" => null
		);
	}

	/**
	 *   Gets the team name of this player
	 */
	public function getTeamName() {
		return Application_Model_DbTable_Teams::getMapper()->find($this->teams_id)->name;
	}
	
	/**
	 *   Gets the team of this player
	 *   
	 *   @return Application_Model_Teams
	 */
	public function getTeam() {
		return Application_Model_DbTable_Teams::getMapper()->find($this->teams_id);
	}
	
	/**
	 *  Get the sport of this player
	 *  
	 *  @return Application_Model_Sports
	 */
	public function getSport() {
		return Application_Model_DbTable_Sports::getMapper()->find($this->getTeam()->sports_id);
	}
	
	/**
	 * Sets this players position to "SG", "PG", or "F/C"
	 * 
	 * @param $position string	"SG"|"PG"|"F/C"
	 */
	public function setPosition( $position ) {
		
		switch($position) {
			case "PG":
			case "SG":
				$this->data["position"] = $position;
				break;
			default:
				$this->data["position"] = "F/C";
		}
		
	}
	
	/**
	 * Computes the score for this player in the given game
	 * 
	 * @param $games_id		int    The idea of the game that we want the players score for
	 * @param $settings_id  int	   The id of the settings for which the score will be computed.
	 * 
	 * @return int the players cumlutative score for the game
	 */
	public function getScoreInGame($games_id, $settings_id) {
		
		if ($this->score) return $this->score;
		
		$playerStatsDb = new Application_Model_DbTable_GamePlayerStats();
		$select = $playerStatsDb->select()
						->where("games_id = ?", $games_id)
						->where("players_id = ?", $this->id);

		$playerStats = $playerStatsDb->getModelsFromRows($playerStatsDb->fetchAll($select));
		
		$score = 0;
		$valueWeights = json_decode(Application_Model_DbTable_MarqueeMatchupSettings::getMapper()->find($settings_id)->score_weights, true);	
		foreach($playerStats as $playerStat) {
			$score += $playerStat->value * $valueWeights[$playerStat->stat];
		}
		
		$this->score = $score;
		
		return $score;
	}
	
	/**
	 * Returns the to-date stats for this player
	 * 
	 * @param int $seasons_id  The season for which we are interested in this players stats
	 * @return array[Application_Model_SeasonPlayerStats]  a associative array where the stat name is the key and the value is the Application_Model_SeasonPlayerStats 
	 */
	public function getSeasonStats($seasons_id) {
		$seasonPlayerStatMapper = Application_Model_DbTable_SeasonPlayerStats::getMapper();
		/* @var $seasonPlayerStatMapper Application_Model_SeasonPlayerStatsMapper */
		
		$statModels = $seasonPlayerStatMapper->findBySeasonAndPlayer($seasons_id, $this->id);
		
		$stats = array();
		foreach($statModels as $statModel) {
			$stats[$statModel->stat] = $statModel;
		}
		
		return $stats;
	}
}
