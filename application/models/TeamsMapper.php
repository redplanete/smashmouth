<?php
/**
 * Mapper for the Teams Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_TeamsMapper extends Application_Model_AbstractMapper
{
	/**
	 * Finds and populates the team by uuid
	 *
	 * @param string $id  The uuid of the team to be retrieved
	 *
	 * @return Application_Model_Players|null
	 */
	public function findByUuid($uuid)
	{
		if ( is_null($uuid) ) return null;
	
		$select = $this->getDbTable()->select();
		$select->where("uuid = ?", $uuid);
	
		$row = $this->getDbTable()->fetchRow($select);
	
		if (!$row) {
			return null;
		}
	
		return $this->getModel()->setData($row->toArray());
	}
}

