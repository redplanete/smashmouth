<?php
/**
 * Mapper for the Players Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_PlayersMapper extends Application_Model_AbstractMapper
{
	/**
	 * Finds and populates the player by uuid
	 *
	 * @param string $uuid  The uuid of the player to be retrieved
	 *
	 * @return Application_Model_Players|null
	 */
	public function findByUuid($uuid)
	{
		if ( is_null($uuid) ) return null;
	
		$select = $this->getDbTable()->select();
		$select->where("uuid = ?", $uuid);
		
		$row = $this->getDbTable()->fetchRow($select);
		
		if (!$row) {
			return null;
		}
	
		return $this->getModel()->setData($row->toArray());
	}
}

