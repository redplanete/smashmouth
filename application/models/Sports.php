<?php
/**
 * The Sports Model holds data that identifies which sports SmashmouthFantasy is supporting.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_Sports extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"league" => null,
			"sport" => null,
			"active" => null,
			"current_season" => null,
			"default_positions" => null,
			"default_score_weights" => null
		);
	}

}

