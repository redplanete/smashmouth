<?php
/**
 * Mapper for the EventsData Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_EventsDataMapper extends Application_Model_AbstractMapper
{
	
	/**
	 * Returns all name/value pairs of event data for a given events_id
	 *
	 * @param string $events_id  The uuid of the team to be retrieved
	 *
	 * @return array  An associative array of name paired to value of the EventData rows
	 */
	public function findDataByEventsId($events_id)
	{
		if ( is_null($events_id) ) return null;
	
		$select = 
			$this->getDbTable()
				->select()
				->where("events_id = ?", $events_id);
	
		return $this->getDbTable()->getDataFromRows( $this->getDbTable()->fetchAll($select) );
	}
}

