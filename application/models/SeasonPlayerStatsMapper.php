<?php
/**
 * Mapper for the SeasonPlayerStats model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_SeasonPlayerStatsMapper extends Application_Model_AbstractMapper
{
	/**
	 * Finds and populates the stats by the season's id and player's id
	 *
	 * @param int $seasons_id  		id of the game of interest
	 * @param int $players_id	 	id of the player of interest
	 *
	 * @return array[SeasonPlayerStats]
	 */
	public function findBySeasonAndPlayer($seasons_id, $players_id)
	{
		if ( is_null($seasons_id) || is_null($players_id)) return null;
	
		$select = 
			$this->getDbTable()->select()
				->where("seasons_id = ?", $seasons_id)
				->where("players_id = ?", $players_id);
	
		$rows = $this->getDbTable()->fetchAll($select);
	
		return $this->getDbTable()->getModelsFromRows($rows);
	}
}

