<?php

class Application_Model_MatchupStatsNBA
{
	protected $_stats;

	public function __construct(Zend_Db_Table_Row $stats)
	{
		$this->_stats = $stats;
	}

	public static function createStat($data) {
		$log = Zend_Registry::get('log');
		$statDb = new Application_Model_DbTable_MatchupStatsNBA();
		foreach (array("season_id", "match_id", "team_id", "team_name", "player_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $statDb->info(Zend_Db_Table_Abstract::COLS);
		try {
			$stats = $statDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$stats->$column = $data[$column];
				}
			}
			$stats->save();
			return new self($stats);
		} catch(Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			$stats->delete();
			if (stristr($ex->getMessage(), 'Duplicate entry')) {
				throw new Exception("Duplicate nba team stats founds");
			} else {
				throw new Exception("An error occured during player creation\n" . $ex->getMessage(), 1);
			}
		}
		return null;
	}
	public static function getPlayerStats($data) {
		$statDb = null;
		$sportID = (isset($data["sport_id"]) && !empty($data["sport_id"]) ? intval($data["sport_id"]) : 1);
		switch ($sportID) {
			case 1: $statDb = new Application_Model_DbTable_MatchupStatsNBA(); break;
		}
		$select = $statDb->select();
		foreach (array("season_id", "match_id", "team_id", "player_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty");
			}
			$select->where("{$field} = ?", $data[$field]);
		}
		$record = $statDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}
	public static function getPlayerScore($data) {
		$statDb = null;
		$sportID = (isset($data["sport_id"]) && !empty($data["sport_id"]) ? intval($data["sport_id"]) : 1);
		switch ($sportID) {
			case 1: $statDb = new Application_Model_DbTable_MatchupStatsNBA(); break;
		}
		$select = $statDb->select();
		foreach (array("season_id", "match_id", "team_id", "player_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty");
			}
			$select->where("{$field} = ?", $data[$field]);
		}
		$record = $statDb->fetchRow($select);
		if ($record) {
			$stats = new self($record);
			$score = 0;
			foreach ($stats as $stat) {
				$score += $stat->points;
				$score += $stat->off_rebounds;
				$score += $stat->def_rebounds;
				$score += $stat->assists;
				$score += $stat->steals;
				$score += $stat->blocked_shots;
				$score -= ($stat->turnovers + $stat->fouls);
			}
			return $score;
		}
	}

	public function __get($name)
	{
		$playerDb = new Application_Model_DbTable_MatchupStatsNBA();
		$cols = $playerDb->info(Zend_Db_Table_Abstract::COLS);
		if(in_array($name, $cols)) {
			return $this->_stats->$name;
		}
		return false;
	}

	public function __set($name, $value)
	{
		$playerDb = new Application_Model_DbTable_MatchupStatsNBA();
		$cols = $playerDb->info(Zend_Db_Table_Abstract::COLS);
		if(in_array($name, array('id'))) {
			throw new Exception("Cannot modify a player {$name} value", 1);
		}
		if(in_array($name, $cols)) {
			$this->_stats->$name = $value;

			try {
				$this->_stats->save();
			} catch(Zend_Db_Exception $e) {
				throw new Exception($e->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}

