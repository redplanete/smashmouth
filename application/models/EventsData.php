<?php
/**
 * This represents the data associated with an event type.  It allows for more
 * flexibility in adding and changing event types.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_EventsData extends Application_Model_AbstractModel
{

	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = 	array(
			"id" => null,
			"events_id" => null,
			"event_types_id" => null,
			"name" => null,
			"value" => null
		);
	}
	
}

