<?php
/**
 * This is a model that will eventual represent League Play.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_League extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"name" => null,
			"max_players" => null
		);
	}

}

