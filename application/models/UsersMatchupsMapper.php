<?php
/**
 * Mapper for the UsersMatchups Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_UsersMatchupsMapper extends Application_Model_AbstractMapper
{
	/**
	 * Creates events while persisting to the database
	 * 
	 * @see Application_Model_AbstractMapper::save()
	 */
	public function save(Application_Model_AbstractModel $usersMatchups) {
		
		// The system will bypass the security checks.
		if (PHP_SAPI == 'cli') {
			return parent::save($usersMatchups);
		}
		
		$auth = Zend_Auth::getInstance();
		$currentId = $auth->getIdentity()->id;
		$request = Zend_Controller_Front::getInstance()->getRequest();

		$is_new = empty($usersMatchups->id) ? true : false;

		$response = null;
		
		if ($is_new) {
			// all matchups must start out as a user initiated request.  No other state is valid.
			if ($usersMatchups->status != "requested") throw new My_Exception_HandledException("Access Denied: All matchups must start by initiating a request to the other user.");
			
			$response = parent::save($usersMatchups);
			
			$sender_user = Application_Model_DbTable_Users::getMapper()->find($usersMatchups->challenge_initiator);
			$sport = Application_Model_DbTable_Sports::getMapper()->find($usersMatchups->sports_id);
			
			$eventsData = array("sender_users_id" => $sender_user->id, "sender_name" => $sender_user->getName(), "sport_name" => $sport->league, "users_matchups_id" => $usersMatchups->getId());
			$events_array = Application_Model_DbTable_Events::executeEvent($usersMatchups->away_user_id, "matchup_invitation", $eventsData);
		} else {
			$oldValues = $this->find($usersMatchups->id);
			$oldUsers = array($usersMatchups->home_user_id, $usersMatchups->away_user_id);
			
			if (!in_array($currentId, $oldUsers)){
				// Make sure current user is in matchup
				throw new My_Exception_HandledException("Access Denied: You are not a player in this matchup.");
			}
			
			// assure that all static values cannot changed by user
			$static_values = array("challenge_initiator","type","sports_id","seasons_id","marquee_matchup_settings_id","home_team_score","home_user_id", "away_user_id", "away_team_score","date_added");		
			foreach($static_values as $attribute) {
				if ($oldValues->$attribute != $usersMatchups->$attribute)
					throw new My_Exception_HandledException("Access Denied: You cannot change the $attribute.");
			}
			
			// now assure that the status is moving from one state to another in a valid way
			// handle all logic including firing events.
			switch ($usersMatchups->status) {
				case 'completed':
				case 'requested':
				case 'in_progress':
					// users cannot make these changes.  only the system.
					throw new My_Exception_HandledException("Access Denied.");
				case 'accepted':
					if ($oldValues->status != 'requested') {
						// you can only accept from a state of 'requested'				
						throw new My_Exception_HandledException("Access Denied: You can not restart this matchup.  You must request a new one.");
					}
					
					if ($currentId != $oldValues->away_user_id) {
						// the currentUser must be the recipient of the invitation to accept
						throw new My_Exception_HandledException("Access Denied: You are not the recipient of this match, and therefore can not accept it.");
					}
					
					// otherwise, the request was accepted and we randomize home and away teams and 
					// fire an invitation accepted event
					shuffle($oldUsers);
					$usersMatchups->home_user_id = $oldUsers[0];
					$usersMatchups->away_user_id = $oldUsers[1];
					
					$response = parent::save($usersMatchups);
						
					$receiver_user = $auth->getIdentity()->getUser();
					$sport = Application_Model_DbTable_Sports::getMapper()->find($usersMatchups->sports_id);
						
					$eventsData = array("receiver_users_id" => $currentId, "receiver_name" => $receiver_user->getName(), "sport_name" => $sport->league, "users_matchups_id" => $usersMatchups->getId());
					$events_array = Application_Model_DbTable_Events::executeEvent($usersMatchups->challenge_initiator, "matchup_invitation_accepted", $eventsData);
					break;
				case 'declined':
					if ($oldValues->status != 'requested') {
						// you can only declined from a state of 'requested'
						throw new My_Exception_HandledException("Access Denied: It is too late to decline this matchup.  If the games have not already started, you may try canceling the matchup.");
					}
					
					if ($currentId != $oldValues->away_user_id) {
						// the currentUser must be the recipient of the invitation to accept
						throw new My_Exception_HandledException("Access Denied: You are not the recipient of this match, and therefore can not accept it.");
					}
					// otherwise, the request was declined and can be saved
					$response = parent::save($usersMatchups);
					break;
				case 'cancelled':
					if ( !in_array( $oldValues->status, array('requested','accepted')) ) {
						// you can only cancel from a state of 'requested' and 'accepted'
						throw new My_Exception_HandledException("Access Denied: This matchup can no longer be cancelled.");
					}	
					if ($oldValues->status == 'requested') {
						// only the initiator can cancel a 'requested'
						if ($currentId != $oldValues->challenge_initiator) 
							throw new My_Exception_HandledException("Access Denied: Only the initiator of this invite can cancel it.  However, you may decline matchup invitations sent to you.");
						
						$response = parent::save($usersMatchups);					
					} else {
						// the matchup was formerly accepted.  We will trigger an event to 
						// inform the other party that the matchup has been cancelled.
						$response = parent::save($usersMatchups);
						
						$canceling_user = $auth->getIdentity()->getUser();
						$other_user_array = array_diff($oldUsers, array($currentId));
						$other_user_id = $other_user_array[0];
						$sport = Application_Model_DbTable_Sports::getMapper()->find($usersMatchups->sports_id);
						
						$eventsData = array("canceling_user_id" => $currentId, "canceling_user_name" => $canceling_user->getName(), "sport_name" => $sport->league, "users_matchups_id" => $usersMatchups->getId());
						$events_array = Application_Model_DbTable_Events::executeEvent($other_user_id, "matchup_cancelled", $eventsData);
					}
					break;
				default:
					throw new My_Exception_HandledException("Invalid Invitation Response.");
			}

		}
		return $response;
	}
}
