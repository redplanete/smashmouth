<?php
/**
 * The UsersMessages Model represents the conversations between users.  It will be tied to
 * both e-mail and on-site chat and messages.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_UsersMessages extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"from_users_id" => null,
			"to_users_id" => null,
			"message" => null,
			"time_sent" => null,
			"time_read" => null
		);
	}
}

