<?php

class Application_Model_Matchup
{
	protected $_matchup;

	public function __construct(Zend_Db_Table_Row $matchup)
	{
		$this->_matchup = $matchup;
	}

	public static function createMatchup($data)
	{
		$log = Zend_Registry::get("log");
		$matchupDb = new Application_Model_DbTable_Matchup();
		foreach (array("sport_id", "match_id", "season_id", "season_week", "round", "date", "home_team_id", "away_team_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $matchupDb->info(Zend_Db_Table_Abstract::COLS);
		$matchup = null;
		try {
			$matchup = $matchupDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$matchup->$column = $data[$column];
				}
			}
			$matchup->save();
			return new self($matchup);
		} catch (Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			$matchup->delete();
			if (stristr($ex->getMessage(), "Duplicate entry")) {
				throw new Exception("Duplicate matchup found");
			} else {
				throw new Exception("An error occured during matchup creation", 1);
			}
		}
		return null;
	}
	public static function deleteMatchup($data)
	{
		$matchupDb = new Application_Model_DbTable_Matchup();
		$select = $matchupDb->select();
		foreach (array("sport_id", "match_id", "season_id") as $field) {
			if (!isset($data[$field])) {
				throw new Exception("{$field} missing");
			}
			$select->where("{$field} = {$data[$field]}");
		}
		$hasHomeTeam = isset($data["home_team_id"]);
		$hasAwayTeam = isset($data["away_team_id"]);
		if ($hasHomeTeam && $hasAwayTeam) {
			$select->where("home_team_id = {$data["home_team_id"]} and away_team_id = {$data["away_team_id"]}");
		} else if ($hasHomeTeam) {
			$select->where("home_team_id = {$data["home_team_id"]}");
		} else if ($hasAwayTeam) {
			$select->where("away_team_id = {$data["away_team_id"]}");
		}
		$record = $matchupDb->fetchRow($select);
		if ($record) {
			$record->delete();
			return true;
		}
		return false;
	}
	public static function getMatchup($data)
	{
		$matchupDb = new Application_Model_DbTable_Matchup();
		$select = $matchupDb->select();
		foreach (array("sport_id", "match_id", "season_id") as $field) {
			if (!isset($data[$field])) {
				throw new Exception("{$field} missing");
			}
			$select->where("{$field} = {$data[$field]}");
		}
		$record = $matchupDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}
	public static function matchupExists($team1, $team2, $sportID, $seasonID) {
		$matchupDb = new Application_Model_DbTable_Matchup();
		$tableName = $matchupDb->info(Zend_Db_Table::NAME);
		$teamID1 = 0;
		$teamID2 = 0;
		if (is_numeric($team1)) {
			$teamID1 = $team1;
		} else {
			$teamID1 = $team1->team_id;
		}
		if (is_numeric($team2)) {
			$teamID2 = $team2;
		} else {
			$teamID2 = $team2->team_id;
		}
		$select = $matchupDb->select();
		$select->where("sport_id = {$sportID}");
		$select->where("season_id = {$seasonID}");
		$select->where("((home_team_id = {$teamID1} and away_team_id = {$teamID2}) or (home_team_id = {$teamID2} and away_team_id = {$teamID1}))");
		if ($matchupDb->fetchRow($select)) {
			return true;
		}
		return false;
	}

	public function __get($name)
	{
		$matchupDb = new Application_Model_DbTable_Matchup();
		$cols = $matchupDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, $cols))
		{
			return $this->_matchup->$name;
		}

		return false;
	}

	public function __set($name, $value)
	{
		$matchupDb = new Application_Model_DbTable_Matchup();
		$cols = $matchupDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, array('id')))
		{
			throw new Exception("Cannot modify an matchup {$name} value", 1);
		}

		if(in_array($name, $cols))
		{
			$this->_matchup->$name = $value;

			try {
				$this->_matchup->save();
			} catch(Zend_Db_Exception $e)
			{
				throw new Exception($e->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}

?>