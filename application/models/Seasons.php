<?php
/**
 * This model is responsible for keep track of season dates which dictate what parameters
 * to send to sports data provider.
 * 
 * @author Leon McCottry
 */
class Application_Model_Seasons extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"sports_id" => null,
			"year" => null,
			"preseason_start_date" => null,
			"regular_season_start_date" => null,
			"postseason_start_date" => null,	 
			"season_end_date" => null,
			"date_added" => null
		);
	}

}

