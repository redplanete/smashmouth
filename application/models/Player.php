<?php

class Application_Model_Player
{
	protected $_player;

	public function __construct(Zend_Db_Table_Row $player)
	{
		$this->_player = $player;
	}

	public static function createPlayer($data)
	{
		$log = Zend_Registry::get('log');
		$guardDb = new Application_Model_DbTable_GuardUpdates();
		$playerDb = new Application_Model_DbTable_Player();
		foreach (array("sport_id", "player_id", "name", "position", "team_id", "season_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $playerDb->info(Zend_Db_Table_Abstract::COLS);
		try {
			$player = $playerDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$player->$column = $data[$column];
				}
			}

			$gselect = $guardDb->select();
			$gselect->where("guard_name = ?", $player->name);
			$grecord = $guardDb->fetchRow($gselect);

			if ($grecord) {
				$player->position = $grecord->guard_position;
			}

			$player->save();
			return new self($player);
		} catch(Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			$player->delete();
			if (stristr($ex->getMessage(), 'Duplicate entry')) {
				//duplicate filter
				$select = $playerDb->select();
				$select->where('player_id = ?', $data["player_id"]);
				$record = $playerDb->fetchRow($select);
				if ($record) {
					$isEmpty = empty($record->join_date);
					$isNew = (strtotime($record->join_date) < strtotime($data["join_date"]));
					$retValue = null;
					if ($isEmpty || $isNew) {
						$record->delete();
					}
					if (!$isEmpty) {
						$retValue = createPlayer($data);
					}
					return $retValue;
				}
			} else {
				throw new Exception("An error occured during player creation", 1);
			}
		}
		return null;
	}

	public static function deletePlayer($data, $sportID) {
		$playerDb = new Application_Model_DbTable_Player();
		$select = $playerDb->select();
		$select->where("sport_id = {$sportID}");
		$requiredCount = 0;
		if (isset($data["player_id"])) {
			$select->where("player_id = {$data["player_id"]}");
			$requiredCount++;
		}
		if (isset($data["name"])) {
			$select->where("name = {$data["name"]}");
			$requiredCount++;
		}
		if ($requiredCount == 0) {
			throw new Exception("missing required parameter player_id or name.");
		}
		$record = $playerDb->fetchRow($select);
		if ($record) {
			$record->delete();
		}
	}
	public static function getPlayer($id) {
		$playerDb = new Application_Model_DbTable_Player();
		$select = $playerDb->select();
		$select->where('player_id = ?', $id);
		$record = $playerDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}

	public static function getPlayers() {
		$playerDb = new Application_Model_DbTable_Player();
		$select = $playerDb->select();
		$records = $playerDb->fetchAll($select);
		$players = array();
		if ($records) foreach ($records as $r) {
			$players[] = new self($r);
		}
		return $players;
	}

	public static function filterPlayersByPosition($players, $position) {
		$filtered = array();
		if ($players) foreach ($players as $p) {
			if ($p->position == $position) {
				$filtered[] = $p;
			}
		}
		return $filtered;
	}

	public static function getPlayersFromPicks($picks) {
		if (is_array($picks)) {
			$players = array();
			if ($picks) foreach ($picks as $p) {
				$players[] = Application_Model_Player::getPlayer($p->pick_id);
			}
			return $players;
		} else if (get_class($picks) == Application_Model_UserPick) {
			$pickPlayer = Application_Model_Player::getPlayer($picks->pick_id);
		}
		return false;
	}

	public function getPlayerMatchups(Application_Model_UserGame $game) {
		$marquisData = array();
		$marquisData["sport_id"] = $game->sport_id;
		$marquisData["season_id"] = $game->season_id;
		$marquisMatchups = Application_Model_MarquisMatchup::getAllMarquisMatchups($marquisData);
		$matchups = array();
		foreach ($marquisMatchups as $marquisMatchup) {
			$matchupData = array();
			$matchupData["sport_id"] = $marquisMatchup->sport_id;
			$matchupData["match_id"] = $marquisMatchup->match_id;
			$matchupData["season_id"] = $marquisMatchup->season_id;
			$matchups[] = Application_Model_Matchup::getMatchup($matchupData);
		}
		$records = array();
		foreach ($matchups as $matchup) {
			if ($matchup->home_team_id == $this->getTeam()->getId()) {
				$targetTeam = Application_Model_Team::getTeam($matchup->away_team_id);
				$records = $targetTeam->getPlayers(array(
					"sport_id" => $game->sport_id,
					"position" => $this->position
				));
				break;
			} else if ($matchup->away_team_id == $this->getTeam()->getId()) {
				$targetTeam = Application_Model_Team::getTeam($matchup->home_team_id);
				$records = $targetTeam->getPlayers(array(
					"sport_id" => $game->sport_id,
					"position" => $this->position
				));
				break;
			}
		}
		return $records;
	}

	public function getId()
	{
		return $this->_player->player_id;
	}

	public function getName()
	{
		return $this->_player->name;
	}

	public function getTeam() {
		return Application_Model_Team::getTeam($this->_player->team_id);
	}

	public function getNewTeam() {
		return Application_Model_Team::getTeam($this->_player->new_team_id);
	}

	public function getStatsForSeason($seasonId) {
		$stats = Application_Model_PlayerStatsNBA::getPlayerStats(array(
			"sport_id" => $this->_player->sport_id,
			"season_id" => $seasonId,
			"team_id" => $this->_player->team_id,
			"player_id" => $this->_player->player_id
		));

		return $stats;
	}
	public function getStatsForMatch($matchID) {
		$stats = Application_Model_MatchStatsNBA::getPlayerStats(array(
			"sport_id" => $this->sport_id,
			"match_id" => $matchID,
			"season_id" => 3,
			"team_id" => $this->team_id,
			"player_id" => $this->player_id
		));
		return $stats;
	}

	public function __get($name)
	{
		$playerDb = new Application_Model_DbTable_Player();
		$cols = $playerDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, $cols))
		{
			return $this->_player->$name;
		}

		return false;
	}

	public function __set($name, $value)
	{
		$playerDb = new Application_Model_DbTable_Player();
		$cols = $playerDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, array('id')))
		{
			throw new Exception("Cannot modify a player {$name} value", 1);
		}

		if(in_array($name, $cols))
		{
			$this->_player->$name = $value;

			try {
				$this->_player->save();
			} catch(Zend_Db_Exception $e)
			{
				throw new Exception($e->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}

