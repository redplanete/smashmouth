<?php

/** 
 * This class handles storing and retrieving data to and from the DbTable
 * 
 * @author Leon McCottry
 */
abstract class Application_Model_AbstractMapper {
	
	/**
	 * @var string The identifier of the underlying database table and model classes
	 */
	protected $model_name;
	
	/**
	 * @var Zend_Db_Table_Abstract The table which persist to the database
	 */
	private $db_table;
	
	/**
	 * the constructor determines the name of the model via introspection
	 * 
	 */
	public function __construct( Application_Model_DbTable_AbstractTable $db_table = null ) {
		$this->model_name = substr_replace(substr_replace(get_class($this), '', 0, 18), '', -6);
		if ( isset($db_table) ) {
			$this->setDbTable($db_table);
		} else {
			$this->setDbTable("Application_Model_DbTable_".$this->model_name);
		}
	}
	
	/**
	 * Sets the database table for this modell
	 * 
	 * @param Zend_Db_Table_Abstract|string $db_table     The db table instance or the name of the class
	 * @throws Exception                                  throws exception if the class is not a Zend_Db_Table_Abstract or of the proper subtype
	 * @return Application_Model_AbstractMapper           returns this mapper on success
	 */
	public function setDbTable($db_table)
	{
		$expected_class = "Application_Model_DbTable_".$this->model_name;
		
		if (is_string($db_table)) {
			$db_table = new $db_table();
		}
		
		if (!$db_table instanceof Zend_Db_Table_Abstract ) {
			throw new Exception('Invalid DbTable data class provided: '.get_class($db_table).".  Expected a subclass of Zend_Db_Table_Abstract.");
		}		
		if (get_class($db_table) !=  $expected_class ) {
			throw new Exception('Invalid DbTable data class provided: '.get_class($db_table).".  Expected: $expected_class");
		}
		
		$this->db_table = $db_table;
		return $this;
	}
	
	/**
	 * Gets the database table gateway
	 * 
	 * @return Application_Model_DbTable_AbstractTable  The database table proxy
	 */
	public function getDbTable()
	{
		if (null === $this->db_table) {
			$this->setDbTable('Application_Model_DbTable_'.$this->model_name);
		}
		return $this->db_table;
	}
	
	
	/**
	 * Saves the content of a Model to the database
	 * 
	 * @param Application_Model_AbstractModel $model
	 * 
	 * @return mixed  returns the primary key(s) of the saved object if it was and insert
	 *                for an update, the number of rows updated is returned
	 */
	public function save(Application_Model_AbstractModel $model)
	{
		$data = $model->getData();
		
		$id = $model->getId();
		
		if (null === $id ) {
			unset($data['id']);
			$id = $this->getDbTable()->insert($data);
			$model->id = $id;
			return $id;
		} else {
			return $this->getDbTable()->update($data, array('id = ?' => $id));
		}
	}
	
	/**
	 * Finds and populates the model with data from the database based on the primary key
	 * 
	 * @param string|integer|array $id  The primary key value of the model data to retrieve
	 * 
	 * @return Application_Model_AbstractModel|null
	 */
	public function find($id)
	{
		if ( is_null($id) ) return null;

		$result = $this->getDbTable()->find($id);
		if (0 == $result->count()) {
			return null;
		}
		
		$row = $result->current();
		return $this->getModel()->setData($row->toArray());
	}
	

	/**
	 * Retrieves all rows of data from the database as an array of models
	 * 
	 * @return array  An array of all rows of data in the database for this model
	 */
	public function fetchAll()
	{
		$resultSet = $this->getDbTable()->fetchAll();
		$entries   = array();
		foreach ($resultSet as $row) {
			$model_name = "Application_Model_".$this->getModelName();
			$entry = new $model_name();
			
			$entry->setData($row->toArray());
			
			$entries[] = $entry;
		}
		return $entries;
	}	
	
	/**
	 *  A string that indentifies the model which the mapper maps.  It is used to reflectively construct the corresponding Model and DbTable
	 */
	public function getModelName() {
		return $this->model_name;
	}
	
	/**
	 * Creates model of this mappers type and populates it with data if provided
	 * 
	 * @var $data array  An array of data to populate this model
	 * @return Application_Model_AbstractModel  The model which corresponds to this Mapper
	 */
	public function getModel(array $data = array()) {
		$model_class = "Application_Model_".$this->getModelName(); 

		/* @var $model Application_Model_AbstractModel */
		$model = new $model_class();

		if (!empty($data)) {
			$model->setData($data);
		}
		
		return $model;
	}
}

?>