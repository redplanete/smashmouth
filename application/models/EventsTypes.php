<?php
/**
 * This represents an expandable array of event types for the real-time 
 * notifications system
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_EventsTypes extends Application_Model_AbstractModel
{

	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = 	array(
			"id" => null,
			"name" => null,
			"description" => null,
			"notification_message" => null,
			"data_fields" => null,
			"instructions" => null
		);
	}
	
}

