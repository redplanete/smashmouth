<?php
/**
 * Games represent the NBA schedule.  It is here that games are designated as marquee matchups
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_Games extends Application_Model_AbstractModel
{

	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = 	array(
			"id" => null,
			"sports_id" => null,
			"uuid" => null,
			"seasons_id" => null,
			"home_team_id" => null,
			"away_team_id" => null,
			"scheduled_date" => null,
			"date_added" => null,
			"home_team_score" => null,
			"away_team_score" => null,
			"status" => null,
			"period" => null,
			"time" => null
		);
	}

	/**
	 *   Gets the home team of this game
	 *   
	 *   @return Application_Model_Teams
	 */
	public function getHomeTeam() {
		return Application_Model_DbTable_Teams::getMapper()->find($this->home_team_id);
	}
	
	/**
	 *   Gets the away team of this game
	 *   
	 *   @return Application_Model_Teams 
	 */
	public function getAwayTeam() {
		return Application_Model_DbTable_Teams::getMapper()->find($this->away_team_id);
	}
	
	
	public function isMarqueeMatchup($marquee_matchup_settings_id) {
		return (bool) Application_Model_DbTable_MarqueeMatchups::getMapper()->findByGameIdAndSettingsId($this->id, $marquee_matchup_settings_id);
	}
}

