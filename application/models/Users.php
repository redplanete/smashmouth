<?php
/**
 * The Users Model represents the login information and account status of a customer.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_Users extends Application_Model_AbstractModel
{
	
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"status" => null,
			"user_type" => null,
			"login_name" => null,
			"email" => null, 	 	 
			"login_pass" => null, 
			"first_name" => null,
			"last_name" => null,
			"change_pass_key" => null,
			"date_added" => null,
			"invite_link" => null
		);
	}
	
	/**
	 * Gets the first and last name of this user
	 * 
	 * @return string the first and last name of the user as a single string
	 */
	public function getName() {
		return $this->login_name;
	}
}

