<?php
/**
 * The central utility class used to manage many elements of the user's account  
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_Account implements Zend_Acl_Role_Interface
{
	/**
	 * The user's data
	 * 
	 * @var Application_Model_Users
	 */
    protected $_user;
    
    /**
     * Static storage of this account for rapid access
     * 
     * @var array[Application_Model_Account]
     */
    protected static $_cache = array();
    
    /**
     *  Creates an Account object from user data and stores itself in a cache
     *  
     * @param Application_Model_Users $user
     */
    public function __construct(Application_Model_Users $user)
    {
        $this->_user = $user;
        self::$_cache[$this->_user->getId()] = $this;
    }
    
    /**
     *  Actions to perform on object after it is deserialized.
     *  This is a magic method.
     */
    public function __wakeup()
    {
        if ($this->_user) {
            // $table = new Application_Model_DbTable_Users();
            // $this->_user->setTable($table);
            
            // $verifyUser = self::getAccount($this->_user->id);
            // unset($verifyUser);
        }       
    }
    
    /**
     * Checks if a account already exist using the same e-mail
     * 
     * @param array $data
     * @return boolean   true if the account exist, otherwise false
     */
    public static function accountExists($data)
    {
    	if (empty($data['email'])) return false;
    	
        $userDb = new Application_Model_DbTable_Users();
        $select = $userDb->select()
        			->where("email = ?", $data['email']);

        return !($userDb->fetchRow($select) === null);
    }

    /**
     * Checks if a account already exist using the same e-mail
     *
     * @param array $data
     * @return boolean   true if the account exist, otherwise false
     */
    public static function loginNameExists($data)
    {
    	if (empty($data['login_name'])) return false;
    	 
    	$userDb = new Application_Model_DbTable_Users();
    	$select = $userDb->select()
    	->where("login_name = ?", $data['login_name']);
    
    	return !($userDb->fetchRow($select) === null);
    }    
    
    /**
     * Returns and array of this accounts data
     * 
     * @return array
     */
    public function toArray()
    {
        $data         = $this->_user->toArray();
        $data['name'] = $this->getName();
        return $data;
    }
    
    /**
     * Returns a json representation of this user's account details
     * 
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
    
    /**
     * (non-PHPdoc)
     * @see Zend_Acl_Role_Interface::getRoleId()
     */
    public function getRoleId()
    {
        return $this->getId();
    }
    
    /**
     * Returns the account user's id
     * 
     * @return int   The id of user of this account
     */
    public function getId()
    {
        return $this->_user->id;
    }

    /**
     * Returns the user row data of this account
     *
     * @return Application_Model_Users
     */
    public function getUser()
    {
    	return $this->_user;
    }
        
    /**
     * Returns the name of the user of this Account
     * 
     * @return string
     */
    public function getName()
    {
        $name = "";
        if (!empty($this->_user->first_name) && !empty($this->_user->last_name)) {
            $name = $this->_user->first_name . " " . $this->_user->last_name;
        } else if (isset($this->_user->first_name)) {
            $name = $this->_user->first_name;
        } else if (isset($this->_user->last_name)) {
            $name = $this->_user->last_name;
        }
        return $name;
    }
    
    
    /**
     * Gets the challenges either sent to or made by the this user
     * 
     * @param string $id_label  a label of 'away_user_id' means this user was challenged,
     * 							a label of 'home_user_id' means that this user initiated the challenge. 
     * @return multitype:Application_Model_UsersMatchups
     */
    public function getChallenges( $id_label = "away_user_id" )
    {
        $matchupDbTable = new Application_Model_DbTable_UsersMatchups();
        $select = $matchupDbTable->select()
        			->where("status = ?", "waiting")
        			->where("$id_label = ?", $this->getId());
        
        $challenges = $matchupDbTable->fetchAll($select);
        
        $usersMatchupsMapper = new Application_Model_UsersMatchupsMapper();
        $matchups = array();
        
        foreach ($challenges as $challenge) {
        	$matchups[]  = $usersMatchupsMapper->find($challenge['id']);
		}

        return $matchups;
    }
    
    /**
     * Gets the challenges sent by this user
     * 
     * @return Ambigous <multitype:Application_Model_UsersMatchups, multitype:Application_Model_UsersMatchups >
     */
    public function getSentChallenges()
    {
        return $this->getChallenges("home_user_id");
    }
    
    
    /**
     * Gets this users picks for a particular matchup
     * 
     * @param array $data     This users matchup data that must include the users_matchups_id
     * @return array[Application_Model_UsersSmashups]
     */
    public function getPicks($data)
    {        
        $data["users_id"] = $this->id;
        return Application_Model_DbTable_UsersSmashups::getPicks($data);
    }
    
/*    
    public function getLeagues($data = null)
    {
        $leagueDb = new Application_Model_DbTable_UsersLeague();
        $select   = $leagueDb->select();
        $id       = $this->id;
        $select->where("(user_id_1 = " . $id . " or user_id_2 = " . $id . ")");
        if ($data) {
            foreach (array(
                "league_id",
                "bracket"
            ) as $field) {
                if (isset($data[$field]) && !empty($data[$field])) {
                    $select->where("{$field} = ?", $data[$field]);
                }
            }
        }
        $records = $leagueDb->fetchAll($select);
        if ($records) {
            $leagues = array();
            foreach ($records as $record) {
                $leagues[] = new Application_Model_UserLeague($record);
            }
            return $leagues;
        }
        return false;
    }
    public function getLeagueSeed($leagueID)
    {
        $seedDb = new Application_Model_DbTable_UsersLeagueSeed();
        $select = $seedDb->select();
        $select->where("league_id = ?", $leagueID);
        $select->where("user_id = " . $this->id);
        $record = $seedDb->fetchRow($select);
        if ($record) {
            return new Application_Model_UserLeagueSeed($record);
        }
        return false;
    }
*/

	/**
	 * Returns value from underlying user model
	 * 
	 * @param string $name
	 * @return Ambigous <multitype:, NULL, boolean, multitype:>
	 */
    public function __get($name)
    {
		return $this->_user->$name;
    }
    
    public function __set($name, $value)
    {        
    	$success = $this->_user->__set($name, $value);
		$userMapper = new Application_Model_UsersMapper();
        
		$userMapper->save($this->_user);
        return $success;
    }
    
    public function refresh()
    {
    	/*
        $table = new Application_Model_DbTable_Users();
        $this->_user->setTable($table);
        */
    }
    
    /**
     * Returns and account based on id
     * 
     * @param int $id
     * @return multitype:Application_Model_Account |Application_Model_Account|null
     */
    public static function getAccount($id)
    {
        if (array_key_exists($id, self::$_cache)) {
            return self::$_cache[$id];
        }
        
        $userDb = new Application_Model_DbTable_Users();
        $select = $userDb->select();
        $select->where('id = ?', $id);
        $select->where("status = ?", 'active');
        $user = $userDb->fetchRow($select);
        	
        if (isset($user)) {        	
            return new self($userDb->getUserById($id));
        } else {
        	return null;
        }
    }
    
	/**
	 * Returns an array of all active accounts
	 * 
	 * @return multitype:Application_Model_Users
	 */
    public static function getAccounts()
    {
        $usersDbTable = new Application_Model_DbTable_Users();
        return $usersDbTable->getAccounts();
    }
    
	/**
     * Returns an array of users associated with this user by status
     * 
     * @param array $statuses   	an array which can contain the values 'requested','accepted','declined','deleted','blocked'
     * 								if not specified, 'accepted' is used
     * 								if empty, all status values are used
     * 								the default is 'accepted' to return a list of friends
     * 
     * @param array $userIdTypes	Sets whether the $user_id is a 'sender_users_id', a 'receiver_users_id' or both.
     * 								By default, both types are listed in the array.  Likewise, if the array is 
     * 								empty, or no valid values are found in the array, then both types are assumed
     * 								(i.e. array('sender_users_id', 'receiver_users_id') ) is provided
     * 
	 * @return multitype:Application_Model_Users
	 */
    public static function getAssociatedUsers(array $statuses = array('accepted'), array $userIdTypes = array('sender_users_id', 'receiver_users_id') )
    {
        $auth = Zend_Auth::getInstance();

        $usersFriendsDbTable = new Application_Model_DbTable_UsersFriends();
		return $usersFriendsDbTable->getAssociatedUsers($auth->getIdentity()->id, $statuses, $userIdTypes);   
    }
    
    
    /**
     * Creates a new account.  Used for registration.
     * 
     * @param unknown_type $data
     * @throws Exception
     * @return Application_Model_Account
     */
    public static function createAccount($data)
    {
        $log = Zend_Registry::get('log');
        $userDb = new Application_Model_DbTable_Users();
        
        $requiredFields = array(
            'email',
            'login_pass',
            'first_name',
            'last_name',
        	'login_name'
        );
        
        foreach ($requiredFields as $field) {
            if (!isset($data[$field]) || empty($data[$field])) {
                throw new Exception("{$field} missing or empty", 1);
            }
        }
        
        $user = new Application_Model_Users();
        $columns = array_keys( $user->getData() );
        
        try {
        	
            foreach ($columns as $columnName) {
                if (array_key_exists($columnName, $data) && $columnName != "id" ) {
                    $user->$columnName = $data[$columnName];
                }
            }
            
            $user->status     = 'active';
            $user->user_type  = 'user';
            $user->login_pass = new Zend_Db_Expr("SHA1('{$user->login_pass}')");
            
            $user->date_added = date("Y-m-d H:i:s");

            $uid = $user->save();
         
            return new self($user);            
        } catch (Zend_Db_Exception $e) {

            $log->info($e->getMessage());
            $user->delete();
            if (stristr($e->getMessage(), 'Duplicate entry')) {
                throw new Exception("Duplicate account detected", 1);
            } else {
                throw new Exception("An error occured during account creation process", 1);
            }
            
        }
    }
    
    
	/**
	 * Gets the account of the logged in user
	 * 
	 * @return Application_Model_Account  the account of the logged in user
	 */
    public static function getAccountFromIdentity()
    {   
        return Zend_Auth::getInstance()->getIdentity();
    }
    
    public static function getNameFromId($id)
    {
        $userDb = new Application_Model_DbTable_Users();
        $select = $userDb->select();
        $select->from('user', array(
            'first_name',
            'last_name'
        ));
        $select->where('id = ?', $id);
        $select->where('status = ?', 'active');
        
        $name = $userDb->fetchRow($select)->toArray();
        
        if ($name) {
            return $name;
        }
        
        return false;
    }
    
    
    
}
