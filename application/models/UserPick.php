<?php

class Application_Model_UserPick
{
	protected $_userPick;

	public function __construct(Zend_Db_Table_Row $userPick)
	{
		$this->_userPick = $userPick;
	}

	public static function createUserPick($data)
	{
		$log = Zend_Registry::get("log");
		$pickDb = new Application_Model_DbTable_UserPick();
		foreach (array("game_id", "user_id", "sport_id", "season_id", "pick_id", "smash", "round") as $field) {
			if (!isset($data[$field])) {
				throw new Exception("{$field} missing", 1);
			}
		}
		foreach (array("game_id", "user_id", "sport_id", "season_id", "pick_id", "round") as $field) {
			if (empty($data[$field])) {
				throw new Exception("{$field} empty");
			}
		}
		$columns = $pickDb->info(Zend_Db_Table_Abstract::COLS);
		$pick = null;
		try {
			$pick = $pickDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$pick->$column = $data[$column];
				}
			}
			$pick->save();
			return new self($pick);
		} catch (Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			$pick->delete();
			if (strisstr($ex->getMessage(), "Duplicate entry")) {
				throw new Exception("Duplicate user pick found");
			} else {
				throw new Exception("An error occured during user pick creation", 1);
			}
		}
		return null;
	}
	public static function getPickById($pick) {
		if (!$pick) return false;

		$pickDb = new Application_Model_DbTable_UserPick();
		$select = $pickDb->select();
		$select->where("id = ?", $pick);
		$record = $pickDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}
	public static function getPick($data)
	{
		$pickDb = new Application_Model_DbTable_UserPick();
		$select = $pickDb->select();
		foreach (array("game_id", "user_id", "sport_id", "season_id", "smash", "round") as $field) {
			if (!isset($data[$field])) {
				throw new Exception("{$field} missing");
			}
			$select->where("{$field} = {$data[$field]}");
		}
		$record = $pickDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}
	public static function getPicks($data) {
		$pickDb = new Application_Model_DbTable_UserPick();
		$select = $pickDb->select();
		foreach (array("game_id", "user_id", "sport_id", "season_id") as $field) {
			if (!isset($data[$field])) {
				throw new Exception("{$field} missing");
			}
			$select->where("{$field} = {$data[$field]}");
		}
		$records = $pickDb->fetchAll($select);
		if ($records) {
			$picks = array();
			foreach ($records as $record) {
				$picks[] = new self($record);
			}
			return $picks;
		}
		return false;
	}

	public function getId() {
		return $this->_userPick->pick_id;
	}

	public function getPlayer() {
		$player = Application_Model_Player::getPlayer($this->pick_id);
		return $player;
	}
	public function getScore() {
		$player = Application_Model_Player::getPlayer($this->pick_id);
		$statData = array(
			"sport_id" => $this->sport_id,
			"season_id" => $this->season_id,
			"match_id" => $this->match_id,
			"team_id" => $player->team_id,
			"player_id" => $this->pick_id
		);
		$stats = Application_Model_MatchupStatsNBA::getPlayerStats($statData);
		$score = 0;
		$score += $stats->points;
		$score += $stats->off_rebounds;
		$score += $stats->def_rebounds;
		$score += $stats->assists;
		$score += $stats->steals;
		$score += $stats->blocked_shots;
		$score -= ($stats->turnovers + $stats->fouls);
		return $score;
	}

	public function getPlayerPosition() {
		$player = Application_Model_Player::getPlayer($this->pick_id);
		if ($player) {
			return $player->position;
		}
		return false;
	}
	public function getTeamID() {
		$player = Application_Model_Player::getPlayer($this->pick_id);
		if ($player) {
			return $player->team_id;
		}
		return false;
	}
	public function getTeam() {
		$player = Application_Model_Player::getPlayer($this->pick_id);
		if ($player) {
			$teamDb = new Application_Model_DbTable_Team();
			$select = $teamDb->select();
			$select->where("team_id = {$player->team_id}");
			$record = $select->fetchRow($select);
			if ($record) {
				return new Application_Model_Team($record);
			}
		}
		return false;
	}
	public function __get($name)
	{
		$teamsDb = new Application_Model_DbTable_UserPick();
		$cols = $teamsDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, $cols))
		{
			return $this->_userPick->$name;
		}

		return false;
	}

	public function __set($name, $value)
	{
		$teamsDb = new Application_Model_DbTable_Teams();
		$cols = $teamsDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, array('id')))
		{
			throw new Exception("Cannot modify an team {$name} value", 1);
		}

		if(in_array($name, $cols))
		{
			$this->_userPick->$name = $value;

			try {
				$this->_userPick->save();
			} catch(Zend_Db_Exception $e)
			{
				throw new Exception($e->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}

?>