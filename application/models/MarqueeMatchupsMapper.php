<?php
/**
 * Mapper for the MarqueeMatchups Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_MarqueeMatchupsMapper extends Application_Model_AbstractMapper
{
	/**
	 * Finds all marquee matchups for a setting
	 *
	 * @param int $marquee_matchup_settings_id  The marquee matchups settings id of the games
	 * 											to be delivered
	 *
	 * @return [Application_Model_MarqueeMatchups]   an array of marquee matchups
	 */
	public function fetchAllBySettingsId($marquee_matchup_settings_id)
	{
		if ( is_null($marquee_matchup_settings_id) ) return null;
	
		$select = $this->getDbTable()->select()
			->where("marquee_matchup_settings_id = ?", $marquee_matchup_settings_id);
	
		$rows = $this->getDbTable()->fetchAll($select);
	
		return !empty($rows) ? $this->getDbTable()->getModelsFromRows($rows) : array();
	}
	
	/**
	 * Finds Marquee Matchup by its settings and game id
	 *
	 * @param int $games_id   The id of the game in this marquee matchup
	 * @param int $marquee_matchup_settings_id  The marquee matchups settings id of the games
	 * 											to be delivered
	 *
	 * @return Application_Model_MarqueeMatchups|null   an marquee matchup object or null
	 */
	public function findByGameIdAndSettingsId($games_id, $marquee_matchup_settings_id)
	{
		if ( is_null($marquee_matchup_settings_id) || is_null($games_id) ) return null;
	
		$select = $this->getDbTable()->select()
			->where("marquee_matchup_settings_id = ?", $marquee_matchup_settings_id)
			->where("games_id = ?", $games_id);
	
		$row = $this->getDbTable()->fetchRow($select);
	
		return ($row) ? $this->getModel($row->toArray()) : null;
	}
}
