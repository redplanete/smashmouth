<?php

class Application_Model_Sport {
	protected $_sport;
	
	public function __construct(Zend_Db_Table_Row $sport) {
		$this->_sport = $sport;
	}
	
	public static function getSeasonID($sportID) {
		$sportDb = new Application_Model_DbTable_Sport();
		$select = $sportDb->select();
		$select->where("id = ?", $sportID);
		$record = $sportDb->fetchRow($select);
		if ($record) {
			return $record->season;
		}
		return 0;
	}
	
	public function __get($name) {
		$sportDb = new Application_Model_DbTable_Sport();
		$columns = $sportDb->info(Zend_Db_Table_Abstract::COLS);
		if (in_array($name, $columns)) {
			return $this->_sport->$name;
		}
		return false;
	}
	public function __set($name, $value) {
		$sportDb = new Application_Model_DbTable_Sport();
		$columns = $sportDb->info(Zend_Db_Table_Abstract::COLS);
		if (in_array($name, array("id"))) {
			throw new Exception("Field {$name} is readonly", 1);
		}
		if (in_array($name, $columns)) {
			$this->_sport->$name = $value;
			try {
				$this->_sport->save();
			} catch (Zend_Db_Exception $ex) {
				throw new Exception($ex->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}

?>