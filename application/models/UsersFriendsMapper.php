<?php
/**
 * Mapper for the UsersFriends Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_UsersFriendsMapper extends Application_Model_AbstractMapper
{
	
	/**
	 * Saves the content of a UsersFriends to the database
	 * Validates whether the request can be made to make a new friendship
	 * Generates appropriate events
	 *
	 * @param Application_Model_UsersFriends $usersFriends
	 *
	 * @return mixed  returns the primary key(s) of the saved object if it was and insert
	 *                for an update, the number of rows updated is returned
	 */
	public function save(Application_Model_AbstractModel $usersFriends)
	{
		$auth = Zend_Auth::getInstance();
		$currentId = $auth->getIdentity()->id;
		
		$dbTable = $this->getDbTable();
		/* @var $dbTable Application_Model_DbTable_UsersFriends */
			
		$usersMapper = Application_Model_DbTable_Users::getMapper();
		
		$sender_user = $usersMapper->find($usersFriends->sender_users_id);
		$receiver_user = $usersMapper->find($usersFriends->receiver_users_id);
		
		$isNewRequest = false;
		$isAcceptingRequest = false;
		
		if (!$sender_user || !$receiver_user) {
			// one of the users is not a member yet.  Technically, this should always be the $receiver_user
			$existingUsersFriends = null;
		} else {
			$existingUsersFriends = $dbTable->haveRelationship($sender_user, $receiver_user, array());
		}

		if ((!in_array($currentId, array($usersFriends->sender_users_id, $usersFriends->receiver_users_id))) 
		 || ($existingUsersFriends && !in_array($currentId, array($existingUsersFriends->sender_users_id, $existingUsersFriends->receiver_users_id))))
		{
			// the current user must be one of the user ids that have been set (i.e. existing) if the relation already exist,
			// and must be one of the user ids being set
			throw new Exception("Access Denied!");
		}
		
		if ($sender_user && $receiver_user && ($sender_user->id == $receiver_user->id ) ) {
			throw new Exception("Sorry. You cannot befriend or play yourself.");
		}
				
		if (null === ($usersFriends->getId())) {
			// we are about to create a new relationship
			
			if ($existingUsersFriends) {
				// but if there is already an established relationship, in most cases it is an error
				// but if the previous request was declined or deleted, we allow this.
				switch ($existingUsersFriends->status) {
					case 'requested':
						throw new Exception("You have already have a pending friend request.");
					case 'accepted':
						throw new Exception("You are already friends.");
					case 'blocked':						
						if (($currentId == $existingUsersFriends->sender_users_id && $existingUsersFriends->is_blocked_by_sender)
							      || ($currentId == $existingUsersFriends->receiver_users_id && $existingUsersFriends->is_blocked_by_receiver)) {
							throw new Exception("You must first unblock this user.");						
						} else {
							// this user has been blocked by the person he is trying to friend
							throw new Exception("This user currently can not be accessed.");
						}
					case 'declined':
					case 'deleted':
					default:
						// we are attempting to re-open a past friendship request
						$usersFriends->status = $usersFriends->status ? $usersFriends->status : "requested";
						break;		
				}
				
				// use the existing id to update via a recursive call
				$usersFriends->id = $existingUsersFriends->id;
				$this->save($usersFriends);
			} else {
				if (!in_array($usersFriends->status,array('requested','blocked'))) {
					throw new Exception("Invalid new relationship status. Only request and blocks can be initiated between users.");
				}
				
				$isNewRequest = $usersFriends->status == 'requested';
			}
		} else {
			if (!$existingUsersFriends || $existingUsersFriends->id != $usersFriends->id ) {
				// The wrong id is being given for the relationship
				// Either the attempt is not authorized or the database has
				// become corrupt with more than one pairing of these two ids
				throw new Exception("Access Denied. id mismatch for relationship.");
			} else {
				// Check if the status is being updated in proper sequence
				switch ($existingUsersFriends->status) {
					case 'requested':
						if (!in_array($usersFriends->status,array('requested','accepted','blocked','declined','deleted'))) {
							throw new Exception("You have already have a pending friend request.");
						}
						$isAcceptingRequest = $usersFriends->status == 'accepted';
						break;
					case 'accepted':
						if (!in_array($usersFriends->status,array('accepted','blocked','deleted'))) {
							throw new Exception("Invalid status change.");
						}
						break;
					case 'blocked':
						if (($currentId == $existingUsersFriends->sender_users_id && $existingUsersFriends->is_blocked_by_sender)
						   || ($currentId == $existingUsersFriends->receiver_users_id && $existingUsersFriends->is_blocked_by_receiver)) {
							if (!in_array($usersFriends->status,array('blocked','deleted'))) {
								throw new Exception("Invalid status change. You must first unblock this user.");
							}
						} else {
							// this user has been blocked by the person he is trying to friend
							throw new Exception("Access Denied.");
						}
						break;
					case 'declined':
						if (!in_array($usersFriends->status,array('requested','blocked','declined'))) {
							throw new Exception("Invalid status change.");
						}
						$isNewRequest = $usersFriends->status == 'requested';
						break;
					case 'deleted':
						if (!in_array($usersFriends->status,array('requested','blocked','deleted'))) {
							throw new Exception("You have already have a pending friend request.");
						}
						$isNewRequest = $usersFriends->status == 'requested';
						break;
					default:
						// this case should never be reached
						$usersFriends->status = $usersFriends->status ? $usersFriends->status : "requested";
						break;
				}				
			}
			
			// Make check to assure that sender and receiver are not being change except when the relationship
			// has already been declined or deleted
			if ( ((($currentId == $existingUsersFriends->sender_users_id) && ($currentId != $usersFriends->sender_users_id))
			|| (($currentId == $existingUsersFriends->receiver_users_id) && ($currentId != $usersFriends->receiver_users_id)))
			&& (!in_array($existingUsersFriends->status,array('declined','deleted'))) ){
				throw new Exception("The sender and receiver cannot switch from this status.");
			}
		}
		
		$new_id = parent::save($usersFriends);
		if ($isNewRequest && $usersFriends->receiver_users_id) {
			// the check for receiver_users_id is there for when the friend request is via e-mail to a non-member
			$eventsData = array("sender_users_id" => $sender_user->id, "sender_name" => $sender_user->getName(), "users_friends_id" => $new_id );
			$events_array = Application_Model_DbTable_Events::executeEvent($usersFriends->receiver_users_id, "friend_request", $eventsData);
		} else if ($isAcceptingRequest) {
			$eventsData = array("receiver_users_id" => $receiver_user->id, "receiver_name" => $receiver_user->getName(), "users_friends_id" => $new_id );
			$events_array = Application_Model_DbTable_Events::executeEvent($usersFriends->sender_users_id, "accepted_friend_request", $eventsData);			
		}
		
		return $new_id;
	}
}

