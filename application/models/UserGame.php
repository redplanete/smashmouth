<?php

class Application_Model_UserGame
{
	protected $_userGame;

	public function __construct(Zend_Db_Table_Row $userGame)
	{
		$this->_userGame = $userGame;
	}

	public static function createGame($data)
	{
		$log = Zend_Registry::get("log");
		$userDb = new Application_Model_DbTable_UserGame();
		foreach (array("user_id_1", "user_id_2", "sport_id", "season_id", "next_user", "next_round", "next_smash") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $userDb->info(Zend_Db_Table_Abstract::COLS);
		$userGame = null;
		try {
			$userGame = $userDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$userGame->$column = $data[$column];
				}
			}
			$userGame->save();
			return new self($userGame);
		} catch (Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			$userGame->delete();
			if (strisstr($ex->getMessage(), "Duplicate entry")) {
				throw new Exception("Duplicate user game found");
			} else {
				throw new Exception("An error occured during user game creation", 1);
			}
		}
	}
	public static function getGame($id)
	{
		$gameDb = new Application_Model_DbTable_UserGame();
		$select = $gameDb->select();
		$select->where("id = ?", $id);
		$record = $gameDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}

	public static function gameExists(Application_Model_Account $user1, Application_Model_Account $user2, $sportID, $anyStatus = false)
	{
		$gameDb = new Application_Model_DbTable_UserGame();
		$userID1 = $user1->getId();
		$userID2 = $user2->getId();
		$select = $gameDb->select();
		if (!$anyStatus) {
			$select->where("(status = 'active' or status = 'waiting')");
		}
		$select->where("sport_id = '{$sportID}' AND ((user_id_1 = {$userID1} AND user_id_2 = {$userID2}) OR (user_id_1 = {$userID2} AND user_id_2 = {$userID1}))");
		if ($gameDb->fetchRow($select)) {
			return true;
		}
		return false;
	}
	public static function sendChallenge(Application_Model_Account $user1, Application_Model_Account $user2, $sportID)
	{
		$log = Zend_Registry::get("log");
		$userID1 = $user1->getId();
		$userID2 = $user2->getId();
		$data = array();
		$data["user_id_1"] = $userID1;
		$data["user_id_2"] = $userID2;
		$data["sport_id"] = $sportID;
		$data["season_id"] = 3;
		$data["status"] = "waiting";
		$data["next_user"] = $userID1;
		$data["next_round"] = 1;
		$data["next_smash"] = true;
		$data["type"] = "individual";
		try {
			Application_Model_UserGame::createGame($data);
			$sender = Application_Model_Account::getAccount($userID1);
			$senderName = $sender->first_name . " " . $sender->last_name;
			$recipient = Application_Model_Account::getAccount($userID2);
			$mail = new Zend_Mail();
			$mail->setFrom("noreply@smashfantasysports.com");
			$mail->setBodyHtml("You've been invited to play NBA Playoff SmashMouth Basketball. Are you up for the challenge?<br><br>Let's get this SmashUp <a href='www.smashfantasysports.com/account/index'>started</a>!");
			$mail->addTo($recipient->email, $senderName);
			$mail->setSubject("SmashUp Challenge from " . $senderName . "!");
			$mail->send();
		} catch (Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			throw new Exception($ex->getMessage());
		}
	}
	public function getUserScore($data) {
		if (!isset($data["user_id"])) {
			throw new Exception("user_id missing");
		}
		$pickData = array(
			"game_id" => (int)$this->id,
			"user_id" => (int)$data["user_id"],
			"sport_id" => (int)$this->sport_id,
			"season_id" => (int)$this->season_id
		);
		$picks = Application_Model_DbTable_UsersSmashups::getPicks($pickData);
		$players = array();
		$matchIDs = array();
		foreach ($picks as $pick) {
			$player = Application_Model_Player::getPlayer($pick->pick_id);
			$players[] = $player;
			$matchIDs[$player->player_id] = $pick->match_id;
		}
		$score = 0;
		foreach ($players as $player) {
			$statData = array(
				"sport_id" => $this->sport_id,
				"season_id" => $this->season_id,
				"match_id" => $matchIDs[$player->player_id],
				"team_id" => $player->team_id,
				"player_id" => $player->player_id
			);
			$stats = Application_Model_PlayerStatsNBA::getPlayerStats($statData);
			$score += $stats->points;
			$score += $stats->off_rebounds;
			$score += $stats->def_rebounds;
			$score += $stats->assists;
			$score += $stats->steals;
			$score += $stats->blocked_shots;
			$score -= ($stats->turnovers + $stats->fouls);
		}
		return $score;
	}

	public function getFinalScores()
	{
		$scores = array();
		$accountA = Application_Model_Account::getAccount($this->user_id_1);

		$team = $this->getPickedTeam($accountA);
		$teamCounter = 1;
		$score = 0;
		foreach ($team as $pos => $players) {
			foreach ($players as $player) {
				$playerPick = null;
				if ($player) {
					$playerPick = $this->getPickFromPlayer($player);
				}
				if ($playerPick) {
					$score += $playerPick->getScore();
				}
				$teamCounter++;
			}
		}

		$scores[$this->user_id_1] = $score;


		$accountB = Application_Model_Account::getAccount($this->user_id_2);
		
		$team = $this->getPickedTeam($accountB);
		$teamCounter = 1;
		$score = 0;
		foreach ($team as $pos => $players) {
			foreach ($players as $player) {
				$playerPick = null;
				if ($player) {
					$playerPick = $this->getPickFromPlayer($player);
				}
				if ($playerPick) {
					$score += $playerPick->getScore();
				}
				$teamCounter++;
			}
		}

		$scores[$this->user_id_2] = $score;
		
		return $scores;
	}

	public function getPick($data) {
		$pickDb = new Application_Model_DbTable_UserPick();
		$select = $pickDb->select();
		foreach (array("user_id", "round", "smash") as $field) {
			if (!isset($data[$field])) {
				throw new Exception("{$field} missing");
			}
			$select->where("{$field} = {$data[$field]}");
		}
		$select->where("game_id = {$this->id}");
		$record = $pickDb->fetchRow($select);
		if ($record) {
			return new Application_Model_UserPick($record);
		}
		return false;
	}
	public function getLastPick($userID) {
		$nextRound = (int)$this->next_round;
		$isSmash = (int)$this->next_smash;
		$wasSmash = !$isSmash;
		$lastRound = max(($isSmash ? $nextRound - 1 : $nextRound), 1);
		$pickData = array();
		$pickData["user_id"] = $userID;
		$pickData["round"] = $lastRound;
		$pickData["smash"] = $wasSmash;
		return $this->getPick($pickData);
	}
	public function getLastGamePick() {
		return Application_Model_UserPick::getPickById($this->last_pick_id);
	}
	public function getNextUser() {
		if ($this->isGameOver()) {
			return false;
		}
		$record = Application_Model_Account::getAccount($this->_userGame->next_user);
		if ($record) return $record;
		return false;
	}
	public function isGameOver() {
		return ((string)$this->status == "finished" || (int)$this->round == 9);
	}

	public function getMarquisMatchupTeams() {
		return Application_Model_MarquisMatchup::getMarquisMatchupTeams($this->sport_id, $this->season_id);
	}

	public function getAvailableTeams() {
		$account = Application_Model_Account::getAccountFromIdentity();
		if ($account) {
			$opponent = $this->getOpponent();
			if ($opponent) {
				$userID = (int)$account->id;
				$opponentID = (int)$opponent->id;
				$marquisData = array();
				$marquisData["sport_id"] = $this->sport_id;
				$marquisData["season_id"] = $this->season_id;
				$marquisMatchups = Application_Model_MarquisMatchup::getAllMarquisMatchups($marquisData);
				$matchups = array();
				foreach ($marquisMatchups as $marquisMatchup) {
					$matchupData = array();
					$matchupData["sport_id"] = $marquisMatchup->sport_id;
					$matchupData["match_id"] = $marquisMatchup->match_id;
					$matchupData["season_id"] = $marquisMatchup->season_id;
					$matchups[] = Application_Model_Matchup::getMatchup($matchupData);
				}
				$pickData = array();
				$pickData["game_id"] = $this->id;
				$pickData["sport_id"] = $this->sport_id;
				$pickData["season_id"] = $this->season_id;
				$myPicks = $account->getPicks($pickData);
				$opponentPicks = $opponent->getPicks($pickData);
				$availableTeams = array();
				$lastPick = null;
				if (!$this->next_smash) {
					$lastPick = $this->getLastPick($opponentID);
					if ($lastPick) {
						$lastPickTeamID = $lastPick->getTeamID();
						$matchupData = array();
						$matchupData["sport_id"] = $this->sport_id;
						$matchupData["match_id"] = $lastPick->match_id;
						$matchupData["season_id"] = $this->season_id;
						$matchup = Application_Model_Matchup::getMatchup($matchupData);
						if ($matchup) {
							if ($lastPickTeamID == $matchup->home_team_id) {
								$awayTeamID = $matchup->away_team_id;
								$availableTeams[$awayTeamID] = Application_Model_Team::getTeam($awayTeamID);
							} else {
								$homeTeamID = $matchup->home_team_id;
								$availableTeams[$homeTeamID] = Application_Model_Team::getTeam($homeTeamID);
							}
						}
					}
				} else {
					foreach ($matchups as $matchup) {
						$homeTeamID = $matchup->home_team_id;
						$availableTeams[$homeTeamID] = Application_Model_Team::getTeam($homeTeamID);
						$awayTeamID = $matchup->away_team_id;
						$availableTeams[$awayTeamID] = Application_Model_Team::getTeam($awayTeamID);
					}
				}
				// Determine which teams have already been picked.
				$pickedTeams = array();
				if ($myPicks) {
					foreach ($myPicks as $myPick) {
						$pickedTeams[] = $myPick->getTeamID();
					}
				}
				if ($opponentPicks) {
					foreach ($opponentPicks as $opponentPick) {
						$pickedTeams[] = $opponentPick->getTeamID();
					}
				}
				// Remove already picked teams from the list of available teams.
				foreach ($pickedTeams as $pickedTeam) {
					unset($availableTeams[$pickedTeam]);
				}
				usort($availableTeams, function($a, $b) {
					return strcmp((string)$a->name, (string)$b->name);
				});
                                
				return $availableTeams;
			}
		}
		return null;
	}
	public function getAvailablePlayers(Application_Model_Team $team, $position = false)
	{
		$account = Application_Model_Account::getAccountFromIdentity();
		if ($account) {
			$opponent = $this->getOpponent($account);
			if ($opponent) {
				$userID = (int)$account->id;
				$opponentID = (int)$opponent->id;
				$playerData = array();
				$playerData["sport_id"] = $this->sport_id;
				$availablePlayers = $team->getPlayers($playerData);
				if ($availablePlayers) {
					$filteredPlayers = array();
					$pickData = array();
					$pickData["game_id"] = $this->id;
					$pickData["sport_id"] = $this->sport_id;
					$pickData["season_id"] = $this->season_id;
					$myPicks = $account->getPicks($pickData);
					$opponentPicks = $opponent->getPicks($pickData);
					$lastPick = null;
					if (!$this->next_smash) {
						// Filter the players based on the previously picked positions.
						$lastPick = $this->getLastPick($opponentID);
					}
					if ($lastPick) {
						// The pick is a force pick.
						// Filter out all players that are in a different position from the last pick.
						$filter = $lastPick->getPlayerPosition();
						foreach ($availablePlayers as $player) {
							if ($player->position == $filter) {
								$filteredPlayers[] = $player;
							}
						}
					} else {
						// The pick is a smash pick.
						if ($this->round == 1) {
							// The first pick is being picked.
							// Add all f/c's, pg's, and sg's to the list of available players.
							foreach ($availablePlayers as $player) {
								if ($player->position == "F/C" ||
									$player->position == "PG" ||
									$player->position == "SG") {
									if (!$position || $position == $player->position) {
										$filteredPlayers[] = $player;
									}
								}
							}
						} else {
							// The third pick or later is being picked.
							// Determine how many of each position have already been picked.
							$fcs = 0;
							$pgs = 0;
							$sgs = 0;
							if ($myPicks) {
								foreach ($myPicks as $myPick) {
									switch ($myPick->getPlayerPosition()) {
										case "F/C": $fcs++; break;
										case "PG": $pgs++; break;
										case "SG": $sgs++; break;
									}
								}
							}
							foreach ($availablePlayers as $player) {
								if (($player->position == "F/C" && $fcs < 4) ||
									($player->position == "PG" && $pgs < 2) ||
									($player->position == "SG" && $sgs < 2)) {
									if (!$position || $position == $player->position) {
										$filteredPlayers[] = $player;
									}
								}
							}
						}
					}
					usort($filteredPlayers, function($a, $b) {
						$posComp = strcmp((string)$a->position, (string)$b->position);
						if ($posComp) {
							return $posComp;
						}
						return strcmp((string)$a->name, (string)$b->name);
					});
					//usort($filteredPlayers, function
					return $filteredPlayers;
				}
			}
		}
		return null;
	}
	public function pickPlayer(Application_Model_Player $player, $matchID) {
		$account = Application_Model_Account::getAccountFromIdentity();
		if ($account) {
			$opponent = $this->getOpponent($account);
			if ($opponent && $player) {
				$userID = (int)$account->id;
				$opponentID = (int)$opponent->id;
				$nextSmash = (int)$this->next_smash;
				$nextRound = (int)$this->next_round;
				$lastRound = $nextRound;
				$pickData = array();
				$pickData["game_id"] = $this->id;
				$pickData["user_id"] = $account->id;
				$pickData["sport_id"] = $this->sport_id;
				$pickData["season_id"] = $this->season_id;
				$pickData["match_id"] = $matchID;
				$pickData["pick_id"] = $player->player_id;
				$pickData["smash"] = $nextSmash;
				$pickData["round"] = $nextRound;
				$pickData["score"] = 0;
				$pick = Application_Model_UserPick::createUserPick($pickData);
				if (!$pick) {
					return false;
				}
				$nextUserID = (!$nextSmash ? $userID : $opponentID);
				$nextSmash = !$nextSmash;
				$nextRound = ($nextSmash ? $nextRound + 1 : $nextRound);
				if ($nextRound == 9) {
					$this->status = "finished";
				}
				$this->next_user = $nextUserID;
				$this->next_smash = $nextSmash;
				$this->next_round = $nextRound;
				$this->last_pick_id = $pick->id;
				if ($this->status != "finished") {
					if (!$nextSmash) {
						$nextUser = Application_Model_Account::getAccount($nextUserID);
						$nextUserName = $nextUser->first_name . " " . $nextUser->last_name;
						$nextOpponent = $this->getOpponent($nextUser);
						$nextOpponentName = $nextOpponent->first_name . " " . $nextOpponent->last_name;
						$nextUserMail = new Zend_Mail();
						$nextUserMail->setFrom("noreply@smashfantasysports.com");
						$nextUserMail->setBodyHtml("It is now your <strong>FORCE PICK</strong>!");
						$nextUserMail->addTo($nextUser->email, $nextUserName);
						$nextUserMail->setSubject("SmashUp with " . $nextOpponentName);
						$nextUserMail->send();
					}
				} else {
					$account = Application_Model_Account::getAccountFromIdentity();
					$opponent = $this->getOpponent($account);
					$opponentName = $opponent->first_name . " " . $opponent->last_name;					
					$accountName = $account->first_name . " " . $account->last_name;
					$accountMail = new Zend_Mail();
					$accountMail->setFrom("noreply@smashfantasysports.com");
					$accountMail->setBodyHtml("Your SmashUp is complete. Check back on 4/22 to track your scores.");
					$accountMail->addTo($account->email, $accountName);
					$accountMail->setSubject("SmashUp Complete with " . $opponentName);
					$accountMail->send();
					$opponentMail = new Zend_Mail();
					$opponentMail->setFrom("noreply@smashfantasysports.com");
					$opponentMail->setBodyHtml("Your SmashUp is complete. Check back on 4/22 to track your scores.");
					$opponentMail->addTo($opponent->email, $opponentName);
					$opponentMail->setSubject("SmashUp Complete with " . $accountName);
					$opponentMail->send();
				}
				return true;
			}
		}
		return false;
	}
	public function pickExists(Application_Model_Player $player, $matchID) {
		$account = Application_Model_Account::getAccountFromIdentity();
		if ($account && $player) {
			return Application_Model_UserPick::getPick(array(
				"game_id" => $this->id,
				"user_id" => $account-id,
				"sport_id" => $this->sport_id,
				"season_id" => $this->season_id,
				"smash" => $this->next_smash,
				"round" => $this->next_round
			));
		}
		return false;
	}
	public function getId() {
		return $this->_userGame->id;
	}
	public function getOpponent($account = null) {
		if (!$account) {
			$account = Application_Model_Account::getAccountFromIdentity();			
		}
		if ($account) {
			if ($account->getId() == $this->user_id_2) {
				return Application_Model_Account::getAccount($this->user_id_1);
			} else {
				return Application_Model_Account::getAccount($this->user_id_2);
			}
		}
		return false;
	}
	public function getOpponentID($account = null) {
		if (!$account) {
			$account = Application_Model_Account::getAccountFromIdentity();
		}
		if ($account) {
			if ($account->id == $this->user_id_2) {
				return $this->user_id_1;
			} else {
				return $this->user_id_2;
			}
		}
		return 0;
	}
	public function getPickedTeam($account) {
		$positions = array("PG" => 2,"SG" => 2,"F/C" => 4);
		$pickedTeam = array();
		$picks = Application_Model_DbTable_UsersSmashups::getPicks(array(
			'user_id' => (int)(is_numeric($account) ? $account : $account->id),
			'sport_id' => (int)$this->_userGame->sport_id,
			'game_id' => (int)$this->_userGame->id,
			'season_id' => (int)$this->_userGame->season_id
		));
		$players = Application_Model_Player::getPlayersFromPicks($picks);
		foreach ($positions as $pos => $count) {
			$filtered = Application_Model_Player::filterPlayersByPosition($players, $pos);
			if ($filtered) foreach ($filtered as $f) {
				$pickedTeam[$pos][] = $f;
			}
			$remainder = count($filtered) - $count;
			while ($remainder < 0) {
				$pickedTeam[$pos][] = false;
				$remainder++;
			}
		}
		return $pickedTeam;
	}
	public function getPickFromPlayer(Application_Model_Player $player) {
		$pickDb = new Application_Model_DbTable_UserPick();
		$select = $pickDb->select();
		$select->where('pick_id = ?', $player->getId());
		$select->where('season_id = ?', $this->season_id);
		$select->where('sport_id = ?', $this->sport_id);
		$select->where('game_id = ?', $this->getId());
		$record = $pickDb->fetchRow($select);
		if ($record) {
			return new Application_Model_UserPick($record);
		}
		return false;
	}
	public function getSport() {
		$sportsDb = new Application_Model_DbTable_Sport();
		$select = $sportsDb->select();
		$select->where("id = ?", $this->_userGame->sport_id);
		$record = $sportsDb->fetchRow($select);

		if ($record) {
			return $record->sport;
		}
		return false;
	}
	public function __get($name)
	{
		$gameDb = new Application_Model_DbTable_UserGame();
		$cols = $gameDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, $cols))
		{
			return $this->_userGame->$name;
		}

		return false;
	}

	public function __set($name, $value)
	{
		$gameDb = new Application_Model_DbTable_UserGame();
		$cols = $gameDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, array('id')))
		{
			throw new Exception("Cannot modify an user game {$name} value", 1);
		}

		if(in_array($name, $cols))
		{
			$this->_userGame->$name = $value;

			try {
				$this->_userGame->save();
			} catch(Zend_Db_Exception $e)
			{
				throw new Exception($e->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}

?>