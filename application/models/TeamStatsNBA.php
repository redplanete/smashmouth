<?php

class Application_Model_TeamStatsNBA
{
	protected $_stats;

	public function __construct(Zend_Db_Table_Row $stats)
	{
		$this->_stats = $stats;
	}

	public static function createStat($data)
	{
		$log = Zend_Registry::get('log');
		$statDb = new Application_Model_DbTable_TeamStatsNBA();
		foreach (array("season_id", "team_id", "team_name") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $statDb->info(Zend_Db_Table_Abstract::COLS);
		try {
			$stats = $statDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$stats->$column = $data[$column];
				}
			}
			$stats->save();
			return new self($stats);
		} catch(Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			$stats->delete();
			if (stristr($ex->getMessage(), 'Duplicate entry')) {
				throw new Exception("Duplicate nba team stats founds");
			} else {
				throw new Exception("An error occured during player creation", 1);
			}
		}
		return null;
	}
	public static function getTeamStats($data) {
		$statDb = new Application_Model_DbTable_TeamStatsNBA();
		$select = $statDb->select();
		foreach (array("season_id", "team_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty");
			}
			$select->where("{$field} = ?", $data[$field]);
		}
		$record = $statDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}

	public function __get($name)
	{
		$playerDb = new Application_Model_DbTable_TeamStatsNBA();
		$cols = $playerDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, $cols))
		{
			return $this->_stats->$name;
		}

		return false;
	}

	public function __set($name, $value)
	{
		$playerDb = new Application_Model_DbTable_TeamStatsNBA();
		$cols = $playerDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, array('id')))
		{
			throw new Exception("Cannot modify a player {$name} value", 1);
		}

		if(in_array($name, $cols))
		{
			$this->_stats->$name = $value;

			try {
				$this->_stats->save();
			} catch(Zend_Db_Exception $e)
			{
				throw new Exception($e->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}

