<?php
/**
 * The UsersMatchups, together with the UsersSmashups model are the main driving models
 * behind the SmashMouthFantasy game.  This data keeps track of which players have been
 * selected by which user.  In a nutshell, it represents a user's team of players for that
 * week or matchup.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_UsersSmashups extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"users_matchups_id" => null,
			"users_id" => null,
			"games_id" => null,
			"players_id" => null
		);
	}
}

