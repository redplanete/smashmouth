<?php
/**
 * Proxy to interacting with the events_types database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_EventsTypes extends Application_Model_DbTable_AbstractTable {
	protected $_name = 'events_types';
	
	/**
	 * A reusable mapper object for this dbTable
	 *
	 * @see Application_Model_DbTable_AbstractTable::getMapper()
	 * @var Application_Model_AbstractMapper
	 */
	protected static $mapper;
}
