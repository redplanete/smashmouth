<?php
/**
 * Proxy to interacting with the users database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_Users extends Application_Model_DbTable_AbstractTable {

    protected $_name = 'users';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;

    /**
     * Returns all accounts with a similar name or email address
     * 
     * @param string $searchTerm a space delimited list of search terms
     * 
     * @return array[Application_Model_Users]  an array of accounts with similar emails or search terms
     */
    public function searchAccountsByNameOrEmail($searchTerm)
    {        
    	$db = $this->getAdapter();
    	
    	$emailSearch = $db->quoteInto('email LIKE ?', "%$searchTerm%");
    	$nameSearch = $db->quoteInto("CONCAT(COALESCE(first_name, ''), ' ', COALESCE(last_name, '')) LIKE ?", "%$searchTerm%");
    	
        $select = $this->select()
        			->where($emailSearch." OR ".$nameSearch)
        			->where("status = ?",'active');
        
		return self::getModelsFromRows($this->fetchAll($select));
    }
        

    /**
     * Returns an array of all active users
     * 
     * @return array[Application_Model_Users]
     */
    public function getAccounts() {
        $select = $this->select()
        			->where("status = ?", 'active');
        
        return self::getModelsFromRows($this->fetchAll($select));
    }

    /**
     * Convenience method for getting user model object from the database
     * 
     * @param int|string $userId
     * @throws Exception  throws and exception if a user cannot be found with the given user id
     * @return Application_Model_Users
     */
    public function getUserById($userId) {
    	$userMapper = new Application_Model_UsersMapper();
    	$userWasFound = $userMapper->find($userId);
    	
    	if ($userWasFound) return $userWasFound;
    	
    	throw new Exception("The user was not found in the system");
    }
}
