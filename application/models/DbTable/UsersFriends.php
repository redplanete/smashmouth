<?php
/**
 * Proxy to interacting with the users_friends database table
 * 
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_UsersFriends extends Application_Model_DbTable_AbstractTable
{
    
    protected $_name = 'users_friends';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_UsersFriendsMapper
     */
    protected static $mapper;
    
    /**
     * Returns an array of users who have a relationship with a particular user
     * 
     * @param int   $user_id		the subject user
     * 
     * @param array $statuses   	an array which can contain the values 'requested','accepted','declined','deleted','blocked'
     * 								if not specified, 'accepted' is used to return a list of friends
     * 								if set to an empty array or null, all status values are used
     * 
     * @param array $userIdTypes	Sets whether the $user_id is a 'sender_users_id', a 'receiver_users_id' or both.
     * 								By default, both types are listed in the array.  Likewise, if the array is 
     * 								empty, or no valid values are found in the array, then both types are assumed
     * 								(i.e. array('sender_users_id', 'receiver_users_id') ) is provided
     * 
     * 
     * @return array[Application_Model_Users]
     */
    public function getAssociatedUsers($user_id, array $statuses = array("accepted"), array $userIdTypes = array('sender_users_id', 'receiver_users_id'))
    {
    	$db = $this->getAdapter();
    	
    	if (empty($statuses)) $statuses = array('requested','accepted','declined','deleted','blocked');
    	if (empty($userIdTypes)) $userIdTypes = array('sender_users_id', 'receiver_users_id');
    	
    	$diff = array_diff(array('sender_users_id', 'receiver_users_id'), $userIdTypes);
    	
    	if (count($diff) == 2) $userIdTypes = $diff;
    	
    	$userTypeWhere = "";
    	if (in_array('sender_users_id', $userIdTypes)) {
    		$userTypeWhere = $db->quoteInto('sender_users_id = ?', $user_id);
    	}
    	if (in_array('receiver_users_id', $userIdTypes)) {
    		if (!empty($userTypeWhere)) $userTypeWhere .= ' OR ';
    		$userTypeWhere .= $db->quoteInto('receiver_users_id = ?', $user_id);
    	}    	
    	
        $select = $this->select()
        				->from($this->_name, array('sender_users_id', 'receiver_users_id'))
        				->where( $userTypeWhere )
        				->where('status IN (?)', $statuses);
     
        $list = $this->fetchAll($select);
        $relations = array();           
        if ($list->count()) {
            foreach ( $list->toArray() as $connection ) {
            	$userMapper = new Application_Model_UsersMapper();
            	
            	$relation = ($connection['sender_users_id'] == $user_id) 
            		? $userMapper->find($connection['receiver_users_id']) 
            		: $userMapper->find($connection['sender_users_id']);           	
            	if (isset($relation)) $relations[] = $relation;
            }
            
        }
        return $relations;
    }
    
    /**
     * Return whether a relationship like friendship, friend request, block, etc. exist between two users
     * 
     * @param Application_Model_Users $user1
     * @param Application_Model_Users $user2
     * @param array $statuses   an array which can contain the values 'requested','accepted','declined','deleted','blocked'
     * 							if not specified, 'accepted' is used.  If an empty array is given, or if null, all status values are used,
     * 							
     * 
     * @return Application_Model_UsersFriends|null  if the specified relationship types exist between the users, a  is returned, otherwise false
     */
    public function haveRelationship(Application_Model_Users $user1 = null, Application_Model_Users $user2 = null, array $statuses = array("accepted")) {
    	$db = $this->getAdapter();
  	
    	if ((!$user1 || !$user1->id) && (!$user2 || !$user2->id)) return null;

    	if ((!$user1 || !$user1->id)) {
    		if (!$user1) $user1 = new Application_Model_Users();
    		$friendship1 = $db->quoteInto('invite_link = ?', $user1->invite_link).' AND '.$db->quoteInto('receiver_users_id = ?', $user2->id).' AND '.$db->quoteInto('sender_users_id IS NULL');
    		$friendship2 = $db->quoteInto('sender_users_id = ?', $user2->id).' AND '.$db->quoteInto('invite_link = ?', $user1->invite_link).' AND '.$db->quoteInto('receiver_users_id IS NULL');
    	} else if (!$user2 || !$user2->id) {
    		if (!$user2) $user2 = new Application_Model_Users();
    		$friendship1 = $db->quoteInto('sender_users_id = ?', $user1->id).' AND '.$db->quoteInto('invite_link = ?', $user2->invite_link).' AND '.$db->quoteInto('receiver_users_id IS NULL');
    		$friendship2 = $db->quoteInto('invite_link = ?', $user2->invite_link).' AND '.$db->quoteInto('receiver_users_id = ?', $user1->id).' AND '.$db->quoteInto('sender_users_id IS NULL');
    	} else {
    		$friendship1 = $db->quoteInto('sender_users_id = ?', $user1->id).' AND '.$db->quoteInto('receiver_users_id = ?', $user2->id);
    		$friendship2 = $db->quoteInto('sender_users_id = ?', $user2->id).' AND '.$db->quoteInto('receiver_users_id = ?', $user1->id);
    	}
    	
    	if (empty($statuses)) $statuses = array('requested','accepted','declined','deleted','blocked');
    	
    	$select = $this->select()
	    	->where("($friendship1) OR ($friendship2)")
	    	->where('status IN (?)', $statuses); 

    	$relationship = $this->fetchRow($select);
    	return ($relationship) ? static::getMapper()->getModel($relationship->toArray()) : null;
    }
    

    /**
     * Creates a url safe invite link to be used to associate a user coming to 
     * the site by an e-mail link with the user who sent the request
     * 
     * @return string  The invite link
     */
    public static function createInviteLink() {
    	$invite_link = "";
    	$ascii_ranges = array();
    	
    	$ascii_ranges[] = range(48,57);
    	$ascii_ranges[] = range(65,90);
    	$ascii_ranges[] = range(97,122);
    	
    	$urlSafeChars = "-_.!*'()";
    	
    	foreach ($ascii_ranges as $range) {
    		foreach ($range as $char ) {
    			$urlSafeChars .= chr($char);
    		}
    	}

    	for ( $i = 0; $i < 20; $i++) {
    		$invite_link .= $urlSafeChars[rand(0, strlen($urlSafeChars) - 1)];
    	}
    	
    	return $invite_link;
    }
}
