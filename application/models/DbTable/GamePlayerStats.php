<?php
/**
 * Proxy to interacting with the game_player_stats database table
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_GamePlayerStats extends Application_Model_DbTable_AbstractTable {
    protected $_name = 'game_player_stats';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;    
}
