<?php
/**
 * This class controls real-time communication between the end user and the server via 
 * a Listener Event, broadcast model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_NotificationManager extends Zend_Controller_Action
{

	/**
	 * Stub method that is ear-marked for handling notifications of events
	 * 
	 * @param array $events_array  information about a newly created event
	 */
	public static function notifyOfEvent( array $events_array ) {
		// This will do nothing for now, but eventually will communicate via long polling,
		// websockets, or server sent.
	}

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

