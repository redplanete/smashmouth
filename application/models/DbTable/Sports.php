<?php
/**
 * Proxy to interacting with the sports database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_Sports extends Application_Model_DbTable_AbstractTable
{
    protected $_name = 'sports';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;
}

