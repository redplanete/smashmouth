<?php

/**
 * Proxy to interacting with the marquee_matchups database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_MarqueeMatchups extends Application_Model_DbTable_AbstractTable
{
    protected $_name = 'marquee_matchups';

    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;
    
    /**
     * Clears marquee matchups for all games within the given settings
     *
     * @param int $settings_id
     *
     * @return int number of rows deleted
     */
    public function clearMarqueeMatchups($settings_id) {
    	return $this->delete(array("marquee_matchup_settings_id = ?" => $settings_id));
    }
    
    /**
     * Clears marquee matchups for all games exclusively outside the dates given within the given settings
     *
     * @param int $settings_id
     * @param date $smashup_open_date
     * @param date $smashup_close_date
     *
     * @return array[Application_Model_Games]
     */
    public function narrowMarqueeMatchups($settings_id, $smashup_open_date, $smashup_close_date) {   
    	$this->getAdapter()
    		->query("DELETE mm FROM marquee_matchups mm INNER JOIN games g ON mm.games_id = g.id WHERE mm.marquee_matchup_settings_id = ? AND ( (DATE(g.scheduled_date) < ?) OR (DATE(g.scheduled_date) > ?) )", array($settings_id, $smashup_open_date, $smashup_close_date))->execute();    	 
    }
    
    public function getMarqueeMatchUpPendingGameCount() {
        $adapter = $this->getAdapter();
        $statement = $adapter->query("SELECT count(*) as count from marquee_matchups mm INNER JOIN games g ON mm.games_id = g.id and g.status = 'pending'");
        $row = $statement->fetch();
        $count = $row["count"];
        return $count;

    }
    
    public function getMarqueeMatchUpInProgressGameCount() {
        $adapter = $this->getAdapter();
        $statement = $adapter->query("SELECT count(*) as count from marquee_matchups mm INNER JOIN games g ON mm.games_id = g.id and g.status = 'in_progress'");
        $row = $statement->fetch();
        $count = $row["count"];
        return $count;

    }
}


 		