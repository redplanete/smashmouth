<?php
/**
 * Proxy to interacting with the players database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_Players extends Application_Model_DbTable_AbstractTable
{

    protected $_name = 'players';

    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;
    
    /**
     * Gets a list of players not updated at the last update date.  Those not update
     * implies that they are no longer in the league.
     * 
     * @param string $update_date  the date of the targeted update
     */
	public function getPlayersNotUpdated($update_date) {
		$select = $this->select();
		$select->where("date_updated != ?", $update_date)
			   ->where("teams_id IS NOT NULL");
		
		$rows = $this->fetchAll($select);
		
		return $this->getModelsFromRows($rows);
	}

}

