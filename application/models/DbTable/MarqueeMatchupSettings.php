<?php
/**
 * Proxy to interacting with the marquee_matchups database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_MarqueeMatchupSettings extends Application_Model_DbTable_AbstractTable
{
    protected $_name = 'marquee_matchup_settings';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;
    
    /**
     * Gets the current marquee matchup settings
     * 
     * @param int $season_id
     * @return Application_Model_MarqueeMatchupSettings|null
     */
    public function getCurrentMarqueeMatchupSettings($season_id, $rollback = false) {
    	$select = $this->select()
    		->where("seasons_id = ?", $season_id)
			->where("'".date("Y-m-d")."' BETWEEN smashup_open_date AND smashup_close_date");

    	$row = $this->fetchRow($select);
    	
    	if ($rollback && !($row)) {
    		$select = $this->select()
	    		->where("seasons_id = ?", $season_id)
	    		->where("'".date("Y-m-d", time()-60*60*24)."' BETWEEN smashup_open_date AND smashup_close_date");
    		
    		$row = $this->fetchRow($select);
    	}
    	
    	return ($row) ? $this->getMapper()->getModel($row->toArray()) : null;
    }

    /**
     * Gets all marquee matchups settings for the specified season
     * These will be referred to in the admin as Games
     *
     * @param int $season_id
     * @return [Application_Model_MarqueeMatchupSettings]
     */
    public function getAllSettingsBySeason($season_id) {
    	$select = $this->select()
	    	->where("seasons_id = ?", $season_id)
	    	->order("id ASC");
    
    	$rowset = $this->fetchAll($select);
    	 
    	return $rowset->count() ? $this->getModelsFromRows($rowset) : array();
    }
    
    /**
     *  Gets the positional limitations on smashup picks
     *
     * @var int $settings_id the MarqueeMatchupSettings id from which to retreive the array.
     *
     * @return array|null  an array of positions for this SmashUp
     */
    public static function getSmashupArray($settings_id) {
    	$settings = static::getMapper()->find($settings_id);

    	if (!empty($settings)) {
	    	$positions = json_decode($settings->positions, true );
	    	$positions["team_ids"] = array();
    	} else {
    		$positions = null;
    	}
    	
    	return $positions;
    }
}