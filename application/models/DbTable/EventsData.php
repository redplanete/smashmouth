<?php
/**
 * Proxy to interacting with the events_data database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_EventsData extends Application_Model_DbTable_AbstractTable {
	protected $_name = 'events_data';
	
	/**
	 * A reusable mapper object for this dbTable
	 *
	 * @see Application_Model_DbTable_AbstractTable::getMapper()
	 * @var Application_Model_AbstractMapper
	 */
	protected static $mapper;
	
	/**
	 * Returns an associative array of name and value for each row in rowset
	 *
	 * @param Zend_Db_Table_Rowset_Abstract $dataRows
	 * @return array of name/value pair for each EventsData row
	 */
	public static function getDataFromRows($rowset) {
		$data = array();
	
		foreach($rowset as $row) {
			/* @var $row Zend_Db_Table_Row_Abstract */
			$eventsData = static::getMapper()->getModel($row->toArray());
			$data[$eventsData->name] = $eventsData->value;
		}
		return $data;
	}	
}
