<?php
/**
 * Proxy to interacting with the users_smashups database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_UsersSmashups extends Application_Model_DbTable_AbstractTable
{
    protected $_name = 'users_smashups';
    
    /**
     * A reusable mapper object for this dbTable
     * 
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;

    /**
     * Gets the number of smashup picks for a particular matchup
     * 
     * @param array $matchupId  id of the matchup of interest
     *
     * @return returns the number of smashups already made for this match
     */
    public function getSmashupCount($matchupId) {
    	 
    	$select = $this->select('id')
    		->where("users_matchups_id = ?", $matchupId);
    	 
    	return $this->fetchAll($select)->count();
    	 
    }
    
    /**
     * Gets the smashup picks for a particular matchup and populates the players
     * in the provided arrays
     * 
     * @param array $matchupData  an array with three elements.  The first is a matchup model
     * 							  object, with id and info to get the smashup info.
     * 							  The second and third elements of the array are empty but
     * 							  structurally ready arrays to hold and organize all the 
     * 							  smashup picks, and a second storage for player by position.  
     * 						      Their keys are the positions
     * 							  that are allowed for this matchup and the sizes of the arrays
     * 							  equal the number to be filled for each position.
     * 
     * @throws My_Exception_HandledException  when the matchup is missing
     * 
     * @return nothing is returned as it updates the passed $matchupData array by reference
     */
    public function getSmashupsWithPlayers(&$matchupData) {
    	
    	if (!($matchupData['matchup'] instanceof Application_Model_UsersMatchups) ||
    		empty($matchupData['matchup']->id)
    	) {
    		throw new My_Exception_HandledException("The services are temporarily unavailable. Please try again later.");
    	}
    	 	
    	$select = $this->select()
    		->where("users_matchups_id = ?", $matchupData['matchup']->id);

    	$smashups = $this->getModelsFromRows($this->fetchAll($select));
   	
		foreach ($smashups as $smashup) {
			$matchupData[$smashup->users_id]["smashups"][$smashup->players_id] = $smashup;
			
			$player = Application_Model_DbTable_Players::getMapper()->find($smashup->players_id);
			
			$index = 0;
			$playersAtPosition = &$matchupData[$smashup->users_id]["players"][$player->position];
			$continue = true;
			while($continue) {
				if ($playersAtPosition[$index] === NULL) {
					$playersAtPosition[$index] = $player;
					$continue = false;
				} else {
					if (($index + 1) < count($playersAtPosition)) {
    					$index++;
    				} else {
    					$continue = false;
    				}
				}
			}
			$matchupData[$smashup->users_id]["players"]["team_ids"][] = $player->teams_id;
		}
    }
    
    /**
     * Gets the last smashup of the matchup
     * 
     * @param int $users_matchups_id  The id of the matchup of interest
     * @return Application_Model_UsersSmashups
     */
    public function getLastSmashup($users_matchups_id) {
    	$select = $this->select()
    		->where("users_matchups_id = ?", $users_matchups_id)
    		->order("id DESC")
    		->limit(1);
    	
    	$smashupRow = $this->fetchRow($select);
    	
    	return $this->getMapper()->getModel($smashupRow->toArray());
    }
    
    /**
     *  Gets the positional limitations on smashup picks
     *
     * @var int $settings_id the MarqueeMatchupSettings id from which to retreive the array.
     *
     * @return array|null  an array of positions for this SmashUp
     */
    public static function getSmashupArray($settings_id) {
    	return Application_Model_DbTable_MarqueeMatchupSettings::getSmashupArray($settings_id);
    }    
}
