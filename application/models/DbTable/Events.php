<?php
/**
 * Proxy to interacting with the events database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_Events extends Application_Model_DbTable_AbstractTable {
	protected $_name = 'events';
	
	/**
	 * A reusable mapper object for this dbTable
	 *
	 * @see Application_Model_DbTable_AbstractTable::getMapper()
	 * @var Application_Model_AbstractMapper
	 */
	protected static $mapper;
	
	
	/**
	 * Creates an event and notifies the Users
	 * 
	 * @param string $user_id          	The id of the user to be notified
	 * @param string $eventsTypeName	The name of the type of event being executed
	 * @param array $eventsData			The relevant data for this particular event
	 * 
	 * @return array  An array containing this particular Events object, the EventsType object, and all the
	 * 				  provided EventsData objects.
	 */
	public static function executeEvent($user_id, $eventsTypeName, array $eventsData) {
		
		$eventsType = Application_Model_DbTable_EventsTypes::getMapper()->getEventsTypeByName($eventsTypeName);
		/* @var $eventsType Application_Model_EventsTypes */
		
		if ($eventsType) {
			$results = array( "event" => null, "eventType" => $eventsType, "eventsData" => array() );
			$eventParameters = array(
								"users_id" => $user_id,
								"event_types_id" => $eventsType->id,
								"is_new" => 1
							);
			
			$event = static::getMapper()->getModel($eventParameters);
			$event->save();	
			
			$results["event"] = $event;
			
			$eventsDataMapper = Application_Model_DbTable_EventsData::getMapper();
			foreach($eventsData as $name => $value) {
				$eventsDataParameters = array(
											"events_id" => $event->id,
											"event_types_id" => $eventsType->id,
											"name" => $name,
											"value" => $value
						
										);
				$eventData = $eventsDataMapper->getModel($eventsDataParameters);	
				$eventData->save();
				
				$results["eventsData"][] = $eventData;
			}
			
			Application_Model_DbTable_NotificationManager::notifyOfEvent($results);
			return $results;
		}
		
		return null;
	}
	
	/**
	 * Gets new events for the specified users_id to be displayed on the frontend
	 *
	 * @param string $user_id          	The id of the user to be notified
	 *
	 * @return array  An array containing this particular Events object, the EventsType object, and all the
	 * 				  provided EventsData objects.
	 */
	public function getNewEvents($user_id, $cookie_stamp = null) {
	
		$eventsTypes = Application_Model_DbTable_EventsTypes::getModelsFromRows(Application_Model_DbTable_EventsTypes::getMapper()->fetchAll());
		
		$newEvents = array();
		foreach($eventsTypes as $eventsType) {
			
			$select = 
				$this->select()
					->where("users_id = ?", $user_id)
					->where("event_types_id = ?", $eventsType->id)
					->where("is_new = 1");
			
			$oldUnreadEvents = array();
			$newUnreportedEvents = array();
			if ($cookie_stamp) {				
				$select->where("cookie_stamp = ?", $cookie_stamp);
				// First we get the old events
				$oldUnreadEvents = $this->getModelsFromRows($this->fetchAll($select));
			
				// now we check for new events
				$select = $this->select()
					->where("users_id = ?", $user_id)
					->where("event_types_id = ?", $eventsType->id)
					->where("is_new = 1")
					->where("cookie_stamp != ? OR cookie_stamp IS NULL", $cookie_stamp);			
			}
					
			$newUnreportedEvents = $this->getModelsFromRows($this->fetchAll($select));
			
			if (!empty($newUnreportedEvents)) {
				$allUnreadEvents = array_merge($newUnreportedEvents, $oldUnreadEvents);
				$newEvents[$eventsType->name] = array();
				
				foreach($allUnreadEvents as $event) {
					$eventsData =
						Application_Model_DbTable_EventsData::getMapper()->findDataByEventsId($event->id);
					
					$newEvents[$eventsType->name][] = array( "event" => $event, "eventType" => $eventsType, "eventsData" => $eventsData );				
				}
			}
				
		}
	
		return $newEvents;
	}	
}
