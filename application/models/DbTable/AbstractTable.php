<?php

/**
 * Abstract Table superclass which sets up reusable mapper for this dbTable type
 * 
 * @author Leon McCottry
 *
 */
abstract class Application_Model_DbTable_AbstractTable extends Zend_Db_Table_Abstract {

	/**
	 * Gets a reusable mapper for this table type
	 * 
	 * @throws Exception  Throws exception when a static $mapper value is not defined in subclass
	 * @return Application_Model_AbstractMapper
	 */
	public static function getMapper() {
		$prop = new ReflectionProperty(get_called_class(), 'mapper');
		if (!property_exists(get_called_class(), "mapper") || !$prop->isStatic()) {
			throw new Exception(get_called_class().' must declare a static $mapper property to use this method.');
		}
		
		if (!isset(static::$mapper)) {
			$mapperName = "Application_Model_".substr_replace(get_called_class(), '', 0, 26)."Mapper";
			static::$mapper = new $mapperName();
		}		
		return static::$mapper;				
	}

	/**
	 * Returns a row of model objects of this table created from the data rows from this table
	 * 
	 * @param Zend_Db_Table_Rowset_Abstract $dataRows
	 * @return array[Application_Model_AbstractModel]
	 */
	public static function getModelsFromRows($rowset) {
		$models = array();
		
		foreach($rowset as $row) {
			/* @var $row Zend_Db_Table_Row_Abstract */
			$models[] = static::getMapper()->getModel($row->toArray());
		}	
		return $models;
	}
}

?>