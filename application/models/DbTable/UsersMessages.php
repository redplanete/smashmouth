<?php
/**
 * Proxy to interacting with the users_messages database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_UsersMessages extends Application_Model_DbTable_AbstractTable
{
    protected $_name = 'users_messages';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;
}

