<?php
/**
 * Proxy to interacting with the users_matchups database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_UsersMatchups extends Application_Model_DbTable_AbstractTable
{
    protected $_name = 'users_matchups';

    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;
    
    /**
     * Gets all matchups of two particular users
     * 
     * @param Application_Model_Users $user1
     * @param Application_Model_Users $user2
     * @param array|null $sportsIds  ids of the sports of interest
     * 								if set to an empty array or null, all sports are selected. This is the default.
	 * 
     * @param array|null $statuses  the status of the matchup: 'completed','requested','in_progress','declined','deleted'
     * 						   the default is array('requested','in_progress') for all currently active games
     * 						   if set to an empty array or null, all status values are used
     * 
     * @return array[Application_Model_AbstractModel]
     */
    public static function getExistingMatchups(Application_Model_Users $user1, Application_Model_Users $user2, $sportIds = null, array $statuses = array("requested", "accepted", "in_progress"))
    {
    	$matchupDb = new Application_Model_DbTable_UsersMatchups();
    	
    	$select = $matchupDb->select()
    				->where("((home_user_id = {$user1->getId()} AND away_user_id = {$user2->getId()}) OR (home_user_id = {$user2->getId()} AND away_user_id = {$user1->getId()}))");

    	$sportsWhere = "sports_id IN (?)";
    	if (count($sportIds)) $select->where($sportsWhere, $sportIds);
    	    	
    	$statusWhere = "status IN (?)";
    	if (count($statuses)) $select->where($statusWhere, $statuses);
    	
    	return self::getModelsFromRows($matchupDb->fetchAll($select));  	
    }
        
    /**
     * Gets all matchups for the the user
     * 
     * @param Application_Model_Account $user  The account of the user whose matchups are to be retrieved
     * 
     * @param array $statuses  the status of the matchup: 'completed','requested', 'accepted', 'in_progress','declined','cancelled'
     * 						   the default is array('requested','in_progress') for all currently active games
     * 						   if set to an empty array or null, all status values are used
     *
     * @param array $roles     whether to return matchups where the initiator is the "initiator", or the "recipient"
     * 						   the default is array("initiator","recipient") representing return both cases
     * 						   if set to an empty array or null, all both cases are used
     * 
     * @param array $orderBy   An array column_name [ASC|DESC] to order the records retrieved.
     * 					  	   The default is null or an empty array for standard ordering
     * 
     * @param int|null $limit  A number to limit the returned set to that amount
     * 						   This is standard MySQL. The default is null for no limit
     * 
     * @param int|null $offset The number of records to offest in the returned result
     * 						   the default is null for no offset
     * 
     * @return multitype:Application_Model_AbstractModel
     */
    public static function getMatchupsForAccount(Application_Model_Users $user = null, $statuses = array("requested", 'accepted', "in_progress"), $roles = array("initiator","recipient"), $orderBy = null, $limit = null, $offset = null )
    {
    	if (!$user) $user = Application_Model_Account::getAccountFromIdentity()->getUser();
    
    	$matchupDb = new Application_Model_DbTable_UsersMatchups();
    	$select = $matchupDb->select()
    				->where("(home_user_id = " . $user->getId() . " OR away_user_id = " . $user->getId() . ")");
  	
    	if (empty($roles) || (in_array("initiator",$roles) && in_array("recipient", $roles)) ) {
    	} else if (in_array("initiator",$roles)) {
    		$select->where("challenge_initiator = ?", $user->getId());
    	} else if (in_array("recipient", $roles)) {
    		$select->where("challenge_initiator != ?", $user->getId());
    	}
    	
    	$statusWhere = "status IN (?)";
    	if (count($statuses)) $select->where($statusWhere, $statuses);
    	
    	if ($orderBy) $select->order($orderBy);
    	$select->limit($limit, $offset);
    	
		return self::getModelsFromRows($matchupDb->fetchAll($select)); 
    }
    
    /**
     *  Retreives the leader board for a particular sport
     *  
     *  @var $sports_id   The id of the sport which we are seeking leaders for.  Defaults to 1 
     *  				  for NBA Individual League.
     *  
     *  @return array containing the current leaders in percentage win descending order.  The fields
     *  		are id for users_id, name for the user's first and last name, wins, losses, and percentage. 
     */
    public function getLeaderBoard($sports_id = 1, $pageIndex = null, $limit = null) {
    	$sports_id = (int) $sports_id;
 		$sql = <<<SQL
SELECT id, 
       name, 
       wins, 
       losses, 
       wins / ( wins + losses ) AS percentage 
FROM   (SELECT u.id                                   AS id, 
               login_name AS name, 
               (SELECT Count(*) 
                FROM   users_matchups um 
                WHERE  ( ( um.home_team_score > um.away_team_score 
                           AND u.id = um.home_user_id ) 
                          OR ( um.away_team_score >= um.home_team_score 
                               AND u.id = um.away_user_id ) ) 
                       AND (um.status = "completed" AND um.sports_id = $sports_id))   AS wins, 
               (SELECT Count(*) 
                FROM   users_matchups um 
                WHERE  ( ( um.home_team_score <= um.away_team_score 
                           AND u.id = um.home_user_id ) 
                          OR ( um.away_team_score < um.home_team_score 
                               AND u.id = um.away_user_id ) ) 
                       AND (um.status = "completed" AND um.sports_id = $sports_id))   AS losses 
        FROM   users u) AS Leaders 
ORDER  BY percentage DESC
SQL;
 		
 		$statement = $this->getAdapter()->query($sql);
 		
 		return $statement->fetchAll();
    }
}


 		