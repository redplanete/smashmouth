<?php
/**
 * Proxy to interacting with the games database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_Games extends Application_Model_DbTable_AbstractTable
{
    protected $_name = 'games';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;
    
    
	/**
	 * Retrieves the marquee matchups as defined by the marquee_matchup_settings table.
	 * 
	 * This includes the games, teams, and player model objects.  This is an essential function
	 * for the smashboard and smashup process.
	 * 
	 * @param int  $settings_id that identifies the sport, and rules for the matchups
	 * 
	 * @throws My_Exception_HandledException  thrown when setting can not be found or the returned
	 * 										  data violates these settings
	 * 
	 * @return array of models in hierarchal form.  The data is in this format with these keys
	 * 
	 * 							interger indexed array of:
	 * 
	 * 									array('game'--Games model , and 'teams' --2D array of teams and players)
	 * 									the two team keys are "home" and "away"
	 * 									each team array has a key "team" which is the actual team model
	 * 									and an array index by 'players' which holds the players
	 * 									the players are stored in an associative array where the keys are the positions
	 * 									'PG','SG','F/C'
	 */
    public function getMarqueeMatchups($settings_id) {
    	
    	$marqueeSettings = Application_Model_DbTable_MarqueeMatchupSettings::getMapper()->find($settings_id);
    	
    	if (!$marqueeSettings) {
    		throw new My_Exception_HandledException("This service is temporarily unavailable.  Please try again later.");
    	}
    	
		$matchups = Application_Model_DbTable_MarqueeMatchups::getMapper()->fetchAllBySettingsId($settings_id);
    	
    	if (count($matchups) != $marqueeSettings->number_of_matchups) {
    		throw new My_Exception_HandledException("Marquee Matches have not yet been finalized for this week.\nPlease try again later.");
    	}
    	
    	$games = array();
    	foreach($matchups as $matchup) {
    		$games[] = $this->getMapper()->find($matchup->games_id);
    	}
    	
    	$marqueeMatchups = array();
    	$players_id_map = array();
    	
    	$i = -1;
    	foreach($games as $game) {
    		$i++;
    		$teams = array();
    		$marqueeMatchups[] = array('game' => $game, 'teams' => $teams );
    		
    		$homePlayers = array( 'PG' => array(), 'SG' => array(), "F/C" => array());
    		$awayPlayers = array( 'PG' => array(), 'SG' => array(), "F/C" => array());
    		
    		// add the two teams 
    		$marqueeMatchups[$i]['teams']['home'] = array( "team" => Application_Model_DbTable_Teams::getMapper()->find($game->home_team_id), "players" => $homePlayers );
    		$marqueeMatchups[$i]['teams']['away'] = array( "team" => Application_Model_DbTable_Teams::getMapper()->find($game->away_team_id), "players" => $awayPlayers );
    		   		
    		foreach( $marqueeMatchups[$i]['teams'] as $key => $team ) {
    			// get the players
    			$playersDb = new Application_Model_DbTable_Players();
    			$playersQuery = $playersDb->select()
    				->where('teams_id = ?', $marqueeMatchups[$i]['teams'][$key]['team']->id)
    				->where('active = 1')
    				->order(new Zend_Db_Expr("CASE position WHEN 'PG' THEN 1 WHEN 'SG' THEN 2 ELSE 3 END"));

    			$players = $playersDb->getModelsFromRows($playersDb->fetchAll($playersQuery));
    			
    			foreach( $players as $player ) {
    				$marqueeMatchups[$i]['teams'][$key]["players"][$player->position][] = $player;
    				$index = count($marqueeMatchups[$i]['teams'][$key]["players"][$player->position]) - 1;
    				$players_id_map[$player->id] = "$i,teams,$key,players,{$player->position},$index";
    			}
    		}
    	}   
		
    	$marqueeMatchups['players_id_map'] = $players_id_map;
    	return $marqueeMatchups;
    }
    
    
	/**
	 * Gets the games for the given season inclusive-between the dates given
	 * 
	 * @param int $seasons_id			The season of concern
	 * @param date $smashup_open_date	inclusive open date of games
	 * @param date $smashup_close_date	inclusive closing date
	 * @param bool $returnMarqueeOnly  	if set to true, only Marquee matchups are returned, false be default
	 * 
	 * @return array[Application_Model_Games]
	 */
    
    public function gameExists($uuid) {
        
        $result = false;
        
        $select = $this->select()
                ->where("uuid = ?", $uuid);
        
        $rows = $this->fetchAll($select);
        
        if (count($rows) > 0) {
            $result = true;
        }
        
        return $result;
    }
    
    public function addGame($uuid, $home_team_uuid, $away_team_uuid, $scheduled_date) {
        
        
        $adapter = $this->getAdapter();
        $statement = $adapter->query("SELECT id from teams where uuid = '$home_team_uuid'");
        $row = $statement->fetch();
        $home_team_id = $row["id"];        
        
        $statement = $adapter->query("SELECT id from teams where uuid = '$away_team_uuid'");
        $row = $statement->fetch();
        $away_team_id = $row["id"];        
        
        
        $data = array( 
		"sports_id" => 1,
		"uuid" => $uuid,
		"seasons_id" => 1,
		"home_team_id" => $home_team_id,
		"away_team_id" => $away_team_id,
		"scheduled_date" => new Zend_Db_Expr("timestamp('$scheduled_date') - INTERVAL 4 HOUR"),
		"date_added" => null,
		"home_team_score" => null,
		"away_team_score" => null,
		"status" => "pending",
		"period" => null,
		"time" => null
            );
        
        
        $this->insert($data);
        
    }
    
    
    public function getGamesByDateRange($seasons_id, $smashup_open_date, $smashup_close_date, $returnMarqueeOnly = false, $settings = array()) {

    	$select = $this->select()
	    	->where("seasons_id = ?", $seasons_id)
	    	->where("DATE(scheduled_date) BETWEEN '{$smashup_open_date}' AND '{$smashup_close_date}'");
    	 
    	$rows = $this->fetchAll($select);
    	
    	$games = $this->getModelsFromRows($rows);
    	
    	if ($returnMarqueeOnly) {
    		$allGames = $games;
    		$settingsDb = new Application_Model_DbTable_MarqueeMatchupSettings();
    		
    		if (empty($settings)) {
    			$settings = $settingsDb->getAllSettingsBySeason($seasons_id);
    		}
    		
			$games = array();
			
			$marqueeMatchupsMapper = Application_Model_DbTable_MarqueeMatchups::getMapper();
			foreach($allGames as $game) {
				$isMarquee = false;
				
				foreach($settings as $setting) {
					$isMarquee = $marqueeMatchupsMapper->findByGameIdAndSettingsId($game->id, $setting->id);
					
					if ($isMarquee) break;
				}
				
				if ($isMarquee) $games[] = $game;
			}
    	}
    	 
    	return $games;
    }   
}
