<?php
/**
 * Proxy to interacting with the teams database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_Teams extends Application_Model_DbTable_AbstractTable
{

    protected $_name = 'teams';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;

    public function getallteam()
    {
	$select = $this->select();
        $select->from($this->_name);

        $data =  $this->fetchAll($select);
        if($data)
            return $data->toArray();
        else return false;
    }
}

?>