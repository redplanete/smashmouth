<?php
/**
 * Proxy to interacting with the league database table
 *
 * @author Leon McCottry
 *
 */
class Application_Model_DbTable_League extends Application_Model_DbTable_AbstractTable {
    protected $_name = 'league';
    
    /**
     * A reusable mapper object for this dbTable
     *
     * @see Application_Model_DbTable_AbstractTable::getMapper()
     * @var Application_Model_AbstractMapper
     */
    protected static $mapper;
}
