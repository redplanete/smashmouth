<?php
/**
 * The MarqueeMatchups for a particular MarqueeMatchupSettings.  This represents
 * the group of games that are used for a particular matchup.
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_MarqueeMatchups extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"marquee_matchup_settings_id" => null,
			'games_id' => null
		);
	}
}

