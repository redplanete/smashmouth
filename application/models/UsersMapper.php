<?php
/**
 * Mapper for the Users Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_UsersMapper extends Application_Model_AbstractMapper
{
	
	/**
	 * Finds and user by e-mail
	 *
	 * @param string $email  The e-mail address of the user
	 *
	 * @return Application_Model_Users|null
	 */
	public function findByEmail($email)
	{
		if ( is_null($email) ) return null;
	
		$select = $this->getDbTable()->select();
		$select->where("email = ?", $email);
	
		$row = $this->getDbTable()->fetchRow($select);
	
		if (!$row) {
			return null;
		}
	
		return $this->getModel()->setData($row->toArray());
	}
	
	/**
	 * Finds and user by e-mail and change_pass_key in order to allow password reset
	 *
	 * @param string $email  The e-mail address of the user
	 * @param string $token	 The random token to verify the user is valid
	 *
	 * @return Application_Model_Users|null
	 */
	public function findByEmailAndKey($email, $key)
	{
		if ( is_null($email) || is_null($key) ) return null;
	
		$select = $this->getDbTable()->select()
			->where("email = ?", $email)
			->where("change_pass_key LIKE ?", "%$key%");
	
		$row = $this->getDbTable()->fetchRow($select);
	
		if (!$row) {
			return null;
		}
	
		return $this->getModel()->setData($row->toArray());
	}	
}

