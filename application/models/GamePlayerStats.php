<?php

/**
 * A table that stores the individual stats for each player in each sport in a 
 * one to many construct.
 * 
 * @author Leon McCottry
 */
class Application_Model_GamePlayerStats extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"seasons_id" => null,
			"games_id" => null,
			"teams_id" => null,
			"players_id" => null,
			"stat" => null,
			"value" => null			
		);
	}
	
}

