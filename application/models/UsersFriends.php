<?php
/**
 * The UsersFriends model keeps track of friend request, responses and who is who's friend
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_UsersFriends extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"sender_users_id" => null,
			"receiver_users_id" => null,
			"status" => null,
			"is_blocked_by_sender" => null,
			"is_blocked_by_receiver" => null,
			"invite_link" => null,
		);
	}
}

