<?php
/**
 * Mapper for the MarqueeMatchupSettings Model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_MarqueeMatchupSettingsMapper extends Application_Model_AbstractMapper
{
	
	/**
	 * Finds and populates the last marqueee matchup settings by season id
	 *
	 * @param string $seasons_id  The seasons id of the player to be retrieved
	 *
	 * @return Application_Model_MarqueeMatchupSettings|null
	 */
	public function findBySeasonsId($seasons_id)
	{
		if ( is_null($seasons_id) ) return null;
	
		$select = $this->getDbTable()->select();
		$select->where("seasons_id = ?", $seasons_id)
				->order("id DESC");
	
		$row = $this->getDbTable()->fetchRow($select);
	
		if (!$row) {
			return null;
		}
	
		return $this->getModel()->setData($row->toArray());
	}
}

