<?php

class Application_Model_UserLeagueSeed {
	protected $_userLeagueSeed;
	
	public function __construct(Zend_Db_Table_Row $userLeagueSeed) {
		$this->_userLeagueSeed = $userLeagueSeed;
	}
	
	public static function createUserLeague($data) {
		$log = Zend_Registry::get('log');
		$leagueDb = new Application_Model_DbTable_UserLeagueSeed();
		foreach (array("league_id", "user_id", "seed") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $leagueDb->info(Zend_Db_Table_Abstract::COLS);
		$userLeagueSeed = null;
		try {
			$userLeagueSeed = $leagueDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$userLeagueSeed->$column = $data[$column];
				}
			}
			$userLeagueSeed->save();
			return new self($userLeagueSeed);
		} catch (Zend_Db_Exception $ex) {
			$error = $ex->getMessage();
			$log->info($error);
			$userLeagueSeed->delete();
			if (strisstr($error, "Duplicate entry")) {
				throw new Exception("Duplicate user league seed found");
			} else {
				throw new Exception("An error occured during user league seed creation", 1);
			}
		}
	}
	public static function deleteUserLeagueSeed($data) {
		$leagueDb = new Application_Model_DbTable_UserLeagueSeed();
		$select = $leagueDb->select();
		foreach (array("league_id", "user_id_1", "user_id_2", "bracket") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
			$select->where("{$field} = ?", $data[$field]);
		}
		$record = $leagueDb->fetchRow($select);
		if ($record) {
			$record->delete();
		}
	}
	public static function getUserLeagueSeed($data) {
		$leagueDb = new Application_Model_DbTable_UserLeagueSeed();
		$select = $leagueDb->select();
		if (is_numeric($data)) {
			$select->where("id = ?", $data);
		} else {
			$fieldCount = 0;
			foreach (array("league_id", "user_id", "seed") as $field) {
				if (isset($data[$field])) {
					if (empty($data[$field])) {
						throw new Exception("{$field} empty", 1);
					} else {
						$fieldCount++;
						$select->where("{$field} = ?", $data[$field]);
					}
				}
			}
			if ($fieldCount == 0) {
				throw new Exception("missing a column");
			}
		}
		$record = $leagueDb->fetchRow($select);
		if ($record) {
			return new self($record);
		}
		return false;
	}
	public static function getUserLeagueSeeds($data) {
		$leagueDb = new Application_Model_DbTable_UserLeagueSeed();
		$select = $leagueDb->select();
		$fieldCount = 0;
		foreach (array("league_id", "user_id", "seed") as $field) {
			if (empty($data[$field])) {
				throw new Exception("{$field} empty", 1);
			}
			$fieldCount++;
			$select->where("{$field} = ?", $data[$field]);
		}
		if ($fieldCount == 0) {
			throw new Exception("missing a column");
		}
		$records = $leagueDb->fetchAll($select);
		if ($records) {
			$leagues = array();
			foreach ($records as $record) {
				$leagues[] = new Application_Model_UserLeague($record);
			}
			return $leagues;
		}
		return false;
	}
	
	public function __get($name) {
		$leagueDb = new Application_Model_DbTable_UserLeagueSeed();
		$cols = $leagueDb->info(Zend_Db_Table_Abstract::COLS);
		if (in_array($name, $cols)) {
			return $this->_userLeagueSeed->$name;
		}
		return false;
	}
	public function __set($name, $value) {
		$leagueDb = new Application_Model_DbTable_UserLeagueSeed();
		$cols = $leagueDb->info(Zend_Db_Table_Abstract::COLS);
		if (in_array($name, array("id"))) {
			throw new Exception("Cannot modify the field {$name} of a user league", 1);
		}
		if (in_array($name, $cols)) {
			$this->_userLeagueSeed->$name = $value;
			try {
				$this->_userLeagueSeed->save();
			} catch (Zend_Db_Exception $ex) {
				throw new Exception($ex->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}

?>