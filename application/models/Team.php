<?php

class Application_Model_Team
{
	protected $_team;

	public function __construct(Zend_Db_Table_Row $team)
	{
		$this->_team = $team;
	}

	public static function createTeam($data)
	{
		$log = Zend_Registry::get('log');
		$teamDb = new Application_Model_DbTable_Team();
		foreach (array("name", "team_id", "season_id") as $field) {
			if (!isset($data[$field]) || empty($data[$field])) {
				throw new Exception("{$field} missing or empty", 1);
			}
		}
		$columns = $teamDb->info(Zend_Db_Table_Abstract::COLS);
		try {
			$team = $teamDb->fetchNew();
			foreach ($columns as $column) {
				if (array_key_exists($column, $data)) {
					$team->$column = $data[$column];
				}
			}
			$team->save();
			return new self($team);
		} catch (Zend_Db_Exception $ex) {
			$log->info($ex->getMessage());
			$team->delete();
			if (stristr($ex->getMessage(), 'Duplicate entry')) {
				throw new Exception("Duplicate team found.");
			} else {
				throw new Exception("An error occured during team creation", 1);
			}
		}
	}
	public static function getAllTeams($data) {
		if (!isset($data["sport_id"])) {
			throw new Exception("sport_id");
		}
		$teamDb = new Application_Model_DbTable_Team();
		$select = $teamDb->select();
		$select->where("sport_id = {$data["sport_id"]}");
		$records = $teamDb->fetchAll();
		if ($records) {
			$teams = array();
			foreach ($records as $record) {
				$teams[] = new self($record);
			}
			return $teams;
		}
		return false;
	}
	public static function getTeam($data)
	{
		$teamDb = new Application_Model_DbTable_Team();
		$select = $teamDb->select();
		if (is_numeric($data)){
			$select->where("team_id = {$data}");			
		} else {
			if (isset($data["team_id"])) {
				$select->where("team_id = {$data["team_id"]}");
			}
			if (isset($data["name"])) {
				$select->where("name = {$data["name"]}");
			}
		}
                
		$record = $teamDb->fetchRow($select);

		if ($record) {
			return new self($record);
		}
		return false;
	}

	public function getPlayers($data) {
		$playerDb = new Application_Model_DbTable_Player();
		$select = $playerDb->select();
		foreach (array("sport_id") as $field) {
			if (!isset($data[$field])) {
				throw new Exception("{$field} missing");
			}
			$select->where("{$field} = {$data[$field]}");
		}
		$select->where("team_id = {$this->team_id}");
		if (isset($data["position"])) {
			$select->where("position = ?", $data["position"]);
		}
		$records = $playerDb->fetchAll($select);
		if ($records) {
			$players = array();
			foreach ($records as $record) {
				$players[] = new Application_Model_Player($record);
			}
			return $players;
		}
		return false;
	}
	public function getMarquisMatchup($data) {
		foreach (array("sport_id", "season_id") as $field) {
			if (!isset($data[$field])) {
				throw new Exception("{$field} missing");
			}
		}
		$marquisData = array();
		$marquisData["sport_id"] = $data["sport_id"];
		$marquisData["season_id"] = $data["season_id"];
		$marquisMatchups = Application_Model_MarquisMatchup::getAllMarquisMatchups($marquisData);
		foreach ($marquisMatchups as $marquisMatchup) {
			$matchupData = array();
			$matchupData["sport_id"] = $marquisMatchup->sport_id;
			$matchupData["match_id"] = $marquisMatchup->match_id;
			$matchupData["season_id"] = $marquisMatchup->season_id;
			$matchup = Application_Model_Matchup::getMatchup($matchupData);
			$teamID = $this->team_id;
			if ($teamID == $matchup->home_team_id ||
				$teamID == $matchup->away_team_id) {
				return $matchup;
			}
		}
		return false;
	}
	public function getId()
	{
		return $this->_team->team_id;
	}

	public function getName()
	{
		return $this->_team->name;
	}

	public function getImage() {
		return "/downloads/" . $this->_team->sport_id . "/" . $this->getId() . "/Logo.png";
	}

	public function __get($name)
	{
		$teamsDb = new Application_Model_DbTable_Team();
		$cols = $teamsDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, $cols))
		{
			return $this->_team->$name;
		}

		return false;
	}

	public function __set($name, $value)
	{
		$teamsDb = new Application_Model_DbTable_Team();
		$cols = $teamsDb->info(Zend_Db_Table_Abstract::COLS);

		if(in_array($name, array('id')))
		{
			throw new Exception("Cannot modify an team {$name} value", 1);
		}

		if(in_array($name, $cols))
		{
			$this->_team->$name = $value;

			try {
				$this->_team->save();
			} catch(Zend_Db_Exception $e)
			{
				throw new Exception($e->getMessage(), 1);
			}
			return true;
		}
		return false;
	}
}
