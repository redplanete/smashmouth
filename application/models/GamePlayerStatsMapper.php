<?php
/**
 * Mapper for the GamePlayerStats model
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_GamePlayerStatsMapper extends Application_Model_AbstractMapper
{
	/**
	 * Finds and populates the stats by the game's id and player's id
	 *
	 * @param int $games_id  		id of the game of interest
	 * @param int $players_id	 	id of the player of interest
	 *
	 * @return array[GamePlayerStats]
	 */
	public function findByGameAndPlayer($games_id, $players_id)
	{
		if ( is_null($games_id) || is_null($players_id)) return null;
	
		$select = 
			$this->getDbTable()->select()
				->where("games_id = ?", $games_id)
				->where("players_id = ?", $players_id);
	
		$rows = $this->getDbTable()->fetchAll($select);
	
		return $this->getDbTable()->getModelsFromRows($rows);
	}
}

