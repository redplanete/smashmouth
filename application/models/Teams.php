<?php
/**
 * This model list all the teams in the various sports with SmashMouth supports
 * 
 * @author Leon McCottry
 *
 */
class Application_Model_Teams extends Application_Model_AbstractModel
{
	/**
	 * The constructor is used to define the data which initially will be an associative
	 * array with the keys being the database columns and all values typically set to null.
	 */
	public function __construct() {
		$this->data = array(
			"id" => null,
			"sports_id" => null,
			"uuid" => null,
			"name" => null,
			"acronym" => null
		);
	}

}