<?php
use library\My\Exception\My_Exception_HandledException as HandledException;

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	
	public function run() {
		try {
			return parent::run();
		} catch (My_Exception_HandledException $e) {
			// The HandledException should take care of everything.
		}
	}
	
    protected function _initLogger() {
        $use_firebug = false;

        if(PHP_SAPI == 'cli' && $use_firebug) {
        	$writer = new Zend_Log_Writer_Stream('php://output');
        } else if (!$use_firebug) {
            $writer = new Zend_Log_Writer_Stream(array("stream" => dirname(__FILE__) . "/../data/debug.txt"));
        } else {
            $writer = new Zend_Log_Writer_Firebug();
        }

        $logger = new Zend_Log($writer);
        Zend_Registry::set('log', $logger);
    }

    protected function _initAutoload() {
        if(APPLICATION_ENV != 'testing' && PHP_SAPI != 'cli') {
            $settings = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
            Zend_Registry::set('options', $settings->modules);

            // $fc->registerPlugin(new Application_Plugin_FlashPlugin());
        }
    }

    protected function _initRouter() {
        if (PHP_SAPI == 'cli') {
            include_once(APPLICATION_PATH . "/routers/Cli.php");
            $this->bootstrap('frontcontroller');
            $front = $this->getResource('frontcontroller');
            $front->setRouter (new Application_Router_Cli());
            $front->setRequest (new Zend_Controller_Request_Simple());
            $front->setResponse(new Zend_Controller_Response_Cli());
            // $this->_helper->viewRenderer->setNoRender(true);

            $this->bootstrap('layout');
            $layout = $this->getResource('layout');
            $layout->disableLayout();
        }
    }

    protected function _initSession() {
        if(PHP_SAPI != 'cli') {
        	
        	session_name("smashmouthFantasySports");
        	
        	$settings = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        	$dbConfig = $settings->resources->db->params;
        	
        	$db = Zend_Db::factory($settings->resources->db->adapter, array(
        			'host'        =>$dbConfig->host,
        			'username'    => $dbConfig->username,
        			'password'    => $dbConfig->password,
        			'dbname'    => $dbConfig->dbname
        	));
        	
        	Zend_Db_Table_Abstract::setDefaultAdapter($db);
        	$config = array(
        			'name'           => 'session',
        			'primary'        => 'id',
        			'modifiedColumn' => 'modified',
        			'dataColumn'     => 'data',
        			'lifetimeColumn' => 'lifetime'
        	);
        	
        	Zend_Session::setSaveHandler(new My_Db_Table_SessionSaveHandler($config));
        	
            Zend_Session::start();
            $namespace = new Zend_Session_Namespace('playcongress');
            Zend_Registry::set('session',$namespace);
        }
    }

    protected function _initCache() {
        if(APPLICATION_ENV != 'testing') {
            $frontendOptions = array(
                    'lifetime' => 300, // cache lifetime of 2 hours
                    'automatic_serialization' => true
            );

            $cacheDir = dirname(dirname(__FILE__)) . "/data";

            $backendOptions = array(
                    'cache_dir' => $cacheDir . "/cache" // Directory where to put the cache files
            );

            // getting a Zend_Cache_Core object
            $dbMetaCache = Zend_Cache::factory('Core',
                    'File',
                    $frontendOptions,
                    $backendOptions);

            Zend_Db_Table_Abstract::setDefaultMetadataCache($dbMetaCache);

            $frontendOptions = array(
                    'lifetime' => 86400,
                    'debug_header' => false, // for debugging
                    'default_options' => array(
                            'cache' => false,
                            'cache_with_cookie_variables' => true,
                            'cache_with_session_variables' => true,
                            'make_id_with_session_variables' => false,
                            'make_id_with_cookie_variables' => false
                    ),
                    'regexps' => array(
                            '^/congressperson/photo' => array(
                                    'cache' => true,
                                    'cache_with_get_variables' => true,
                                    'make_id_with_get_variables' => true,
                                    'memorize_headers' => true
                            ),
                            '^/xcongressperson/list' => array(
                                    'cache' => true,
                                    'cache_with_get_variables' => true,
                                    'make_id_with_get_variables' => true,
                                    'cache_with_post_variables' => true,
                                    'make_id_with_post_variables' => true,
                                    'memorize_headers' => true
                            ),
                            '^/xindex/topplayers' => array(
                                    'cache' => true
                            )
                    )
            );

            $backendOptions = array(
                    'cache_dir' => $cacheDir . "/viewcache" // Directory where to put the cache files
            );

            // getting a Zend_Cache_Frontend_Page object
            $cache = Zend_Cache::factory('Page',
                    'File',
                    $frontendOptions,
                    $backendOptions);

            $cache->start();
        }

    }

    protected function _initTranslations() {
        $english = array('test'=>'test');

        $translate = new Zend_Translate('array', $english, 'en');
        //$translate->addTranslation(array('content' => $english, 'locale' => 'en_US'));

        Zend_Registry::set('tr', $translate);
    }

    protected function _initLocale() {
    	$settings = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
    	
    	date_default_timezone_set($settings->params->ServerTimeZone);
        $locale = new Zend_Locale('en_US');
        Zend_Registry::set('Zend_Locale', $locale);
    }
    
    protected function _initFacebook() {
    	$settings = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $facebook_settings = $settings->resources->facebook;
    	
        Zend_Registry::set('Facebook', $facebook_settings);
    }

    protected function _initDoctype() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->setEncoding('UTF-8');
        $view->doctype('HTML5');
    }

    protected function _initViewHelpers() {
        Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH .'/controllers/helpers');

        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        $view->addHelperPath('My/View/Helper/', 'My_View_Helper');

        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
    }

    protected function _initMessenger() {
        $this->bootstrap('layout');
    }

    protected  function _initNavigation() {
        $this->bootstrap('layout');

        /*if(APPLICATION_ENV != 'testing')
    	{
	    	$acl = Zend_Registry::get('acl');
	        $layout = $this->getResource('layout');
	        $view = $layout->getView();
	        $role = Zend_Auth::getInstance()->getStorage()->read()->user_type;

	        $pages = array(
	    		array(
	    			'label' => 'Login',
	    			'controller' => 'authentication',
	    			'action' => 'login',
	    			'resource' => 'authentication',
	    			'privilege' => 'login'
	    		),
			);

	        $navigation = new Zend_Navigation($pages);
			$view->navigation($navigation)->setAcl($acl)->setRole($role);
		}*/
    }

    protected function _initEmail() {
        $settings = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV);
        $mailConfig = $settings->resources->mail;
        Zend_Registry::set('mail_config', $mailConfig);

        if($mailConfig->transport->type == 'smtp') {
            $mailTrans = new Zend_Mail_Transport_Smtp($mailConfig->transport->host, $mailConfig->transport->toArray());
        }
        else {
            $mailTrans = new Zend_Mail_Transport_Sendmail();
        }

        Zend_Mail::setDefaultTransport($mailTrans);
        Zend_Mail::setDefaultFrom($mailConfig->from_email, $mailConfig->from_name);
    }


    protected function _initRoutes() {
        $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/routes.ini');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $router->addConfig($config, 'routes');
    }
}

