<?php

class Application_Plugin_AccessCheck extends Zend_Controller_Plugin_Abstract
{
	private $_acl = null;
	private $_auth = null;

	public function __construct(Zend_Acl $acl, Zend_Auth $auth)
	{
		$this->_acl = $acl;
		$this->_auth = $auth;
	}

	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$log = Zend_Registry::get('log');
		$resource = $request->getControllerName();
		$action = $request->getActionName();
		$response = $this->getResponse();

		if($request->isXmlHttpRequest())
		{
			// Access-Control-Allow-Origin
			$response->setHeader('Access-Control-Allow-Origin', 'http://*.playcongress.com');
			$response->setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
			$response->setHeader('Access-Control-Allow-Headers', 'X-PINGOTHER');
			$response->setHeader('Access-Control-Max-Age', '1728000');
		}

		$identity = $this->_auth->getStorage()->read();

		$role = $identity->user_type;

        $namespace = Zend_Registry::get('session');

		if(!$this->_acl->isAllowed($role, $resource, $action))
		{
			$layout = Zend_Layout::getMvcInstance();
			$layout->error = "Permission required to access {$resource}/{$action}";

			$request->setControllerName('index')
				->setActionName('index');
		} else if ($identity && isset($namespace->invitation_id) && !empty($namespace->invitation_id)) {
			$request->setControllerName('league')->setActionName('join');
		}
	}
}