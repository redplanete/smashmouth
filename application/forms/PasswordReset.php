<?php

class Application_Form_PasswordReset extends Zend_Form
{

    public function init()
    {
       	/* Form Elements & Other Definitions Here ... */
    	$this->setName('password_reset');
    	$this->setMethod('post');
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/authentication/passwordreset');

    	$cpk = new Zend_Form_Element_Hidden('change_pass_key');
    	$cpk->setRequired(true);

    	$username = new Zend_Form_Element_Hidden('email');
    	$username->setRequired(true);

    	$passConfirm = new My_Validate_PasswordConfirmation();

		$password = new Zend_Form_Element_Password('login_pass');
    	$password->setLabel('Password:')
    		->addValidator($passConfirm);

    	$cpassword = new Zend_Form_Element_Password('password_confirm');
    	$cpassword->setLabel('Confirm Password:')
    		->setRequired(true);

    	$login = new Zend_Form_Element_Submit('login');
    	$login->setLabel('Reset Password');

    	$this->addElements(array($cpk, $username, $password, $cpassword, $login));
    }


}

