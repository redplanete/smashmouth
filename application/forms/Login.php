<?php

class Application_Form_Login extends My_Form
{
    
    public function init()
    {
        $this->setName("loginfrm");
        $this->setMethod('post');
        //$this->setAttrib('class', 'login-form');

        $this->addElement('text', 'username', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', false, array(0, 75)),
            ),
            'required'   => true,
            'placeholder' =>'',
            'label'      => 'Email Address',
        ));

        $this->addElement('password', 'password', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('StringLength', false, array(0, 50)),
            ),
            'required'   => true,
            'label'      => 'Password',
        ));
    }
    
}

