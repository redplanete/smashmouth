<?php

class Application_Form_ForgetPassword extends My_Form
{

    public function init()
    {
        $this->setName("forget_password");
        $this->setMethod('post');

        $this->addElement('text', 'email', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('StringLength', false, array(0, 75)),
            ),
            'required'   => true,
            'label'      => 'Email:',
        ));

		$forgotPass = new Zend_Form_Element_Html('element');
                $div='<div class="element flt-left"><input id="login" class="orange-btn login-btn" type="submit" value="Send" name="login"><div class="request-btn">
                        </div> </div><div class="clr"></div>';
		$forgotPass->setValue($div);
                $this->addElements(array($forgotPass));
    }
}