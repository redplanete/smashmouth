<?php

class Application_Form_ResetPassword extends My_Form
{

    public function init()
    {
        $this->setName("reset_password");
        $this->setMethod('post');
        $passwordConfirmation = new My_Validate_PasswordConfirmation();
        
        $this->addElement('password', 'login_pass', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('StringLength', false, array(6, 50))
			),
            'required'   => true,
            'label'      => 'New Password:',
        ));

		$this->addElement('password', 'password_confirm', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('StringLength', false, array(6, 50)),
				$passwordConfirmation
            ),
            'required'  => true,
			'ignore'	=> true,
            'label'     => 'Retype Password:',
        ));

		$forgotPass = new Zend_Form_Element_Html('element');
                $div='<div class="element flt-left"><input id="login" class="orange-btn login-btn" type="submit" value="Save" name="login"><div class="request-btn">
                        </div> </div><div class="clr"></div>';
		$forgotPass->setValue($div);
                $this->addElements(array($forgotPass));
    }
}
