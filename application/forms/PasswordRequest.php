<?php

class Application_Form_PasswordRequest extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    	$this->setName('password_reset_request');
    	$this->setMethod('post');
    	$this->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/authentication/passwordreset');

    	$username = new Zend_Form_Element_Text('email');
    	$username->setLabel('Email/Login:')
    		->setRequired(true);

    	$login = new Zend_Form_Element_Submit('login');
    	$login->setLabel('Reset Password');

    	$this->addElements(array($username, $login));
    }


}

