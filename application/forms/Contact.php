<?php
class Application_Form_Contact extends My_Form
{
    public function init()
    {
        $this->setName("contactfrm");
        $this->setMethod('post');
        $this->addElement('text', 'email', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', false, array(0, 75)),
                array('EmailAddress'),
            ),
            'required'   => true,
            'label'      => 'Your Email Address',
        ));
         $this->addElement('text', 'name', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('StringLength', false, array(0, 75)),
            ),
            'required'   => false,
            'label'      => 'Your Name',
        ));
        $this->addElement('textarea', 'feedback', array(
            'filters'    => array('StringTrim'),
            'required'   => true,
            'rows' =>'10',
            'cols' => '80',
            'label'      => 'Feedback',
        ));
    }
}