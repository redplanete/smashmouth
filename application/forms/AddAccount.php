<?php

class Application_Form_AddAccount extends My_Form
{

    public function init()
    {
        $this->setName("registerfrm");
        $this->setMethod('post');

        $this->addElement('text', 'first_name', array(
        		'filters'    => array('StringTrim'),
        		'validators' => array(
        				array('StringLength', false, array(0, 75)),
        		),
        		'required'   => true,
        		'label'      => 'First Name :',
        ));
        
        $this->addElement('text', 'last_name', array(
        		'filters'    => array('StringTrim'),
        		'validators' => array(
        				array('StringLength', false, array(0, 75)),
        		),
        		'required'   => true,
        		'label'      => 'Last Name :',
        ));

        $this->addElement('text', 'login_name', array(
        		'filters'    => array('StringTrim'),
        		'validators' => array(
        				array('StringLength', false, array(0, 120)),
        		),
        		'required'   => true,
        		'label' => '<span title="This name will be displayed to other users throughout the site.">Public Display Name :</span>',
        		'label_attributes' => array(
        				'escape' => false,
        		)
        ));
                
        $passwordConfirmation = new My_Validate_PasswordConfirmation();
        $this->addElement('text', 'email', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', false, array(0, 75)),
                array('EmailAddress'),
            ),
            'required'   => true,
            'label'      => 'Email Address :',
            
        ));

        $this->addElement('password', 'login_pass', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('StringLength', false, array(0, 50)),
            ),
            'required'   => true,
            'label'      => 'Password :',
        ));

        $this->addElement('password', 'confirm_password', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                $passwordConfirmation,
                array('StringLength', false, array(0, 50)),
            ),
            'required'   => true,
            'label'      => 'Confirm Password :',
        ));

        $this->addElement('submit', 'register', array(
            'required' => false,
            'ignore'   => true,
            'class' =>'orange-btn register-btn',
            'label'    => 'Submit',
        ));

        $this->addElement('checkbox', 'confirmConsent', array(
        		'required' => true,
        		'ignore'   => true,
        		'class' =>'left',
        		'style' => 'float: left; width: 30px; margin: 4px 0 4px 30px;',
        		'label' => '&nbsp;&nbsp;<span>I\'ve read and to the <a href="/terms-and-conditions">Terms and Conditions</a> and the <a href="/privacy-policy">Privacy Policy</a></span>',
        		'label_attributes' => array(
        				'escape' => false,
        		)    		
        ));
        
		$this->register->removeDecorator('label');
                 $this->email->addErrorMessage('Value is not a valid email account');
                
    }
}





















//class Application_Form_AddAccount extends Zend_Form
//{
//    public function init()
//    {
//        /* Form Elements & Other Definitions Here ... */
//
//        $email = new Zend_Form_Element_Text('email');
//        $email
//          ->setLabel('Email Address:*')
//          ->setRequired(true);
//
//        $password = new Zend_Form_Element_Password('login_pass');
//        $password
//          ->setLabel('Password:*')
//          ->setRequired(true);
//
//        $confirm = new Zend_Form_Element_Password('confirm_password');
//        $confirm
//          ->setLabel('Confirm Password:*')
//          ->setRequired(true);
//
//        $firstName = new Zend_Form_Element_Text('first_name');
//        $firstName
//        	->setLabel('First Name:*')
//        	->setRequired(true);
//
//        $lastName = new Zend_Form_Element_Text('last_name');
//        $lastName
//        	->setLabel('Last Name:*')
//          ->setRequired(true);
//        $termsConditions = new Zend_Form_Element_Checkbox('terms_conditions');
//        $termsConditions
//          ->setDescription('I agree to the <a href="#">Terms &amp; Conditions</a>.')
//          ->setRequired(true);
//        $termsConditions->getDecorator('Description')->setOption('escape', false);
//
//       $submit = new Zend_Form_Element_Submit('submit');
//       $submit->setLabel("Create Account");
//
//       $this->addDisplayGroup(array(
//			$email,
//			$password,
//			$confirm,
//			$firstName,
//        	$lastName,
//			$nickname,
//			$termsConditions,
//			$submit
//        ), 'registerGroup');
//    }
//}
//
