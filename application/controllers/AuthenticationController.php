<?php

class AuthenticationController extends My_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        // action body
    }

    public function loginAction() {
        // $logDb = new Application_Model_DbTable_Userlog();

        if(Zend_Auth::getInstance()->hasIdentity()) {
        	// if already logged in, forward to front page
            $this->redirect('/index/index');
        }

        $form = new Application_Form_Login();
        $request = $this->getRequest();
		/* @var  $request Zend_Controller_Request_Http */
        
        if($request->isPost()) {

            if($form->isValid($request->getParams())) {
            	
                $authAdapter = $this->getAuthAdapter();
                $authAdapter->setIdentity($form->getValue('username'));
                $authAdapter->setCredential($form->getValue('password'));

                $auth = Zend_Auth::getInstance();

                if($auth->authenticate($authAdapter)->isValid()) {             	
                	// update the identity to an Account object for convenience
                    $userRow = (array) $authAdapter->getResultRowObject();
                    $userDBTable = new Application_Model_DbTable_Users();
                   
                    $identityAsAccount = new Application_Model_Account($userDBTable->getUserById($userRow['id']));                    
                    $auth->getStorage()->write($identityAsAccount);

                    $this->redirect('account/index');
                } else {
                    $this->view->msg = "Invalid Email or password";
			        $aclSession = new Zend_Session_Namespace('acl');
			        $aclSession->unsetAll();
			        Zend_Auth::getInstance()->clearIdentity();
			        Zend_Session::destroy();
                }
            }
        }

        $this->view->form = $form;

    }

    public function logoutAction() {
        $aclSession = new Zend_Session_Namespace('acl');
        $aclSession->unsetAll();
        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy();
        $this->redirect('/authentication/login');
    }

    public function forgetpasswordAction() {

        $form = new Application_Form_ForgetPassword();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $user = Application_Model_DbTable_Users::getMapper()->findByEmail($formData['email']);

                if ($user) {
                    #Generate token and update user
                    $token = My_General::randomString(30);
                    $user->change_pass_key = $token . ":" . time();

                    $user->save();
                    

                    // ***************************************************************************
                    // Email user password link
                    // ***************************************************************************
                    $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
                    $site = $config->getOption("site");
                    	
                    $templateData = array(
                    	"name" 			=> $user->getName(),
                    	"reset_link" 	=> "http://{$_SERVER['SERVER_NAME']}" . "/authentication/resetpassword/email/" . $user->email . "/token/" . $token
                    );
                    	
                    $parser = new My_TemplateParser($templateData);
                    $content = $parser->parseTemplateFile(APPLICATION_PATH.'/layouts/email_templates/en/forget_password.html', $templateData);
                    	
                    $mail = new Zend_Mail();
                    $mail->setFromToDefaultFrom();
                    $mail->setSubject("New Password Request");
                    $mail->setBodyHtml($content);
                    $mail->addTo($user->email, $user->getName());
                    $result = $mail->send();
                    // ***************************************************************************                    

                    if ($result) {
                        $this->addMessage("An email has been sent to <u>{$user->email}</u> with instructions on how to change your password.");
                    } else {
                         $this->view->msg = 'There is problem for sending email';
                    }
                    
                } else {
                     $this->view->msg ='Unknown email address';
                }
            }
        }
    }    
    
     public function resetpasswordAction() {
     	
        $is_valid_link = false;

        $token = $this->getRequest()->getParam('token');
        $email = $this->getRequest()->getParam('email');
        
        #find user by token
        if ($token && $email) {
        	
            $user =  Application_Model_DbTable_Users::getMapper()->findByEmailAndKey($email, $token);

            if ($user) {
            	
            	$db_token = $user->change_pass_key;
            	$tokens = explode(':', $db_token);
            	
            	if ($token == $tokens[0]) {
            		if (time() - $tokens[1] > 60*60*24) {
            			$this->addMessage('This link has expired.  Please try again.');
            			return;
            		}
            	} else {
            		$this->addMessage('Invalid password reset link');
            		return;
            	}
            	
            	$is_valid_link = true;
            	
                $reset_form = new Application_Form_ResetPassword();
                $this->view->form = $reset_form;
                
                if ($this->getRequest()->isPost()) {
                	
                    $formData = $this->getRequest()->getPost();
                    
                    if ($reset_form->isValid($formData)) {
                        $user->login_pass = new Zend_Db_Expr("SHA1('{$formData['login_pass']}')");
                        $user->change_pass_key = '';
                        $user->save();
                        
                        $this->addMessage('Password reset successfully');
                        $this->redirect('/authentication/login');
                    }
                }
                
            }
        }

        if (!$is_valid_link) {
            $this->addMessage('Invalid password reset link');
        }
    }   
    

    private function getAuthAdapter() {
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        $authAdapter->setTableName('users')
                ->setIdentityColumn('email')
                ->setCredentialColumn('login_pass')
                ->setCredentialTreatment('SHA1(?)');

        return $authAdapter;
    }

}





