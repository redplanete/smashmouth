<?php
class TestController extends Zend_Controller_Action
{
	public function outputAction() {
		$output = "";
		$pickDb = new Application_Model_DbTable_UserPick();
		// Copy the picks from games 8 and 33 to 46 and 47.
		$sources = array(
			array("game_id" => 8, "next_game_id" => 46, "user_id" => 9),
			array("game_id" => 8, "next_game_id" => 46, "user_id" => 26),
			array("game_id" => 33, "next_game_id" => 47, "user_id" => 32),
			array("game_id" => 33, "next_game_id" => 47, "user_id" => 25)
		);
		$count = 0;
		foreach ($sources as $source) {
			$select = $pickDb->select();
			$select->where("game_id = ?", $source["game_id"]);
			$select->where("user_id = ?", $source["user_id"]);
			$records = $pickDb->fetchAll($select);
			foreach ($records as $record) {
				$matchup = Application_Model_Matchup::getMatchup(array(
					"sport_id" => 1,
					"match_id" => $record->match_id,
					"season_id" => $record->season_id
				));
				$homeTeamID = $matchup->home_team_id;
				$awayTeamID = $matchup->away_team_id;
				$matchupDb = new Application_Model_DbTable_Matchup();
				$matchupSelect = $matchupDb->select();
				$matchupSelect->where("(home_team_id = {$homeTeamID} and away_team_id = {$awayTeamID}) or (home_team_id = {$awayTeamID} and away_team_id = {$homeTeamID})");
				$matchupSelect->where("round = 4");
				$matchup = $matchupDb->fetchRow($matchupSelect);
				if ($matchup) {
					$copy = array(
						"game_id" => $source["next_game_id"],
						"user_id" => $source["user_id"],
						"sport_id" => 1,
						"match_id" => $matchup->match_id,
						"season_id" => $record->season_id,
						"pick_id" => $record->pick_id,
						"smash" => $record->smash,
						"round" => $record->round
					);
					//Application_Model_UserPick::createUserPick($copy);
				}
			}
		}
		
		$this->view->output = $output;
	}
	
	public static function Test($teamA) {
		$teamID = $teamA->team_id;
	}
}
?>