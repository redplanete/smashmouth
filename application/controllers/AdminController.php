<?php
/**
 * The Admin interface for the system
 * 
 * @author Leon McCottry
 *
 */
class AdminController extends Zend_Controller_Action
{
	/**
	 * @var Application_Model_User  the current user logged into the system
	 */
	protected $currentUser;
	
	/**
	 * @var Zend_Controller_Request_Http $request
	 *				The request that was made to the server
	 */
	protected $request;
	
	/**
	 * Sets up the current user and the request for global use.
	 * 
	 * @see Zend_Controller_Action::init()
	 */
	public function init()
    {
    	$account = Zend_Auth::getInstance()->getIdentity();
    	if ($account) {
	        $this->currentUser = $account->getUser();
	        $this->request = $this->getRequest();
    	}
    }
    
    /**
     * Makes sure that the user has logged in before allowing any account actions.
     *
     * @see Zend_Controller_Action::preDispatch()
     */
    public function preDispatch() {
    	$account = Application_Model_Account::getAccountFromIdentity();
    	if (!isset($account)) {
    		// redirect to login if there is no identity in the auth storage.
    		$this->redirect('/authentication/login');
    		return false;
    	}

    	if (!in_array($this->currentUser->user_type, array("super_admin","admin"))) {
    		// redirect to index if not an admin
    		$this->redirect('/index');
    		return false;    		
    	}
    }
    
    /**
     * Sets up the current Marquee Matchup settings.
     */
    public function settingsAction() {
    	
    	$settings_id = $this->getParam("settings_id");
    	$settingsDb = new Application_Model_DbTable_MarqueeMatchupSettings();
    	
    	if ($settings_id && ($settings_id != 0) ) {
    		//  The settings have been specified so get those
    		$this->view->settings = $settingsDb->getMapper()->find($settings_id);
    		
    		if (!$this->view->settings) {
    			throw new My_Exception_HandledException("No such settings exist.");
    		}
    		
    		$season_id = $this->view->settings->seasons_id;
    		$season = Application_Model_DbTable_Seasons::getMapper()->find($season_id);
    		$this->view->sport = Application_Model_DbTable_Sports::getMapper()->find($season->sports_id);
    		
    		if ($this->getParam("delete")) {
    			$settingsDb->delete(array("id = ?" => $this->view->settings->id));
    			$this->view->settings = null;
    			$settings_id = null;
    			$this->setParam("delete", null);
    		}
    	} else if ( empty($settings_id) ) {
    		$season_id = $this->getParam("season_id");
    		$season = Application_Model_DbTable_Seasons::getMapper()->find($season_id);
    		
    		if (!$season) {
    			throw new My_Exception_HandledException("No such season exist.");
    		}
    		
    		$this->view->sport = Application_Model_DbTable_Sports::getMapper()->find($season->sports_id);

    		if ($settings_id === "0") {
    			// This is a request to make a new setting
				$this->getNewSettings($season_id);
				$doRedirect = true;
    		}
    	}

    	$this->view->allSettings = $settingsDb->getAllSettingsBySeason($season_id);
    	
		//////////////////////////////////////////////////////////
		// Get the current, non-expired matchup
		//////////////////////////////////////////////////////////
		$select = $settingsDb->select()
			->where("seasons_id = ?", $season_id)
			->where("'".date("Y-m-d")."' <= smashup_close_date")
			->order("id ASC")
			->limit(1);

		$row = $settingsDb->fetchRow($select);
		if ($row) $this->view->currentSettings = $settingsDb->getMapper()->getModel($row->toArray());
		/////////////////////////////////////////////////////////
		
 		if (is_null($settings_id)) {
 			if (!$this->view->currentSettings) {
 				// no non-expired settings exist, so create a new set
 				$this->getNewSettings($season_id);
 			} else {
				// Get the current settings
				$this->view->settings = $this->view->currentSettings;
 			}
 			$doRedirect = true;
		}
		
		$this->view->allSettings = $settingsDb->getAllSettingsBySeason($season_id);
	
		if ($this->request->isPost()) {
			$marqueeMatchupsDb = new Application_Model_DbTable_MarqueeMatchups();
			
			if ($this->request->getPost("form_name") == "Update Dates" ) {
				$this->view->settings->smashup_open_date = $this->request->getParam("smashup_open_date");
				$this->view->settings->smashup_close_date = $this->request->getParam("smashup_close_date");
				$this->view->settings->save();
				$marqueeMatchupsDb->narrowMarqueeMatchups($this->view->settings->id, $this->view->settings->smashup_open_date, $this->view->settings->smashup_close_date);
			} else if ($this->request->getPost("form_name") == "Update Marquee Matchups" ) {

				$marqueeMatchupsDb->clearMarqueeMatchups($this->view->settings->id);
				$games_ids = $this->request->getParam("games_id") ? $this->request->getParam("games_id") : array();
				
				foreach($this->request->getParam("games_id") as $games_id ) {
					$marqueeMatchup = new Application_Model_MarqueeMatchups();
					
					$marqueeMatchup->games_id = $games_id;
					$marqueeMatchup->marquee_matchup_settings_id = $this->view->settings->id;
					
					$marqueeMatchup->save();
				}
								
				$positions = array();
				foreach($this->request->getParams() as $key => $value) {
					if (strpos($key, "position-") === 0) {
						$position = str_replace("position-", "", $key);
						$temp = array();
						for($i = 0; $i < $value; $i++) {
							$temp[$i] = null;
						}
						$positions[$position] = $temp;
					}
				}
				
				$jsonPositions = json_encode($positions);
				$this->view->settings->positions = stripslashes($jsonPositions);
				$this->view->settings->number_of_matchups = $this->request->getParam("number_of_matchups");
				
				$this->view->settings->save();
			}
		}

		$gamesDb = new Application_Model_DbTable_Games();
		$this->view->games = $gamesDb->getGamesByDateRange($this->view->settings->seasons_id, $this->view->settings->smashup_open_date, $this->view->settings->smashup_close_date);	
		
		if ($doRedirect) $this->redirect("/admin/settings/settings_id/".$this->view->settings->id);
    }
    
    /**
     * Adds new setting parameters to the global view variables;
     * 
     * @param int $season_id    The seasons_id of the setting to be created
     */
    private function getNewSettings($season_id) {
    	$this->view->settings = new Application_Model_MarqueeMatchupSettings();
    	 
    	$this->view->settings->seasons_id = $season_id;
    	 
    	// default to tomorrow
    	$this->view->settings->smashup_open_date = date("Y-m-d", time()+60*60*24);
    	$this->view->settings->smashup_close_date = date("Y-m-d", time()+60*60*24);

    	$this->view->settings->positions = $this->view->sport->default_positions;
    	$this->view->settings->score_weights = $this->view->sport->default_score_weights;
    	$this->view->settings->number_of_matchups = count(json_decode($this->view->settings->positions,true), COUNT_RECURSIVE) - count(json_decode($this->view->settings->positions,true));
    	$this->setParam("settings_id",$this->view->settings->save());
    }
}

?>