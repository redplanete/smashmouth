<?php
/**
 * 
 * @author Leon McCottry
 *
 */
class MessageController extends Zend_Controller_Action
{
	/**
	 * @var Application_Model_User  the current user logged into the system
	 */
	protected $currentUser;
	
	/**
	 * @var Zend_Controller_Request_Http $request
	 *				The request that was made to the server
	 */
	protected $request;
	
	/**
	 * Sets up the current user and the request for global use.
	 * 
	 * @see Zend_Controller_Action::init()
	 */
	public function init()
    {
    	$this->request = $this->getRequest();
    	
    	$account = Zend_Auth::getInstance()->getIdentity();
    	if ($account) {
	        $this->currentUser = $account->getUser();
    	}
    }

    /**
     * Makes sure that the user has logged in before allowing any account actions.
     *
     * @see Zend_Controller_Action::preDispatch()
     */
    public function preDispatch() {
    	$account = Application_Model_Account::getAccountFromIdentity();
    	if (!isset($account)) {
    		
    		// if it is for events, we should cancel it and all future request until login;
    		if ($this->request->getActionName() == 'getevents') {
    			$response = array(
    					'success' => true,
    					'instructions' => array(array('actions' => "doGetEvents = false;"))
    			);
    			
    			$this->_helper->json($response);
    			exit; 			
    		}
    		
    		// otherwise, redirect to login if there is no identity in the auth storage.
    		$this->redirect('/authentication/login');
    		return false;
    	}
    }
    
    /**
     * Sends a message to a user by e-mail and stores it in the database.
     */
    public function sendmessageAction() {
    	$to_user = Application_Model_DbTable_Users::getMapper()->find($this->request->getParam("to_users_id"));
    	
    	if (!$to_user)
    		throw new My_Exception_HandledException("Unable to find user.");
    	
    	// verify if the users are friends
    	$usersFriendsDb = new Application_Model_DbTable_UsersFriends();
    	$usersFriends = $usersFriendsDb->haveRelationship($this->currentUser, $to_user);
    	
    	if (!$usersFriends)
    		throw new My_Exception_HandledException("You are only able to send messages to your friends.");
    	
    	$email = $to_user->email;
    	$message = $this->request->getPost("message");
    	
    	// gather email data from the application.ini file
    	$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
    	$site = $config->getOption("site");
    		
    	$templateData = array(
    			"to_user_name" 		=> $to_user->getName(),
    			"from_user_name" 	=> $this->currentUser->getName(),
    			"site_url"			=> "http://".$site["url"],
    			"message"			=> $message
    	);
    	
    	$parser = new My_TemplateParser($templateData);
    	$content = $parser->parseTemplateFile(APPLICATION_PATH."/layouts/email_templates/en/user_message.html", $templateData);
    	
    	$mail = new Zend_Mail();
    	$mail->setFromToDefaultFrom();
    	$mail->setSubject($this->currentUser->getName()." sent you a message on SmashMouth Fantasy Sports");
    	$mail->setBodyHtml($content);
    	$mail->addTo($email, $to_user->getName());
    	$result = $mail->send();

    	$messageData = array(
    			"from_users_id" => $this->currentUser->id,
    			"to_users_id" => $to_user->id,
    			"message" => $message,
    	);
    	
    	$success = Application_Model_DbTable_UsersMessages::getMapper()->getModel($messageData)->save();
    	
    	if ($success) {
    		$response = array( "success" => true, "user_message" => "Successfully sent an email to ".$to_user->getName());
    		$this->_helper->json($response);
    	} else {
    		throw new My_Exception_HandledException("This service is currently unavailable.  Please try again, later.");
    	}
    }
    
    /**
     * The action for long-polling to get notification of events
     */
    public function geteventsAction() {
    	set_time_limit(0);
    	$requestId = uniqid("request[{$_SERVER['REMOTE_ADDR']}]".$this->getRequest()->getParam("guid")."--");
    	$eventsFound = false;
    	
    	$time = 0;
    	do {
    		$eventsDb = new Application_Model_DbTable_Events();
    		
    		$newEvents = $eventsDb->getNewEvents($this->currentUser->id, $this->request->getCookie("cookie_stamp"));
    		
    		if (empty($newEvents)) {
	    		sleep(20);
	    		if ($time++ >= 30) {
	    			$this->_helper->json(array("success" => true));
	    			exit;
	    		}
    		} else {
    			$eventsFound = true;
    			$cookie_stamp = uniqid("events_");
    			setcookie("cookie_stamp", uniqid("events_"), null, "/");	
    			$allInstructions = array();
    			$i = 0;
    			$log = Zend_Registry::get("log");
    			foreach($newEvents as $eventType => $eventsInfo) {

    				$instructions = is_null($eventsInfo[0]["eventType"]->instructions) ? null : substr_replace(substr_replace(trim($eventsInfo[0]["eventType"]->instructions), "", -1, 1), "", 0, 1);
    				$templateData = $eventsInfo[0]["eventsData"];
    				
					$templateData["count"] = count($eventsInfo);
					
					$tooltip = array();
					$log->info("************************************************************************************");
					foreach (getallheaders() as $name => $value) {
						$log->info( "$name: $value" );
					}
					$log->info("------------------------------------------------------------------------------------");
						
					foreach($eventsInfo as &$eventInfo) {
						setcookie("cookie_stamp_old".$i++, $eventInfo["event"]->cookie_stamp, null, "/");
						$log->info($requestId.": Changing event {$eventInfo["event"]->id} from {$eventInfo["event"]->cookie_stamp} to ".$cookie_stamp);
						$eventInfo["event"]->cookie_stamp = $cookie_stamp;
						$eventInfo["event"]->save();
						$parserTooltip = new My_TemplateParser($templateData);
						if (!is_null($eventInfo["eventType"]->notification_message)) {
							$tooltips[] = $parserTooltip->parseTemplateData($eventInfo["eventType"]->notification_message);
						}
					}
					$log->info("************************************************************************************\n\n\n");
						
					$templateData["notification_messages"] = '"'.implode("&#10;", $tooltips).'"';
					
					$parserInstructions = new My_TemplateParser($templateData);
					
    				if (!empty($instructions)) {
						$allInstructions[] =$parserInstructions->parseTemplateData($instructions);
					}
    			}
    			
    			$response = "{ \"success\": true, \"instructions\": ";
    			if (empty($allInstructions)) {
    				$response .= "null }";
    			} else {
    				$response .= "[".implode(",", $allInstructions)."] }";  				
    			}
    			
    			$this->_helper->layout()->disableLayout(); 
        		$this->_helper->viewRenderer->setNoRender(true);
        		
    			header('Content-type: application/json');
    			echo $response;
    			exit;
    		}
    	} while (!$eventsFound);
    }

}

?>