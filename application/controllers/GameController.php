<?php

/**
 * This is the engine for the NBA standard smashup
 * 
 * @author Leon McCottry
 *
 */
class GameController extends Zend_Controller_Action
{
	/**
	 * @var Application_Model_User  the current user logged into the system
	 */
	protected $currentUser;
	
	/**
	 * @var Zend_Controller_Request_Http $request
	 *				The request that was made to the server
	 */
	protected $request;
	
	/**
	 * Sets up the current user and the request for global use.
	 * 
	 * @see Zend_Controller_Action::init()
	 */
	public function init()
    {
    	$account = Zend_Auth::getInstance()->getIdentity();
    	if ($account) {
	        $this->currentUser = $account->getUser();
	        $this->request = $this->getRequest();
    	}
    }
    
    /**
     * Makes sure that the user has logged in before allowing any account actions.
     *
     * @see Zend_Controller_Action::preDispatch()
     */
    public function preDispatch() {
    	$account = Application_Model_Account::getAccountFromIdentity();
    	if (!isset($account) && $this->getRequest()->getActionName() != "scoreboard") {
    		// redirect to login if there is no identity in the auth storage.
    		$this->redirect('/authentication/login');
    		return false;
    	}
    }
    
    /**
     * Fowards to the smashup Action
     */
    public function indexAction() {
    	$this->_forward('smashup');
    }
    
    /**
     *  Gets the scoreboard for a the current games
     */
    public function scoreboardAction() {
    	$this->_helper->layout()->disableLayout();
    	$this->view->games = array();
    	$seasons_id = $this->getRequest()->getParam("seasons_id");
    	
    	if ($seasons_id) {
    		$this->view->season = Application_Model_DbTable_Seasons::getMapper()->find($seasons_id);
    		if (!$this->view->season) return;
    		
    		$this->view->sport = Application_Model_DbTable_Sports::getMapper()->find($this->view->season->sports_id);
    		
	    	$gamesDb = new Application_Model_DbTable_Games(); 
	    	$settingsDb = new Application_Model_DbTable_MarqueeMatchupSettings();
	    	
	    	$all_settings = $settingsDb->getAllSettingsBySeason($seasons_id);
	    	$current_settings = $settingsDb->getCurrentMarqueeMatchupSettings($seasons_id);
	    	$previous_settings = null;
	    	$this->view->next_setting = null;
	    	
	    	$now = date("Y-m-d");
	    	$breakOnNext = false;
	    	foreach ($all_settings as $setting) {
	    		if ($breakOnNext) {
	    			$this->view->next_setting = $setting; 
	    			break;
          }
//          echo "\n\n***".$current_settings->id."\n";
//          echo $setting->id." ***\n\n\n";

	    		if ($current_settings->id != $setting->id) {
	    			$previous_settings = $setting;
	    		} else {
	    			$breakOnNext = true;
	    		}
	    	}
	    	
	    	$settings = array();
	    	if ($current_settings) $settings[] = $current_settings;
	    	if ($previous_settings) $settings[] = $previous_settings;
	    	
	    	$this->view->games = $gamesDb->getGamesByDateRange($seasons_id, date("Y-m-d", time()-(60*60*12)), date("Y-m-d", time()), $returnMarqueeOnly = true, $settings);
	    	$this->view->current_settings = $current_settings;   	
    	}
    }
    
    /**
     * Gets the smashup boards which displays who's turn it is, the picks that have been made,
     * and the current score or results of a game being played.
     */
    public function smashupAction()
    {    	
        $request = $this->request;
        $log = Zend_Registry::get('log');

        $this->view->matchup = $matchup = Application_Model_DbTable_UsersMatchups::getMapper()->find($request->getParam('matchup'));

        if (!$matchup || !in_array($this->currentUser->id, array($matchup->home_user_id, $matchup->away_user_id) )) {
        	// unauthorized access.  Return to the dashboard.
            $this->redirect("/dashboard");
            return;
        }

        $matchSettingsDb = new Application_Model_DbTable_MarqueeMatchupSettings();
        $this->view->settings = $matchupSettings = $matchSettingsDb->getMapper()->find($matchup->marquee_matchup_settings_id);
        
        $startDate = date('m/d/Y',strtotime($matchupSettings->smashup_open_date));
        $endDate = date('m/d/Y',strtotime($matchupSettings->smashup_close_date));
               
        $this->view->isCurrentUserHomeUser = ($this->currentUser->id == $matchup->home_user_id);
        
        if (!$this->view->isCurrentUserHomeUser) {
        	$this->view->home_user = $home_user = Application_Model_DbTable_Users::getMapper()->find($matchup->home_user_id);
        	$this->view->away_user = $away_user = $this->currentUser;
        } else {
        	$this->view->away_user = $away_user = Application_Model_DbTable_Users::getMapper()->find($matchup->away_user_id);
        	$this->view->home_user = $home_user = $this->currentUser;
        }
        
        $smashupsData =  array(
            "matchup" => $matchup,
        	$matchup->home_user_id => array("smashups"=>array(), "players"=>Application_Model_DbTable_MarqueeMatchupSettings::getSmashupArray($matchupSettings->id)),
        	$matchup->away_user_id => array("smashups"=>array(), "players"=>Application_Model_DbTable_MarqueeMatchupSettings::getSmashupArray($matchupSettings->id))
        );
     
        $positionsMap = Application_Model_DbTable_MarqueeMatchupSettings::getSmashupArray($matchupSettings->id);

        $this->view->positionsMap = array();
        foreach($positionsMap as $key => $positionArray ) {
        	if ($key == "team_ids") continue;
        	
        	$mapIndex = 0;
        	foreach($positionArray as $temp) {
        		$this->view->positionsMap[] = "$key:".$mapIndex++;
        	}
        }
   
        $smashupsDb = new Application_Model_DbTable_UsersSmashups();
        $smashupsDb->getSmashupsWithPlayers($smashupsData);

        $this->view->smashupsData = $smashupsData;
    
        // determine status and who's pick it is if match has not started, then set messages accordingly
        //****************************************************************************************************        
        if ($matchup->status == "completed") {
        	$this->view->winner = ($matchup->home_team_score > $matchup->away_team_score) ? $home_user : $away_user;
        	
        	if ($this->currentUser->id == $this->view->winner->id) {
        		$this->view->user_messages = array("Congratulations. You Won!", "Continue competing to increase your international ranking!");
        	} else {
        		$this->view->user_messages = array("Sorry. {$this->view->winner->getName()} won.", "Try again next week and increase your international ranking!");
        	}
        	
        } else if ( $matchup->status == "in_progress" ) {
        	$this->view->user_messages = array("This matchup is currently in progress.", "Scores will update live, in real-time!");
        } else if ( $matchup->status == "accepted" ) {
        	$homeCount = count($smashupsData[$matchup->home_user_id]["smashups"]);
        	$awayCount = count($smashupsData[$matchup->away_user_id]["smashups"]);
        	
        	$this->view->pickNumber = $homeCount + $awayCount + 1;
        	$this->determineWhoPicksWithMessages($home_user, $homeCount, $away_user, $awayCount);
        	
        	// ---------------------------------------------------
        	// if forced pick, get forced position
        	// ---------------------------------------------------
        	if (!$this->view->nextPick["is_smash_pick"] ) {
        		$last_smashup = $smashupsDb->getLastSmashup($matchup->id);
        		$last_player = Application_Model_DbTable_Players::getMapper()->find($last_smashup->players_id);
        	    $this->view->forced_position = $last_player->position;        	
        	}
        	
        	$this->view->user_messages[] = "SmashUps are open from {$startDate} to {$endDate}";
        }
	    //****************************************************************************************************

        // always reject picks that are out of turn.
        // pick logic is as follow
              // first pick always goes to the home user
              // if the last pick was odd, then it is the next persons turn
              // otherwise it is the same user
              // even picks must always pick from the same game same position, and opposite team as last pick
              // once a team is picked from, that team and all other players from that team can no longer be picked.
              // the final pick is pick 16
              
    }    
    
    /**
     *  Gets the current smashup pick number.  It is used to determine when to refresh.
     *  
     *  Note: In the future, the client will send it's current pick number and if it 
     *  differs from the server, the data to construct new picks will be sent.
     *  Called by XHR.
     */
    public function smashupnumberAction() {
    	$userSmashupDb = new Application_Model_DbTable_UsersSmashups();
    	
    	$response = array(
    		'success' => true,
    		'pickNumber' => $userSmashupDb->getSmashupCount($this->getParam('matchup')) + 1
    	);
    	
    	$this->_helper->json($response);
    }
    
    /**
     * Challenges a friend to a matchup
     * 
     * @throws My_Exception_HandledException  Thrown when the request is made outside of allowed
     * 										  workflow.
     */
    public function challengeAction() {
        $request = $this->getRequest();
        $log = Zend_Registry::get('log');
        $currentUser = Application_Model_Account::getAccountFromIdentity()->getUser();

        $challengeInitiator = $currentUser;
        $challengedUser = Application_Model_DbTable_Users::getMapper()->find($request->getParam("challengedUser"));
		$sport = Application_Model_DbTable_Sports::getMapper()->find($request->getParam("sportsId"));
        $matchupsDb = new Application_Model_DbTable_UsersMatchups();
        
		$marqueeMatchupSettings = null;
		
        // handle all known exceptional states
        if (!$challengedUser) {
        	throw new My_Exception_HandledException("Unable to find this user.");
        } else if (!$sport || ($sport->active != 1) ) {
        	throw new My_Exception_HandledException("This sport is currently not available.");
        } else {

                $marqueematchupsDb = new Application_Model_DbTable_MarqueeMatchups();
                $pendinggamecount = $marqueematchupsDb->getMarqueeMatchupPendingGameCount();
                $inprogresscount = $marqueematchupsDb->getMarqueeMatchupInProgressGameCount();
                
                if ($inprogresscount >= 1) {
                        throw new My_Exception_HandledException("Marquee Matchups are now in progress.  Please try again later!");
                }
                
                
                if ($pendinggamecount <= 0) {
                        throw new My_Exception_HandledException("There are no Marquee Matchups at this time. Please try again later!");
                }
                
            
        	$marqueeMatchupSettingsDb = new Application_Model_DbTable_MarqueeMatchupSettings();
        	$marqueeMatchupSettings = $marqueeMatchupSettingsDb->getCurrentMarqueeMatchupSettings($sport->current_season);
        	
                
                
        	if (!$marqueeMatchupSettings) {
        		throw new My_Exception_HandledException("There are no Marquee Matchups at this time. Please try again later!");
        	}

                
        	$usersFriendsDb = new Application_Model_DbTable_UsersFriends();
        	$relationship = $usersFriendsDb->haveRelationship($challengeInitiator,$challengedUser, array());
        	
        	if (!$relationship || !in_array($relationship->status, array('accepted','blocked','requested')) ) {
        		// a friendship must be initiated first
        		throw new My_Exception_HandledException("You must first initiate a friendship request.");
        	} else if ($relationship->status == 'requested') {
        		throw new My_Exception_HandledException("The user must first accept your friendship request.");
        	} else if ($relationship->status == 'blocked') {
        		throw new My_Exception_HandledException("Unable to find this user.");
        	} else {
        		$matchups = $matchupsDb->getExistingMatchups($challengeInitiator, $challengedUser, array($sport->id));
        		if (count($matchups)) {
        			throw new My_Exception_HandledException("You already have an active ".$sport->league." matchup with ".$challengedUser->getName());
        		}
        	}
        }

        $data =  array(
			"challenge_initiator" => $currentUser->id,
			"type" => "individual",
			"home_user_id" => $currentUser->id,
			"away_user_id" => $challengedUser->id,
			"sports_id" => $sport->id,
			"seasons_id" => $sport->current_season,
			"status" => "requested",
			"marquee_matchup_settings_id" => $marqueeMatchupSettings->id
		);
        
        $matchupId = $matchupsDb->getMapper()->getModel($data)->save();

        // ***************************************************************************
        // Email user that they have been challenged to a matchup
        // ***************************************************************************
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $site = $config->getOption("site");
        
        $templateData = array(
        		"to_user_name" 					=> $challengedUser->getName(),
        		"from_user_name" 				=> $currentUser->getName(),
        		"site_url"						=> "http://".$site["url"],
        		"league"						=> $sport->league
        );
        
        $parser = new My_TemplateParser($templateData);
        $content = $parser->parseTemplateFile(APPLICATION_PATH."/layouts/email_templates/en/challenge.html");
        
        $mail = new Zend_Mail();
        $mail->setFromToDefaultFrom();
        $mail->setSubject($currentUser->getName()." has invited you to an {$sport->league} Matchup!");
        $mail->setBodyHtml($content);
        $mail->addTo($challengedUser->email, $challengedUser->getName());
        $result = $mail->send();
        // ***************************************************************************
        
        if ($matchupId) {
        	// send back all current invites
        	$invites = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("requested"), array("initiator"));        	
        	$invitedUsers = array();
        	 
        	foreach($invites as $invite) {
        		$invitedUser = Application_Model_DbTable_Users::getMapper()->find($invite->away_user_id);
        		$invitedUsers[] = array('id' => $invitedUser->id, 'name' => $invitedUser->getName());
        	}
        	$this->_helper->json( array(
        		"success" => true,
        		"name" => $challengedUser->getName(),
        		"invitedUsers" => $invitedUsers,
        		"user_message" => $challengedUser->getName()." has been sent a Matchup Invitation!"
        	));
        } else {
			throw new My_Exception_HandledException("Service currently unavailable.  Please try again later.");
        }
    }

    /**
     * Player responds to challenges made by friend
     * 
     * @throws My_Exception_HandledException   thrown when the a response is made that doesn't
     * 										   fit within the security model.
     */
    public function respondtochallengeAction() {
        $request = $this->getRequest();

        $matchup_id = $request->getParam('matchup');
        $new_status = $request->getParam('status');
               
        $matchup = Application_Model_DbTable_UsersMatchups::getMapper()->find($matchup_id);
        
		if (!$matchup) {
			throw new My_Exception_HandledException("Could not find this invitation in the system.");
		}
		
		$matchup->status = $new_status;
		$success = $matchup->save();
		
		if ($success) {
			
			$user_message = "";
			$sport = Application_Model_DbTable_Sports::getMapper()->find($matchup->sports_id);
			$currentUser = Zend_Auth::getInstance()->getIdentity()->getUser();
			$opponent_id = ($matchup->home_user_id == $currentUser->id) ? $matchup->away_user_id : $matchup->home_user_id;
			$opponent = Application_Model_DbTable_Users::getMapper()->find($opponent_id);
			
			switch ($new_status) {
				case "accepted":
					// ***************************************************************************
					// Email opponent that the smashup has begun
					// ***************************************************************************
					$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
					$site = $config->getOption("site");
					
					$templateData = array(
							"to_user_name" 					=> $opponent->getName(),
							"from_user_name" 				=> $currentUser->getName(),
							"site_url"						=> "http://".$site["url"],
							"matchup_id"					=> $matchup->id,
							"league"						=> $sport->league
					);
					
					$parser = new My_TemplateParser($templateData);
					$content = $parser->parseTemplateFile(APPLICATION_PATH."/layouts/email_templates/en/challenge_accepted.html", $templateData);
					
					$mail = new Zend_Mail();
					$mail->setFromToDefaultFrom();
					$mail->setSubject($currentUser->getName()." has Accepted your {$sport->league} Challenge!");
					$mail->setBodyHtml($content);
					$mail->addTo($opponent->email, $opponent->getName());
					$result = $mail->send();
					// ***************************************************************************
					$user_message = "You have accepted  ".$opponent->getName()."'s ".$sport->league." matchup invitation!";
					break;
				case "declined":
					$user_message = "You have declined  ".$opponent->getName()."'s ".$sport->league." matchup invitation.";
					break;
				case "cancelled":
					$user_message = "You have cancelled your ".$sport->league." matchup with ".$opponent->getName().",";
					break;
			}
			
			$response = array();
			$response['success'] = true;
			$response['user_message'] = $user_message;
			$response['receivedInvites'] = array();
			$response['upcomingSmashups'] = array();
			
			$receivedInvites = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("requested"), array("recipient"));
			$upcomingSmashups = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("accepted"));
			
			foreach($receivedInvites as $invite) {
				$response['receivedInvites'][] = 
					array(
						"name" => Application_Model_DbTable_Users::getMapper()->find($invite->challenge_initiator)->getName(),
						"matchup_id" => $invite->id
					)
				;			
			}
			
			foreach($upcomingSmashups as $smashup) {
				$opponent_id = ($smashup->home_user_id == $currentUser->id) ? $smashup->away_user_id : $smashup->home_user_id;
				$opponent = Application_Model_DbTable_Users::getMapper()->find($opponent_id);
				
				$response['upcomingSmashups'][] =
				array(
						"name" => $opponent->getName(),
						"matchup_id" => $smashup->id
				)
				;
			}	

			$this->_helper->json($response);
		} else {
			throw new My_Exception_HandledException("This service is temporarily unavailable.  Please try again later.");
		}
    }
    
    /**
     *  Simply gets the smashboard of Marquee Matchups without the ability to pick
     */
    public function marqueematchupsAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$this->view->settings = $matchupSettings = Application_Model_DbTable_MarqueeMatchupSettings::getMapper()->find($this->getRequest()->getParam("settings_id"));
    	 
    	if (!$matchupSettings) {
    		throw new My_Exception_HandledException("There are no matching marquee matchups.  Please check your request and try again.");
    	}
    	
    	$this->view->seasons_id = $matchupSettings->seasons_id;
    	
    	$this->view->matchup = false;
    	$this->view->currentUser = $this->currentUser; 

    	// Get the marquee matchups
    	$gamesDb = new Application_Model_DbTable_Games();
    	$marqueeMatchups = $gamesDb->getMarqueeMatchups($matchupSettings->id);
    	$player_id_map = array_pop($marqueeMatchups);
    	$this->view->marqueeMatchups = $marqueeMatchups;
    	
    	$this->render("smashboard");
    	
    }
    
    /**
     * Delivers the teams and players where the user is allowed to make a SmashPick
     */
	public function smashboardAction() {
		$this->_helper->layout->disableLayout();
				
		$this->view->matchup = $matchup = Application_Model_DbTable_UsersMatchups::getMapper()->find($this->request->getParam('matchup'));
		$this->view->currentUser = $this->currentUser;
		
		if (!$matchup || !in_array($this->currentUser->id, array($matchup->home_user_id, $matchup->away_user_id) )) {
			// unauthorized access.  Return to the dashboard.
			$this->redirect("/dashboard");
			return;
		}

		// *************************************************************************
		// make sure the matchup is active for picking
		// *************************************************************************
		$matchSettingsDb = new Application_Model_DbTable_MarqueeMatchupSettings();
		$this->view->settings = $matchupSettings = $matchSettingsDb->getCurrentMarqueeMatchupSettings($matchup->seasons_id);
		
		$startDate = strtotime($matchupSettings->smashup_open_date);
		$endDate = strtotime($matchupSettings->smashup_close_date);
		$now = strtotime(date("Y/m/d"));
		
		if ( ($matchup->marquee_matchup_settings_id != $matchupSettings->id) ||
				$now < $startDate || $now > $endDate
		) {
			throw new My_Exception_HandledException("SmashUps are currently closed for this matchup.");
		}
		// *************************************************************************
		
		$this->view->isCurrentUserHomeUser = ($this->currentUser->id == $matchup->home_user_id);

		if (!$this->view->isCurrentUserHomeUser) {
			$this->view->opponent = $this->view->home_user = $home_user = Application_Model_DbTable_Users::getMapper()->find($matchup->home_user_id);
			$this->view->away_user = $away_user = $this->currentUser;
		} else {
			$this->view->opponent = $this->view->away_user = $away_user = Application_Model_DbTable_Users::getMapper()->find($matchup->away_user_id);
			$this->view->home_user = $home_user = $this->currentUser;
		}
		
		$smashupsData =  array(
			"matchup" => $matchup,
			$matchup->home_user_id => array("smashups"=>array(), "players"=>Application_Model_DbTable_MarqueeMatchupSettings::getSmashupArray($matchupSettings->id)),
			$matchup->away_user_id => array("smashups"=>array(), "players"=>Application_Model_DbTable_MarqueeMatchupSettings::getSmashupArray($matchupSettings->id))
		);
		
		$smashupsDb = new Application_Model_DbTable_UsersSmashups();
		$smashupsDb->getSmashupsWithPlayers($smashupsData);
		
		$this->view->smashupsData = $smashupsData;
		
		// *************************************************************************
		// make sure it is the this user's turn
		// *************************************************************************
		// determine who's pick it is
		$homeCount = count($smashupsData[$matchup->home_user_id]["smashups"]);
		$awayCount = count($smashupsData[$matchup->away_user_id]["smashups"]);
		
		$this->determineWhoPicksWithMessages($home_user, $homeCount, $away_user, $awayCount);

		// Get the marquee matchups
		$gamesDb = new Application_Model_DbTable_Games();
		$marqueeMatchups = $gamesDb->getMarqueeMatchups($matchupSettings->id);
		$player_id_map = array_pop($marqueeMatchups);
		$this->view->marqueeMatchups = $marqueeMatchups;

		// ---------------------------------------------------
		// if forced pick, get forced team and forced position
		// ---------------------------------------------------
		if (!$this->view->nextPick["is_smash_pick"] ) {
			$last_smashup = $smashupsDb->getLastSmashup($matchup->id);
			$last_game = $gamesDb->getMapper()->find($last_smashup->games_id);
			$last_player = Application_Model_DbTable_Players::getMapper()->find($last_smashup->players_id);
			$forced_team_id = ($last_player->teams_id == $last_game->home_team_id) ? $last_game->away_team_id : $last_game->home_team_id;
		
			$this->view->forced_team = Application_Model_DbTable_Teams::getMapper()->find($forced_team_id);
			$this->view->forced_position = $last_player->position;
				
		}
				
		// ----------------------------------------------
		// get blocked positions
		// ----------------------------------------------
		$this->view->pgBlocked = (!key_exists("PG", $smashupsData[$this->currentUser->id]["players"])
				|| !is_array($smashupsData[$this->currentUser->id]["players"]["PG"])
				|| empty($smashupsData[$this->currentUser->id]["players"]["PG"])
				|| ($smashupsData[$this->currentUser->id]["players"]["PG"][count($smashupsData[$this->currentUser->id]["players"]["PG"])-1] !== null));
		
		$this->view->sgBlocked = (!key_exists("SG", $smashupsData[$this->currentUser->id]["players"])
				|| !is_array($smashupsData[$this->currentUser->id]["players"]["SG"])
				|| empty($smashupsData[$this->currentUser->id]["players"]["SG"])
				|| ($smashupsData[$this->currentUser->id]["players"]["SG"][count($smashupsData[$this->currentUser->id]["players"]["SG"])-1] !== null));
		
		$this->view->fcBlocked = (!key_exists("F/C", $smashupsData[$this->currentUser->id]["players"])
				|| !is_array($smashupsData[$this->currentUser->id]["players"]["F/C"])
				|| empty($smashupsData[$this->currentUser->id]["players"]["F/C"])
				|| ($smashupsData[$this->currentUser->id]["players"]["F/C"][count($smashupsData[$this->currentUser->id]["players"]["F/C"])-1] !== null));
		
	}

	
	public function smashuppickAction() {
		
		$player_id = $this->request->getParam("player");
		
		$this->view->matchup = $matchup = Application_Model_DbTable_UsersMatchups::getMapper()->find($this->request->getParam('matchup'));
		
		if (!$matchup || !in_array($this->currentUser->id, array($matchup->home_user_id, $matchup->away_user_id) )) {
			// unauthorized access.  Return to the dashboard.
			$this->redirect("/dashboard");
			return;
		}

		// *************************************************************************
		// make sure the matchup is active for picking
		// *************************************************************************
		$matchSettingsDb = new Application_Model_DbTable_MarqueeMatchupSettings();
		$this->view->settings = $matchupSettings = $matchSettingsDb->getCurrentMarqueeMatchupSettings($matchup->seasons_id);
		
		$startDate = strtotime($matchupSettings->smashup_open_date);
		$endDate = strtotime($matchupSettings->smashup_close_date);
		$now = strtotime(date("Y/m/d"));
		
		if ( false /*($matchup->marquee_matchup_settings_id != $matchupSettings->id) ||
			 $now < $startDate || $now > $endDate*/
		) {
			throw new My_Exception_HandledException("SmashUps are currently closed for this matchup.");
		}  
		// *************************************************************************
		
		
		$this->view->isCurrentUserHomeUser = ($this->currentUser->id == $matchup->home_user_id);
		
		if (!$this->view->isCurrentUserHomeUser) {
			$opponent = $this->view->home_user = $home_user = Application_Model_DbTable_Users::getMapper()->find($matchup->home_user_id);
			$this->view->away_user = $away_user = $this->currentUser;
		} else {
			$opponent = $this->view->away_user = $away_user = Application_Model_DbTable_Users::getMapper()->find($matchup->away_user_id);
			$this->view->home_user = $home_user = $this->currentUser;
		}
				
		$smashupsData =  array(
			"matchup" => $matchup,
			$matchup->home_user_id => array("smashups"=>array(), "players"=>Application_Model_DbTable_MarqueeMatchupSettings::getSmashupArray($matchupSettings->id)),
			$matchup->away_user_id => array("smashups"=>array(), "players"=>Application_Model_DbTable_MarqueeMatchupSettings::getSmashupArray($matchupSettings->id))
		);
		
		$smashupsDb = new Application_Model_DbTable_UsersSmashups();
		$smashupsDb->getSmashupsWithPlayers($smashupsData);
		
		$this->view->smashupsData = $smashupsData;		
		
		// *************************************************************************
		// make sure it is the this user's turn
		// *************************************************************************
		// determine who's pick it is 
		$homeCount = count($smashupsData[$matchup->home_user_id]["smashups"]);
		$awayCount = count($smashupsData[$matchup->away_user_id]["smashups"]);
		
		$this->determineWhoPicksWithMessages($home_user, $homeCount, $away_user, $awayCount);
	
		
		if ($this->view->nextPick["is_home_user"] === null)  {
			throw new My_Exception_HandledException("All Smash Picks have been completed.");
		}
		if (($this->view->isCurrentUserHomeUser && !$this->view->nextPick["is_home_user"])
			|| (!$this->view->isCurrentUserHomeUser && $this->view->nextPick["is_home_user"])){
			throw new My_Exception_HandledException("It is not your turn.");
		}
		// *************************************************************************

		$gamesDb = new Application_Model_DbTable_Games();
		$marqueeMatchups = $gamesDb->getMarqueeMatchups($matchupSettings->id);
		$player_id_map = array_pop($marqueeMatchups);
		$this->view->marqueeMatchups = $marqueeMatchups;
		
		// *************************************************************************
		// make sure the player is part of a marquee matchup
		// *************************************************************************
		if (!array_key_exists($player_id, $player_id_map))
			throw new My_Exception_HandledException("This player is not available for a the SmashUp.");
		// *************************************************************************

		$player_key = explode(",", $player_id_map[$player_id]);
		$player = $marqueeMatchups[$player_key[0]][$player_key[1]][$player_key[2]][$player_key[3]][$player_key[4]][$player_key[5]];
		
		
		// *************************************************************************
		// make sure that no player on the same team has already been picked
		// *************************************************************************
		if ( in_array($player->teams_id, $smashupsData[$matchup->home_user_id]["players"]["team_ids"]) || in_array($player->teams_id, $smashupsData[$matchup->away_user_id]["players"]["team_ids"]) ) {
			throw new My_Exception_HandledException("This player is no longer available for this SmashUp.");
		}
		// *************************************************************************
		
		
		// *************************************************************************
		// make sure user can pick a player at this position
		// *************************************************************************
		if (!key_exists($player->position, $smashupsData[$this->currentUser->id]["players"]) 
			|| !is_array($smashupsData[$this->currentUser->id]["players"][$player->position])
			|| empty($smashupsData[$this->currentUser->id]["players"][$player->position])) {
			throw new My_Exception_HandledException("You can not select a player at this position for this SmashUp.");
		}
		$smashupsAtPosition = $smashupsData[$this->currentUser->id]["players"][$player->position];

		if ($smashupsData[$this->currentUser->id]["players"][$player->position][count($smashupsAtPosition)-1] !== null) {
			throw new My_Exception_HandledException("All {$player->position} have already been filled.");
		}
		// *************************************************************************
		
		
		// *************************************************************************
		// If this is a forced pick, make sure that pick is on opposite team, same position
		// as last pick.
		// *************************************************************************
		if (!$this->view->nextPick["is_smash_pick"] ) {
			$last_smashup = $smashupsDb->getLastSmashup($matchup->id);
			$last_game = $gamesDb->getMapper()->find($last_smashup->games_id);
			$last_player = Application_Model_DbTable_Players::getMapper()->find($last_smashup->players_id);
			$forced_team_id = ($last_player->teams_id == $last_game->home_team_id) ? $last_game->away_team_id : $last_game->home_team_id;
			
			if (($player->teams_id != $forced_team_id) || ($player->position != $last_player->position) ) {
				$forced_team = Application_Model_DbTable_Teams::getMapper()->find($forced_team_id);
				throw new My_Exception_HandledException("The SmashUp must be from the ".$forced_team->name." at ".$last_player->position);
			}
		}
		
		
		//  The pick should be valid so record it and report it to the user
		$newSmashupData = array(
			"users_matchups_id" => $matchup->id,
			"users_id" => $this->currentUser->id,
			"games_id" => $marqueeMatchups[$player_key[0]]["game"]->id,
			"players_id" => $player->id
		);
		
		$smashup = Application_Model_DbTable_UsersSmashups::getMapper()->getModel($newSmashupData);
		$smashup->save();
		$response = array();
		
		if ($smashup->getId()) {
			
			// ***************************************************************************
			// Email the opponent of the smashpick activity
			// ***************************************************************************
			$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$site = $config->getOption("site");
			$team = Application_Model_DbTable_Teams::getMapper()->find($player->teams_id);
			
			if ($this->view->isCurrentUserHomeUser) {
				$homeCount++;
			} else {
				$awayCount++;
			}
			
			$storage = new stdClass();
			$storage->isCurrentUserHomeUser = !$this->view->isCurrentUserHomeUser;
			$storage->settings = $this->view->settings;
			
			$this->determineWhoPicksWithMessages($home_user, $homeCount, $away_user, $awayCount, $storage);
			
			$templateData = array(
					"to_user_name" 					=> $opponent->getName(),
					"from_user_name" 				=> $this->currentUser->getName(),
					"site_url"						=> "http://".$site["url"],
					"matchup_id"					=> $matchup->id,
					"smashup"						=> "{$player->name} from {$team->name} at {$player->position}",
					"next_action"					=> "<a href='http://{$site["url"]}/game/smashup/matchup/{$matchup->id}'>{$storage->user_messages[0]}</a>"
			);
			
			$parser = new My_TemplateParser($templateData);
			$content = $parser->parseTemplateFile(APPLICATION_PATH."/layouts/email_templates/en/smashup_pick_notification.html", $templateData);
			
			$mail = new Zend_Mail();
			$mail->setFromToDefaultFrom();
			$mail->setSubject($this->currentUser->getName()." has made a ". $this->view->nextPick["is_smash_pick"] ? "Smash": "Force"  ." Pick!");
			$mail->setBodyHtml($content);
			$mail->addTo($opponent->email, $opponent->getName());
			$result = $mail->send();
			// ***************************************************************************
			
			$response['success'] =  true;
			$response['user_message'] = "You have successfully chosen ".$player->name." of the ".$marqueeMatchups[$player_key[0]][$player_key[1]][$player_key[2]]['team']->name." at ".$player->position;
		} else {
			throw new My_Exception_HandledException("Services are unavailble at this time.  Please try again later.");
		}
		$this->_helper->json($response);
	}
	
	/**
	 * Determine who's pick it is and sets messages accordingly
	 * 
	 * @param Application_Model_Users 	$home_user	the home user
	 * @param int 						$homeCount	number of picks already chosen by home team
	 * @param Application_Model_Users 	$away_user	the away user
	 * @param int 						$awayCount	number of picks already chosen by away team
	 */
	private function determineWhoPicksWithMessages( Application_Model_Users $home_user, $homeCount, Application_Model_Users $away_user, $awayCount, &$storage = null ) {
		
		if (!$storage) {
			$storage = $this->view;
		}

		$storage->user_messages = array();
		
		if ( ($homeCount == $awayCount) && ($homeCount == $storage->settings->number_of_matchups) ) {
			 
			// we will use a double null as the sentinel value that the smashup rounds have all been completed.
			$storage->nextPick = array("is_home_user" => null, "is_smash_pick" => null );
			$storage->user_messages[] = "This SmashUp is complete!";
			 
		} else if (($homeCount == $awayCount) && ($homeCount%2 == 0)) {
			 
			$storage->nextPick = array("is_home_user" => true, "is_smash_pick" => true );
			$storage->user_messages[] = ($storage->isCurrentUserHomeUser) ? "It is now your SMASH PICK!" : "It is ".$home_user->getName()."'s SMASH PICK!";
			 
		} else if (($homeCount == $awayCount) && ($homeCount%2 == 1)) {
			 
			$storage->nextPick = array("is_home_user" => false, "is_smash_pick" => true );
			$storage->user_messages[] = (!$storage->isCurrentUserHomeUser) ? "It is now your SMASH PICK!" : "It is ".$away_user->getName()."'s SMASH PICK!";
			 
		} else if ($homeCount < $awayCount) {
			 
			$storage->nextPick = array("is_home_user" => true, "is_smash_pick" => false );
			$storage->user_messages[] = ($storage->isCurrentUserHomeUser) ? "It is now your FORCE PICK." : "It is ".$home_user->getName()."'s FORCE PICK.";;
			 
		} else {
			 
			$storage->nextPick = array("is_home_user" => false, "is_smash_pick" => false );
			$storage->user_messages[] = (!$storage->isCurrentUserHomeUser) ? "It is now your FORCE PICK." : "It is ".$away_user->getName()."'s FORCE PICK.";
			 
		}
	}	
}

?>
