<?php

class IndexController extends My_Controller_Action
{

	private $request;
	
    public function init()
    {
        /* Initialize action controller here */
    	$this->request = $this->getRequest();
    }

    public function indexAction()
    {
    }

    public function howtoplayAction()
    {

    }

    public function blogAction()
    {
    
    }
    
    public function contactAction()
    {
        $form = new Application_Form_Contact();
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {
                    $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
                    $mailConfig = $config->getOption("resources")['mail'];
                    
                    #Parse html template files
                    $parser = new My_TemplateParser($formData);
                    $content = $parser->parseTemplateFile(APPLICATION_PATH . '/layouts/email_templates/en/contactform.html');

                    #Send email
                    $mail = new Zend_Mail();
                    $mail->setSubject('Contact Form Message - Smash Fantasy Sports');
                    $mail->setBodyHtml($content);
                    $mail->setFrom($formData['email'], $formData['name']);
                    $mail->setReplyTo($formData['email'], $formData['name']);
                    $mail->addTo( $mailConfig["admin_email"], $mailConfig["admin_name"]);
                    if ($mail->send()) {
                        $this->addMessage("Your message has been submitted to the SmashMouth Fantasy Sports Team. <br /> Thank you!");
                        $this->redirect('/home');
                    } else {
                         $this->addMessage('There was a problem in sending your message.  Please e-mail us directly or try again in a few minutes.');
                    }
            }
        }


        $this->view->form = $form;    	
    }

    public function aboutusAction()
    {   	
    }

    /**
     * Gets the current leaders in Matchup play.  This will eventually be expanded to cover 
     * individual sports, but for now is set to cover NBA individual play.  This will be passed
     * in the sports_id parameter
     * 
     * Parameters can be passed to fetch a subset.  This will be stored in
     * the pageIndex which begins at 0 and limit parameter which specifies the max
     * number of users to be returned.
     */
    public function leaderboardAction()
    {
		$userMatchupsDb = new Application_Model_DbTable_UsersMatchups();
		
		$this->view->leaders = $userMatchupsDb->getLeaderBoard(
									$this->request->getParam("sports_id", 1),
									$this->request->getParam("pageIndex", null),
									$this->request->getParam("limit", null)
								);
		
    }
    
    public function leaguescomingsoonAction()
    {
        
    }

    public function privacyAction()
    {
    
    }
    
    public function termsAction()
    {
    
    }

}

