<?php
/**
 * The FeedController is the class central to communicating with sportsdatallc
 * It is run by means of cron/scheduled task and must be implemented from a
 * CLI interface.
 * 
 * @author Leon McCottry
 *
 */
class FeedController extends Zend_Controller_Action {
	public function init() { }
	
	public function preDispatch() {
		if (PHP_SAPI != 'cli') {
			//throw new My_Exception_HandledException("Access Denied!");
		}
	}
	
	/**
	 * Updates all players seaosonal stats to the date
	 */
	public function syncnbaseasonstatsAction() {
		
		$season_id = 1;
		
		$season = Application_Model_DbTable_Seasons::getMapper()->find($season_id);
		
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$sportsDataLLCOptions = $config->getOption('sportsdatallc');
		
		$teamsDb = new Application_Model_DbTable_Teams();
		$teams =  $teamsDb->getModelsFromRows($teamsDb->getMapper()->fetchAll());
		
		$playersMapper = Application_Model_DbTable_Players::getMapper();
		/* @var $playersMapper	Application_Model_PlayersMapper */
		
		$playerStatsDb = new Application_Model_DbTable_SeasonPlayerStats();
		
		$playerStatsDb->getAdapter()->beginTransaction();
		
		$log = Zend_Registry::get("log");
		
		try {
			// Get the latest rosters
			foreach( $teams as $team ) {
				try {
					$log->info( "fetching {$team->name}:$team->uuid" );
					$context = stream_context_create(array('http' => array('header'=>'Connection: close')));			
					$seasonStatsFeed = file_get_contents("http://api.sportsdatallc.org/nba-{$sportsDataLLCOptions['nba_access_level']}{$sportsDataLLCOptions['nba_version']}/seasontd/{$season->year}/reg/teams/{$team->uuid}/statistics.xml?api_key={$sportsDataLLCOptions['nba_api_key']}");
	
					// sleep 1 second if this is a trial account
					if ($sportsDataLLCOptions['nba_access_level'] == 't') sleep(1);
			
					$seasonNode = new SimpleXMLElement($seasonStatsFeed);
			
					foreach($seasonNode->team->player_records->player as $player) {
						try {			
							$foundPlayer = $playersMapper->findByUuid($player['id']);
				
							if (!$foundPlayer) continue;
							
							$playerStats = $playerStatsDb->getMapper()->findBySeasonAndPlayer($season->id, $foundPlayer->id);
							
							$recordedStats = array(	
								"points" => true,
								"turnovers" => true,
								"assists" => true,
								"blocks" => true,
								"personal_fouls" => true,
								"rebounds" => true,
								"steals" => true
							);
							
							$statString = "{$foundPlayer->name}:id[{$foundPlayer->id}] stats=";
							
							// updated already recorded stats
							foreach($playerStats as $playerStat) {
								$playerStat->value = $player->overall->average[0][$playerStat->stat] ? $player->overall->average[0][$playerStat->stat] : 0;
								$playerStat->save();
								$statString .= "<{$playerStat->stat},{$playerStat->value}>";
								unset($recordedStats[$playerStat->stat]);
							}
							
							// check for new stats and record
							foreach($recordedStats as $statType => $isRecorded ) {		
								if ($player->overall->average[0][$statType]) {
									$newStat = $playerStatsDb->getMapper()->getModel(
										array(
											"seasons_id" => $season->id,
											"players_id" => $foundPlayer->id,
											"stat" => $statType,
											"value" => ($player->overall->average[0][$statType]) ? $player->overall->average[0][$statType] : 0
										)
									);
									
									$newStat->save();
									$statString .= "<{$newStat->stat},{$newStat->value}>";
								}
							}
							
							$log->info( $statString );
						} catch (Exception $e) {
							$log->err("Player error - skipping to next player: " + $e->getMessage());
						}
					}
			
					$log->info("finished {$team->name}");
				} catch (Exception $e) {
					$log->err( "team error - skipping to next team: ".$e->getMessage() );
				}
			}
		} catch (Exception $e) {
			$e->getMessage();
			$log->err("error: ".$e->getMessage());
		}
		
		// End transaction
		$playerStatsDb->getAdapter()->commit();
	}
	
	/**
	 * Real-time update of the players stats
	 */
	public function syncnbastatsAction() {

		echo "Here we go....\n";

		$log = Zend_Registry::get("log");
		$log->info("Running stat sync.");
		
		$season_id = 1;
		
		$matchSettingsDb = new Application_Model_DbTable_MarqueeMatchupSettings();
		$marqueeSettings = $matchSettingsDb->getCurrentMarqueeMatchupSettings($season_id, true);

		if (is_null($marqueeSettings)) {
			$log->info("No marquee matchups to check.");
			throw new My_Exception_HandledException("This service is temporarily unavailable.  Please try again later.");
		}
		
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$sportsDataLLCOptions = $config->getOption('sportsdatallc');
		
		// get the current games
		$gamesDb = new Application_Model_DbTable_Games();
		$marqueeMatchups = $gamesDb->getMarqueeMatchups($marqueeSettings->id);
		$player_id_map = array_pop($marqueeMatchups);

		$teamsMapper = Application_Model_DbTable_Teams::getMapper();
		/* @var $teamsMapper	Application_Model_TeamsMapper */
		
		$playersMapper = Application_Model_DbTable_Players::getMapper();
		/* @var $playersMapper	Application_Model_PlayersMapper */
		
		$playerStatsDb = new Application_Model_DbTable_GamePlayerStats();
		
		//*************************************************************************
		// Use this data to determine if we change the status of matchups to 
		// in_progress or completed based on the settings 'number_of_matchups'
		//*************************************************************************		
		$matchupsInProgress = 0;
		$matchupsCompleted = 0;
		$numberOfMatchups = $marqueeSettings->number_of_matchups;
		//*************************************************************************
		
		try {
			// Update stats for all in progress games.
			foreach( $marqueeMatchups as $matchupInfo ) {
				
				$game = $matchupInfo['game'];
				/* @var $game	Application_Model_Games */
				
				$homeTeam = $game->getHomeTeam();
				$awayTeam = $game->getAwayTeam();
				$log->info( "checking {$homeTeam->name}:{$homeTeam->uuid} vs. {$awayTeam->name}:{$awayTeam->uuid}" );
				
				
				// continue if the game is already over and count it as completed
				if ($game->status == "final") {
					$matchupsCompleted++;
					$log->info( "already completed -- {$homeTeam->name}:{$homeTeam->uuid} vs. {$awayTeam->name}:{$awayTeam->uuid}" );
					continue;
				}

				//**************************************************************
				// check to see if we need to mark the game as in progress
				//**************************************************************
				$currentTime = time();
				$gameTime = strtotime($game->scheduled_date);
				$log->info("The current time is: $currentTime.  Game time is $gameTime");
								
				if (($game->status == "pending") && ($currentTime > $gameTime) ) {
					$game->status = "in_progress";
					$log->info( "starting {$homeTeam->name}:{$homeTeam->uuid} vs. {$awayTeam->name}:{$awayTeam->uuid}" );					
				}
				//**************************************************************

				
				
				//**************************************************************
				// for games in progress, update the score and players stats
				//**************************************************************
				if ($game->status == "in_progress") {			
					$log->info("-------------------------------CALLING SPORTSDATALLC----------------------------");		
					$context = stream_context_create(array('http' => array('header'=>'Connection: close')));
					$gameFeedUrl = "http://api.sportsdatallc.org/nba-{$sportsDataLLCOptions['nba_access_level']}{$sportsDataLLCOptions['nba_version']}/games/{$game->uuid}/summary.xml?api_key={$sportsDataLLCOptions['nba_api_key']}";
					$gameFeed = file_get_contents($gameFeedUrl);
					
					// sleep 1 second if this is a trial account
					if ($sportsDataLLCOptions['access_level'] == 't') sleep(1);
					
					$gameNode = new SimpleXMLElement($gameFeed);
					
					$game->period = $gameNode["quarter"];
					$game->time = "00:".$gameNode["clock"];
					
					$log->info("The quarter is {$game->period}.  The clock reads {$game->time}");
					
					foreach($gameNode->team as $team ) {
						try {
							//**************************************************************
							// update score
							//**************************************************************
							if ($team["id"] == $homeTeam->uuid) {
								$game->home_team_score = $team["points"];
								$currentTeam =  $homeTeam;
							} else if ($team["id"] == $awayTeam->uuid) {
								$currentTeam =  $awayTeam;
								$game->away_team_score = $team["points"];
							}
							//**************************************************************
						
	
							//**************************************************************
							// update player stats
							//**************************************************************
							foreach($team->players->player as $player) {
								try {
									$foundPlayer = $playersMapper->findByUuid($player['id']);
									$playerStats = $playerStatsDb->getMapper()->findByGameAndPlayer($game->id, $foundPlayer->id);
									
									$recordedStats = array(	"points" => true,
															"tech_fouls" => true,
															"personal_fouls" => true,
															"blocks" => true,
															"steals" => true,
															"turnovers" => true,
															"assists" => true,
															"rebounds" => true
									);
									
									$statString = "{$foundPlayer->name}:id[{$foundPlayer->id}] stats=";
									
									// updated already recorded stats
									foreach($playerStats as $playerStat) {
										$playerStat->value = $player->statistics[$playerStat->stat] ? $player->statistics[$playerStat->stat] : 0;
										$playerStat->save();
										$statString .= "<{$playerStat->stat},{$playerStat->value}>";
										unset($recordedStats[$playerStat->stat]);
									}
									
									// check for new stats and record
									foreach($recordedStats as $statType => $isRecorded ) {
										
										if ($player->statistics[$statType]) {
											$newStat = $playerStatsDb->getMapper()->getModel(
												array(
													"seasons_id" => $game->seasons_id,
													"games_id" => $game->id,
													"teams_id" => $currentTeam->id,
													"players_id" => $foundPlayer->id,
													"stat" => $statType,
													"value" => ($player->statistics[$statType]) ? $player->statistics[$statType] : 0
												)
											);
											
											$newStat->save();
											$statString .= "<{$newStat->stat},{$newStat->value}>";
										}
									}
									
									$log->info( $statString );
								} catch (Exception $e) {
									$log->err( "player error - skipping to next player: ".$player['full_name']."\n".$e->getMessage() );
									$log->err("feed url: ".$gameFeedUrl);
									try {
										$this->syncnbarosterAction();
									} catch (Exception $e2) {
										// clobber exception
									}
								}		
							}
							//**************************************************************
						} catch (Exception $e) {
							$log->err( "team error - skipping to next team: ".$e->getMessage() );
							$log->err("feed url: ".$gameFeedUrl);
						}
					
					}
					
					$log->info( "The score is now {$homeTeam->name}:{$game->home_team_score} vs. {$awayTeam->name}:{$game->away_team_score}" );
					
					//**************************************************************
					// Check if the game is has completed or still in progress
					//**************************************************************
					if ($gameNode['status'] == "closed") {
						$game->status = "final";
						$matchupsCompleted++;
						$log->info( "has completed -- {$homeTeam->name}:{$homeTeam->uuid} vs. {$awayTeam->name}:{$awayTeam->uuid}" );					
					} else {
						$matchupsInProgress++;
					}
										
				}
				
				$game->save();
				$log->info( "starting {$homeTeam->name}:{$homeTeam->uuid} vs. {$awayTeam->name}:{$awayTeam->uuid}" );					
				
			}
			
			//*************************************************************************
			// Update the status of matchups based on the number of games in progress
			// and completed.
			//*************************************************************************
			$matchupDb = new Application_Model_DbTable_UsersMatchups();
			$log->info( "trying to update matchups status.  $matchupsCompleted:$numberOfMatchups" );
				
			if ($matchupsCompleted == $numberOfMatchups) {
				//switch all in_progress matchups to completed
				$select = $matchupDb->select()->where("seasons_id = $season_id AND status IN ('in_progress','accepted')");
				$log->info( "completing matchups: $select" );
				$matchups = $matchupDb->getModelsFromRows($matchupDb->fetchAll($select));	
				foreach($matchups as $matchup) {
					$matchup->status = "completed";
					$matchup->save();
				}	
			} else if ($matchupsInProgress) {
				// switch all accepted matchups to in_progress
				$select = $matchupDb->select()->where("seasons_id = $season_id AND status = 'accepted'");
				$rows = $matchupDb->fetchAll($select);
				if ($rows->count()) {		
					$log->info( "switching matchups to in_progress: $select" );
					$matchups = $matchupDb->getModelsFromRows($rows);
					foreach($matchups as $matchup) {
						$matchup->status = "in_progress";
						$matchup->save();
					} 
				} else {
					$log->info("no matchups to update at this time");
				}

				// switch all requested to cancelled
				$select = $matchupDb->select()->where("seasons_id = $season_id AND status = 'requested'");
				$rows = $matchupDb->fetchAll($select);
				if ($rows->count()) {
					$log->info( "switching matchups to cancelled: $select" );
					$matchups = $matchupDb->getModelsFromRows($rows);
					foreach($matchups as $matchup) {
						$matchup->status = "cancelled";
						$matchup->save();
					}
				} else {
					$log->info("no matchups to cancel at this time");
				}
			}
			//*************************************************************************
					
		} catch (Exception $e) {
			$e->getMessage();
			$log->err("error: ".$e->getMessage());			
			exit;
		}
		
		$log->info("Completed sync.");
	}
		
        public function loadgamesfromfeedAction() {

            // For now, there no view for this action
            $this->_helper->viewRenderer->setNoRender(true);
            
            // get the year and seasontype (pre/reg/pst) from the url
            $request = $this->getRequest();
            $year = $request->getParam("year");
            $seasontype =$request->getParam("seasontype");
            
            // get SportsData LLC parameters and construct feed URL
            $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $sportsDataLLCOptions = $config->getOption('sportsdatallc');
            $url =  "http://api.sportsdatallc.org/nba-".
                    "{$sportsDataLLCOptions['nba_access_level']}".
                    "{$sportsDataLLCOptions['nba_version']}".
                    "/games/{$year}/{$seasontype}/schedule.xml?api_key={$sportsDataLLCOptions['nba_api_key']}";
                    
            // Load the feed into a variable
            $schedulefeed = file_get_contents($url);
            
            // parse the feed for game info
            $leagueNode = new SimpleXMLElement($schedulefeed);
            
            
            $games = $leagueNode->{"season-schedule"}->games;
            
            $gamesdb = new Application_Model_DbTable_Games();
            print "<pre>";
            foreach ($games->children() as $game) {
                
                
                
                $uuid = (string)$game->attributes()->id;
                $home_team_uuid = (string)$game->attributes()->home_team;
                $away_team_uuid = (string)$game->attributes()->away_team;
                $scheduled = (string)$game->attributes()->scheduled;
                
                echo "$uuid,$home_team,$away_team,$scheduled\n";
                if (!$gamesdb->gameExists($uuid)) {
                    echo "Adding new entry.\n";
                    try {
                        $gamesdb->addGame($uuid, $home_team_uuid, $away_team_uuid, $scheduled);
                    }
                    catch (Exception $e) {
                        echo "oops -> ",$e->getMessage(),"\n";
                    }
                } else {
                    echo "Already in database.\n";
                }
                
            }
            
            
            
            print "</pre>";
            
            
            
            
        }
        
       
        
	public function syncnbarosterAction()
	{
		$log = Zend_Registry::get("log");
		
		try {
			// first try to update the seasonal stats
			$this->syncnbaseasonstatsAction();
		} catch ( Exception $e ) {
			echo $e->getMessage();
			$log->err("error: ".$e->getMessage());			
		}
		
		//$sportsDataLLCOptions = $this->getInvokeArg('bootstrap')->getOption('sportsdatallc');
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$sportsDataLLCOptions = $config->getOption('sportsdatallc');

		$teamsDb = new Application_Model_DbTable_Teams();
		$teams =  $teamsDb->getModelsFromRows($teamsDb->getMapper()->fetchAll());
		
		$playersDb = new Application_Model_DbTable_Players();
		// Begin transaction
		$playersDb->getAdapter()->beginTransaction();
		
		$updated = date("Y-m-d H:i:s");
		
		try {		
			// Get the latest rosters
			foreach( $teams as $team ) {
				$log->info( "starting {$team->name}:$team->uuid" );
				$context = stream_context_create(array('http' => array('header'=>'Connection: close')));
				$rosterFeed = file_get_contents("http://api.sportsdatallc.org/nba-{$sportsDataLLCOptions['nba_access_level']}{$sportsDataLLCOptions['nba_version']}/teams/{$team->uuid}/profile.xml?api_key={$sportsDataLLCOptions['nba_api_key']}");
				
				// sleep 1 second if this is a trial account 
				if ($sportsDataLLCOptions['nba_access_level'] == 't') sleep(1);
				
				$teamNode = new SimpleXMLElement($rosterFeed);
	
				foreach($teamNode->players->player as $player) {
					
					$isActive = true;
					if ($player->injuries && $player->injuries->injury[0]) {
						$isActive = (int)(stripos($player->injuries->injury[0]["status"],"Out") === false);
					}
					
					$foundPlayer = $playersDb->getMapper()->findByUuid($player['id']);
	
					if ($foundPlayer) {
						// update existing player
						$foundPlayer->name = $player['full_name'];
						$foundPlayer->position = $player['primary_position'];
						$foundPlayer->teams_id = $team->id;
						$foundPlayer->active = $isActive;
						$foundPlayer->date_updated = $updated;
					} else {
						// add new player
						$newPlayerData = array(
							"uuid" => $player['id'],
							"name" => $player['full_name'],
							"position" => $player['primary_position'],
							"teams_id" => $team->id,
							"active" => $isActive,
							"date_updated" => $updated
						);
						
						$foundPlayer = $playersDb->getMapper()->getModel($newPlayerData);
					}
					
					$foundPlayer->save();
				}
	
				$log->info("finished {$team->name}");		
			}
		} catch (Exception $e) {
			echo $e->getMessage();
			$log->err("error: ".$e->getMessage());
		}

		// End transaction
		$playersDb->getAdapter()->commit();
		
		// Dissassociate and deactivate existing players that no longer seem to have a team
		$released_players = $playersDb->getPlayersNotUpdated($updated);
		
		$log->info("removing release players -- ");
		$numberRemoved = 0;
		foreach($released_players as $released_player) {
			$released_player->active = 0;
			$released_player->teams_id = null;
			$released_player->save();
			$numberRemoved++;
		}
		$log->info("$numberRemoved released players removed successfully!!!!!!!!");
		
		// Update headshots
		$log->info("getting headshots");
		$headshotFeed = file_get_contents("http://api.sportsdatallc.org/nba-images-{$sportsDataLLCOptions['image_access_level']}{$sportsDataLLCOptions['image_version']}/manifests/headshot/all_assets.xml?api_key={$sportsDataLLCOptions['image_api_key']}");

		$assetlistNode = new SimpleXMLElement($headshotFeed);
		
		foreach($assetlistNode->asset as $asset) {
			try {
				$player = $playersDb->getMapper()->findByUuid($asset["player_id"]);
				
				if ($player && ($player->asset_id != $asset['id'])) {
					
					// sleep 1 second if this is a trial account
					if ($sportsDataLLCOptions['image_access_level'] == 't') sleep(1);
					
					$player->asset_id = $asset['id'];
									
					if ($asset->links->link[0]) {
						$imageLarge = file_get_contents("http://api.sportsdatallc.org/nba-images-{$sportsDataLLCOptions['image_access_level']}{$sportsDataLLCOptions['image_version']}/headshot/{$asset['id']}/{$asset->links->link[0]["width"]}.jpg?api_key={$sportsDataLLCOptions['image_api_key']}");
						file_put_contents(APPLICATION_PATH."/../public/images/players/headshots/{$asset['id']}_large.jpg", $imageLarge);
					}
					
					if ($asset->links->link[1]) {
						$imageSmall = file_get_contents("http://api.sportsdatallc.org/nba-images-{$sportsDataLLCOptions['image_access_level']}{$sportsDataLLCOptions['image_version']}/headshot/{$asset['id']}/{$asset->links->link[1]["width"]}.jpg?api_key={$sportsDataLLCOptions['image_api_key']}");
						file_put_contents(APPLICATION_PATH."/../public/images/players/headshots/{$asset['id']}_small.jpg", $imageSmall);
					}				
					
					$player->save();
					$log->info("saved images for {$player->name}");;
				}
			} catch (Exception $e) {
				$e->getMessage();
				$log->err("error: ".$e->getMessage());
			}
		}
	}
}
?>
