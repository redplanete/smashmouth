<?php
/**
 * Entry point for all account related request.  If the user has not authenticated,
 * he will be sent to login.
 * 
 * @author Leon McCottry
 *
 */
class AccountController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }
    
    /**
     * Makes sure that the user has logged in before allowing any account actions.
     * 
     * @see Zend_Controller_Action::preDispatch()
     */
    public function preDispatch() {
    	$account = Application_Model_Account::getAccountFromIdentity();
    	$exclusions = array("add");
    	if (!isset($account) && !in_array($this->getRequest()->getActionName(),$exclusions)) {
    		// redirect to login if there is no identity in the auth storage.
    		$this->redirect('/authentication/login');
    		return false;
    	}
    }

    public function indexAction() {
    	$this->_forward('dashboard');
    }
    
    public function dashboardAction() {
        $this->view->account = Application_Model_Account::getAccountFromIdentity();      
        $this->view->sentInvites = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("requested"), array("initiator"));
        $this->view->receivedInvites = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("requested"), array("recipient"));
        $this->view->friendAccounts = Application_Model_Account::getAssociatedUsers();
        $this->view->pendingFriendRequest = Application_Model_Account::getAssociatedUsers(array("requested"),array('receiver_users_id'));
        $this->view->upcomingSmashups = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("accepted"));
        $this->view->inProgressMatchups = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("in_progress"));
        $this->view->completedMatchups = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("completed"), null, array("id DESC"), $limit = 10 );
    }

    /**
     * Returns a list of all of this users friends.  This is called via XHR
     */
    public function getfriendlistAction() {
    	$friendAccounts = Application_Model_Account::getAssociatedUsers();
    	$response = array('success' => true, 'friends' => array() );
    	foreach($friendAccounts as $friend) {
    		$response['friends'][] = array('id' => $friend->id, 'name' => $friend->getName());
    	}
    	$this->_helper->json($response);
    }

    /**
     * Returns a list of all invites of this user.
     * the parameter role determines if received or sent invites are returned.
     * valid values are "initiator" and "recipient"
     * 
     * This is called via XHR
     */
    public function getinvitelistAction() {
    	
    	$role = $this->getParam("role");
    	
    	if (!in_array(array($role, "initiator","recipient"))) {
    		$this->_helper->json( array( "success" => false ) );
    		return;
    	}
    	
    	$invites = Application_Model_DbTable_UsersMatchups::getMatchupsForAccount(null, array("pending"), array($role));

    	$response = array('success' => true, 'invites' => array() );
    	
    	foreach($invites as $invite) {
    		$response['invites'][] = array('id' => $invite->id, 'name' => $invite->getName());
    	}
    	
    	$this->_helper->json($response);
    }
    
    /**
     * Searches through those who the user is not already connected.  This is used
     * to auto complete search form on dashboard page via XHR
     */
    public function friendsearchAction() {
        
        /* @var $request Zend_Controller_Request_Http */
        $request = $this->getRequest();
        $searchTerm = $request->getQuery("term");
        $sportsId = (array) $request->getParam("sport");
        
        $log = Zend_Registry::get("log");
            
		$response = array();
              
		$usersDbTable = new Application_Model_DbTable_Users();
		$matchingUsers  = $usersDbTable->searchAccountsByNameOrEmail($searchTerm);

		$currentUser = Zend_Auth::getInstance()->getIdentity()->getUser();

		foreach($matchingUsers as $user) {
        	if ($user->id == $currentUser->id) continue;
            	
            $matchupsDbTable = new Application_Model_DbTable_UsersMatchups();          	
            $preexistingMatchups = $matchupsDbTable->getExistingMatchups($user, $currentUser, $sportsId);    	
            if (count($preexistingMatchups)) continue;
          	
            $userFriendsDbTable = new Application_Model_DbTable_UsersFriends();
            if ( $userFriendsDbTable->haveRelationship($user, $currentUser, array('requested','accepted','blocked'))) continue;
          	
            $response[] = array(
            	"label" => "$user->name <$user->email>",
            	"value" => $user->name,
            	"user_id" => $user->id,
            	"search" => $searchTerm,
            );
		}
		
		if (empty($response)) {
			$response[] = array(
            	"label" => "no users where found for '$searchTerm', but you can invite them by email!",
            	"value" => $searchTerm,
            	"user_id" => null,
            	"search_term" => $searchTerm,
            );
		}
		
        $this->_helper->json($response);           
    }

    public function sendfriendrequestAction() {
        $request = $this->getRequest();
        $receiverUsersId = $request->getParam("userId");
        
        $auth = Zend_Auth::getInstance();
        $id = $auth->getIdentity()->id;

        $userFriendsDbTable = new Application_Model_DbTable_UsersFriends();
        
        $insertData = array();
        $insertData['sender_users_id'] = $id;
        $insertData['receiver_users_id'] = $receiverUsersId;
        $insertData['status'] = "requested";
        
        $friendRequest = $userFriendsDbTable->getMapper()->getModel($insertData);

        $friendRequest->save();
        
        // ***************************************************************************
        // send e-mail to member notifying them of friend request
        // ***************************************************************************
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $site = $config->getOption("site");
        	
        $auth = Zend_Auth::getInstance();
        $currentUser = $auth->getIdentity()->getUser();
        $receiver = Application_Model_DbTable_Users::getMapper()->find($receiverUsersId);
        
        $templateData = array(
        		"to_user_name" 			=> $receiver->getName(),
        		"from_user_name" 		=> $currentUser->getName(),
        		"site_url"				=> "http://".$site["url"]
        );
        
        $parser = new My_TemplateParser($templateData);
        $content = $parser->parseTemplateFile(APPLICATION_PATH."/layouts/email_templates/en/friend_request_to_member.html", $templateData);
        
        $mail = new Zend_Mail();
        $mail->setFromToDefaultFrom();
        $mail->setSubject("You have a new Friend Request!");
        $mail->setBodyHtml($content);
        $mail->addTo($receiver->email, $receiver->getName());
        $result = $mail->send();
        // ***************************************************************************
        
        $response = array();
        
        if ($friendRequest->getId()) {
        	$response['success'] =  true;
        	$response['receiver_name'] = $receiver->getName();
        	$response['receiver_id'] = $receiver->id;
        	$response['user_message'] = "Successfully sent friend request to ".$receiver->getName();
        } else {
        	throw new My_Exception_HandledException("Services are unavailble at this time.  Please try again later.");
        }
        
        $this->_helper->json($response);        
    }

    public function invitetoregisterAction() {   
        $request = $this->getRequest();
        $response = array();
		$email = $request->getParam("email");
		
		// Validate email
		$emailValidator = new Zend_Validate_EmailAddress();
		if (!$emailValidator->isValid($email)) {
			foreach($emailValidator->getMessages() as $message) {
				$response[] = $message;
			}
			throw new My_Exception_HandledException(implode("<br/>\n",$response));
		}

		// gather email data from the application.ini file
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$site = $config->getOption("site");
			
        $auth = Zend_Auth::getInstance();
        $currentUser = $auth->getIdentity()->getUser();

        $invite_link = Application_Model_DbTable_UsersFriends::createInviteLink();
        
        $templateData = array(
        	"name" 					=> $currentUser->getName(),
        	"email" 				=> $currentUser->email,
        	"url_register"			=> "http://".$site["url"]."/account/add/invite_link/".$invite_link,
	        "url_smashmouth_home"	=> "http://".$site["url"]
	    );

		$parser = new My_TemplateParser($templateData);
       	$content = $parser->parseTemplateFile(APPLICATION_PATH."/layouts/email_templates/en/friend_request_to_non_member.html", $templateData);
        
       	$mail = new Zend_Mail();
       	$mail->setFromToDefaultFrom();
        $mail->setSubject($currentUser->getName()." has invited you to SmashMouth Fantasy Sports");
        $mail->setBodyHtml($content);
        $mail->addTo($email, "SmashUp");
        $result = $mail->send();

        // add $invite_link to database so that when the friend signs up, this user will
        // automatically send him a friend request
        $friendshipRequestData = array(
        		"sender_users_id" => $currentUser->id,
        		"status" => "requested",
        		"invite_link" => $invite_link
        );
        
        $friendRequest = Application_Model_DbTable_UsersFriends::getMapper()->getModel($friendshipRequestData);
        
        $friendRequest->save();
        $response = array();
        
        if ($friendRequest->getId()) {
        	$response['success'] =  true;
        	$response['receiver_email'] = $email;
        	$response['user_message'] = "An invitation was successfully sent to ".$email;
        } else {
        	throw new My_Exception_HandledException("Services are unavailble at this time.  Please try again later.");	
        }
        $this->_helper->json($response);
    }

    /**
     * Attempts to set a friend request to accepted
     */
    public function friendrequestapproveAction() {
    	$this->friendRequestResponse('accepted','requested');
    }
    
    /**
     * Attempts to set a friend request to 'declined'
     */
    public function friendrequestdeclineAction() {
    	$this->friendRequestResponse('declined','requested');
    }
    
    /**
     * Attempts to changes the status of a relationship to the specified status
     * Certain rules however prevent many changes.  For instance, a relation
     * cannot go from 'blocked' to 'accepted'.  In fact, only relations in 
     * the status of 'requested' may move to 'accepted'.  In the latter case,
     * an event is automatically triggered.
     * 
     * @param string $oldStatus
     * @param string $newStatus
     */
    private function friendRequestResponse($newStatus = 'accepted', $oldStatus = 'requested') {
            $auth = Zend_Auth::getInstance();
            $user = $auth->getIdentity()->getUser();
            /* @var $user Application_Model_Users */
            
            $senderUser = $user->getMapper()->find($this->getRequest()->getParam('requestorId'));
            
            $usersFriendsDb = new Application_Model_DbTable_UsersFriends();
			$usersFriend = $usersFriendsDb->haveRelationship($user, $senderUser, array($oldStatus));

            $response = array("success" => false);
            if ($usersFriend) {
            	$usersFriend->status = "$newStatus";
            	$result = $usersFriend->save();
            	if ($result) {
            		$response['success'] =  true;
            		if ($oldStatus == 'requested' && $newStatus == 'accepted') {
            			$response['user_message'] = $senderUser->getName()." has been added as your friend.";
            		}
            		$response['pendingFriendRequest'] = array();
            		$potentialFriends = Application_Model_Account::getAssociatedUsers(array("requested"),array('receiver_users_id'));
            		foreach ( $potentialFriends as $friend ) {
            			$response['pendingFriendRequest'][] = array("name" => $friend->name, "id" => $friend->id);
            		}      		
            	}
            }           
            $this->_helper->json($response);
    }
    
    /**
     * Gets the list of pending friends
     */
    public function getpendingfriendrequestAction() {
    	$auth = Zend_Auth::getInstance();
    	$user = $auth->getIdentity()->getUser();
    	/* @var $user Application_Model_Users */
    	
    	$usersFriendsDb = new Application_Model_DbTable_UsersFriends();
    	
    	$response['success'] =  true;
		$response['instructions'] = array();
		
		$response['instructions'][0] = array(
			"actions" => array(
				0 => "updatePendingFriendRequest"
			), 
			"targets" => array( 
				0 => array(
					array(
						'success' => "true", 
						'pendingFriendRequest'=>array()
					)
				)
			)
		); 
		
    	$potentialFriends = Application_Model_Account::getAssociatedUsers(array("requested"),array('receiver_users_id'));
    	
		foreach ( $potentialFriends as $friend ) {
			$response['instructions'][0]["targets"][0][0]['pendingFriendRequest'][] = array("name" => $friend->name, "id" => $friend->id);
		}

		$response['pendingFriendRequest'] = $response['instructions'][0]["targets"][0][0]['pendingFriendRequest'];
		
    	$this->_helper->json($response); 	
    }
    
    public function addAction() {
        $form = new Application_Form_AddAccount();

        $request = $this->getRequest();
        if ($request->isPost()) {  
            if ($form->isValid($request->getParams())) {
                $errmsg="";
                $acctData = $request->getParams();

                if (Application_Model_Account::accountExists(array("email" => $acctData["email"]))) {
                	$form->email->setValue("");
                	$errmsg = "<div>The email <em>{$acctData["email"]}</em> is already registered.</div>";
                }
                
                if (Application_Model_Account::loginNameExists(array("login_name" => $acctData["login_name"]))) {
                	$form->login_name->setValue("");
                	$errmsg .= "<div>The Public Display Name <em>{$acctData["login_name"]}</em> is already registered.</div>";
                }
                                  
                if ($acctData["login_pass"] != $acctData["confirm_password"]) {              	
                    $errmsg .= "<div>The passwords you entered did not match.</div>";
                }

                if($errmsg=="") {
                    $account = Application_Model_Account::createAccount($acctData);              
                    $this->redirect("/authentication/login");
                }
                else {
                    $this->view->msg = $errmsg;
                }

            }
        }

        $this->view->form = $form;
    }

    public function editAction() {
        $form = new Application_Form_AddAccount();
        $request = $this->getRequest();

        $account = Application_Model_Account::getAccountFromIdentity();

        $form->removeElement('terms_conditions');
        $form->login_pass->setRequired(false);

        if ($request->isPost()) {
            if ($form->isValid($request->getParams())) {
                foreach ($request->getParams() as $field => $value) {
                    try {
                        $account->$field = $value;
                    } catch (Exception $e) {
                        $this->_helper->layout()->error = $e->getMessage();
                    }
                }
            }
        } else {
            $data = $account->toArray();
            $data['birthday'] = date("m/d/Y", strtotime($data['birthday']));
            $form->populate($data);
        }

        $this->view->form = $form;
    }

    public function viewAction() {
        $request = $this->getRequest();
        $resp = new Application_Model_AjaxResponse();
        $account = Application_Model_Account::getAccountFromIdentity();

        $resp->setData($account->toArray());

        if ($request->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $jHelper = $this->_helper->getHelper('Json');
            $jHelper->sendJson($resp, false);
            return;
        }
    }

}

