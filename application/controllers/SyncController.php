<?php
class SyncController extends Zend_Controller_Action {
	public function waitingmatchupsAction() {
		$matchupDb = new Application_Model_DbTable_Matchup();
		$select = $matchupDb->select();
		$select->where("status = 'waiting'");
		$records = $matchupDb->fetchAll($select);
		if ($records) {
			foreach ($records as $record) {
				$matchup = new Application_Model_Matchup($record);
				if ($matchup->status == "waiting") {
					$currentDate = date("Y-m-d H:i:s");
					$matchDate = date_create_from_format("Y-m-d H:i:s", $matchup->date);					
					if ($currentDate > $matchDate) {
						$matchup->status = "active";
					}			
				}
			}
		}
	}
}
?>