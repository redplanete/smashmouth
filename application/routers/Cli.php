<?php

class Application_Router_Cli extends Zend_Controller_Router_Rewrite
{
    public function route (Zend_Controller_Request_Abstract $dispatcher)
    {
        $getopt = new Zend_Console_Getopt (array ());
        $arguments = $getopt->getRemainingArgs();

        file_put_contents(dirname(__FILE__). "/../../data/debug.log", print_r(implode(" -> ",$arguments), true), FILE_APPEND);

        if ($arguments)
        {
            $controller = array_shift ($arguments);
            $action = array_shift ($arguments);

            if (! preg_match ('~\W~', $controller) && ! preg_match ('~\W~', $action))
            {
                $dispatcher->setControllerName($controller);
                $dispatcher->setActionName($action);
                unset ($_SERVER['argv'] [1]);
                unset ($_SERVER['argv'] [2]);

                return $dispatcher;
            }

            echo "Invalid command.\n", exit;
        }

        echo "No command given.\n", exit;
    }


    public function assemble ($userParams, $name = null, $reset = false, $encode = true)
    {
        echo "Not implemented\n", exit;
    }
}